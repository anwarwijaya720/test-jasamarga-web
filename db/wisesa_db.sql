-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2020 at 06:52 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wisesa_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE `m_menu` (
  `no` int(11) NOT NULL,
  `menu_id` varchar(100) NOT NULL,
  `role_id` varchar(100) NOT NULL,
  `m_info_ruangan` tinyint(1) NOT NULL,
  `m_info_darah` tinyint(1) NOT NULL,
  `m_info_doketer_sp` tinyint(1) NOT NULL,
  `m_update_doketer_sp` tinyint(1) NOT NULL,
  `m_update_ruangan` tinyint(1) NOT NULL,
  `m_update_darah` tinyint(1) NOT NULL,
  `m_laporan_rujukan` tinyint(1) NOT NULL,
  `user_managemen` tinyint(1) NOT NULL,
  `setting` tinyint(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`no`, `menu_id`, `role_id`, `m_info_ruangan`, `m_info_darah`, `m_info_doketer_sp`, `m_update_doketer_sp`, `m_update_ruangan`, `m_update_darah`, `m_laporan_rujukan`, `user_managemen`, `setting`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, '7529d6a1-69d4-4ffa-9831-60bf22cd06b8', 'c6e2bf24-165a-43f8-bdb6-99dea1a3f058', 0, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-09 07:13:25', '', '0000-00-00 00:00:00', ''),
(2, '980af1cc-6429-4425-b74b-1b8c82880734', '86cf572a-03b8-4dbd-ab0f-49c0ab3b98bd', 0, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-09 07:14:20', '', '0000-00-00 00:00:00', ''),
(3, '46b60642-378e-44b0-90b7-a22878bb9adf', 'f6085309-6f8d-4ce7-a32f-05330fe3d6ad', 0, 0, 0, 0, 0, 0, 0, 0, 0, '2020-10-09 07:14:33', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_position`
--

CREATE TABLE `m_position` (
  `no` int(11) NOT NULL,
  `position_id` varchar(100) NOT NULL,
  `position_name` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_position`
--

INSERT INTO `m_position` (`no`, `position_id`, `position_name`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'beb1ab74-70af-4bb8-be4d-74740d2df36b', 'Bidan', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 'caa3f4c3-5483-463d-aa0e-6a5c72f2da8b', 'Dokter Spesialis', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 'fd7e0e48-7e40-4caf-b355-8be8c40ed2f5', 'Dokter Umum', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_role`
--

CREATE TABLE `m_role` (
  `no` int(11) NOT NULL,
  `role_id` varchar(100) NOT NULL,
  `role_name` enum('penerima','perujuk','super admin','') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_role`
--

INSERT INTO `m_role` (`no`, `role_id`, `role_name`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'c6e2bf24-165a-43f8-bdb6-99dea1a3f058', 'perujuk', '2020-10-09 06:40:01', 'sutrisna', '0000-00-00 00:00:00', ''),
(2, '86cf572a-03b8-4dbd-ab0f-49c0ab3b98bd', 'penerima', '2020-10-09 06:40:03', 'sutrisna', '0000-00-00 00:00:00', ''),
(4, 'f6085309-6f8d-4ce7-a32f-05330fe3d6ad', 'super admin', '2020-10-09 15:13:02', 'sutrisna', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_rs`
--

CREATE TABLE `m_rs` (
  `no` int(11) NOT NULL,
  `rs_id` varchar(100) NOT NULL,
  `rs_name` varchar(100) NOT NULL,
  `rs_type` varchar(2) NOT NULL,
  `level_maternal` varchar(10) NOT NULL,
  `img_rs` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_rs`
--

INSERT INTO `m_rs` (`no`, `rs_id`, `rs_name`, `rs_type`, `level_maternal`, `img_rs`, `address`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'ff24274b-62b9-4a06-8c67-b330fcf5b78e', 'Rs Bumi Lestari', 'c', 'maternal', '', 'bogor', '2020-10-13 12:36:06', 'oki', '0000-00-00 00:00:00', ''),
(2, '4cb41d80-9b35-4904-bcca-28f0be9dac77', 'Rs Muju Mundur', 'c', 'maternal', '', 'depok', '2020-10-13 12:36:17', 'oki', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_schedule`
--

CREATE TABLE `m_schedule` (
  `no` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rs_id` varchar(100) NOT NULL,
  `hour_1` tinyint(1) NOT NULL,
  `hour_2` tinyint(1) NOT NULL,
  `hour_3` tinyint(1) NOT NULL,
  `hour_4` tinyint(1) NOT NULL,
  `hour_5` tinyint(1) NOT NULL,
  `hour_6` tinyint(1) NOT NULL,
  `hour_7` tinyint(1) NOT NULL,
  `hour_8` tinyint(1) NOT NULL,
  `hour_9` tinyint(1) NOT NULL,
  `hour_10` tinyint(1) NOT NULL,
  `hour_11` tinyint(1) NOT NULL,
  `hour_12` tinyint(1) NOT NULL,
  `hour_13` tinyint(1) NOT NULL,
  `hour_14` tinyint(1) NOT NULL,
  `hour_15` tinyint(1) NOT NULL,
  `hour_16` tinyint(1) NOT NULL,
  `hour_17` tinyint(1) NOT NULL,
  `hour_18` tinyint(1) NOT NULL,
  `hour_19` tinyint(1) NOT NULL,
  `hour_20` tinyint(1) NOT NULL,
  `hour_21` tinyint(1) NOT NULL,
  `hour_22` tinyint(1) NOT NULL,
  `hour_23` tinyint(1) NOT NULL,
  `hour_24` tinyint(1) NOT NULL,
  `on_site` tinyint(1) NOT NULL,
  `prwt` tinyint(1) NOT NULL,
  `on_call` tinyint(1) NOT NULL,
  `off` tinyint(1) NOT NULL,
  `update_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `m_title`
--

CREATE TABLE `m_title` (
  `no` int(11) NOT NULL,
  `title_id` varchar(100) NOT NULL,
  `title_name` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `m_type_blood`
--

CREATE TABLE `m_type_blood` (
  `no` int(11) NOT NULL,
  `id_blood` varchar(100) NOT NULL,
  `blood_type` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_type_blood`
--

INSERT INTO `m_type_blood` (`no`, `id_blood`, `blood_type`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'bb12c8f7-d084-4608-bf8a-6c7b1a3f4dbb', 'A', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, '44294851-117f-4770-aa74-d607408814a0', 'AB', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 'b5b58e0e-b5b2-4fa7-a273-a54290b3a934', 'B', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(4, 'a960dc6e-ceae-4d13-babf-acdf19b68dfe', 'O', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_type_room`
--

CREATE TABLE `m_type_room` (
  `no` int(11) NOT NULL,
  `id_stock_room` varchar(100) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_type_room`
--

INSERT INTO `m_type_room` (`no`, `id_stock_room`, `room_name`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'a0763152-1fe2-4c0e-9f2f-8835c2cf4617', 'NICU', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 'e2c687ad-8c21-40bc-a4e0-34b5dc5623fa', 'ICU', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 'd2ad5baa-5ae0-4111-8e1f-156543f05474', 'Ruangan Perawatan', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(4, 'fa653329-fc72-49cf-ad3f-3369c8c07ad4', 'HCU', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(5, '9acfd44a-db62-4e57-8559-b7c7528f4db5', 'Ruangan Isolasi Ibu', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(6, 'd7804637-1e9d-4927-b76d-2a827a7fb075', 'Ruangan Isolasi Bayi', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `no` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `rs_id` varchar(100) NOT NULL,
  `role_id` varchar(100) NOT NULL,
  `position_id` varchar(100) NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `device` enum('web','mobile') NOT NULL,
  `approve` tinyint(1) NOT NULL,
  `approve_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `approve_by` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `delet_date` datetime DEFAULT NULL,
  `delet_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_users`
--

INSERT INTO `m_users` (`no`, `user_id`, `username`, `password`, `no_hp`, `rs_id`, `role_id`, `position_id`, `status`, `device`, `approve`, `approve_date`, `approve_by`, `created_date`, `created_by`, `updated_date`, `updated_by`, `delet_date`, `delet_by`) VALUES
(1, 'ae8b9988-448d-4583-b6c3-0d2f912b9209', 'saya', '21232f297a57a5a743894a0e4a801fc3', '0858926788', 'ff24274b-62b9-4a06-8c67-b330fcf5b78e', 'f6085309-6f8d-4ce7-a32f-05330fe3d6ad', 'beb1ab74-70af-4bb8-be4d-74740d2df36b', '1', 'web', 1, '2020-10-14 16:52:18', 'saya', '2020-10-14 16:52:18', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(2, 'afe0a8d3-7e3e-4f36-a88d-de98e3000e84', 'kamu', '21232f297a57a5a743894a0e4a801fc3', '14271783613', '4cb41d80-9b35-4904-bcca-28f0be9dac77', 'f6085309-6f8d-4ce7-a32f-05330fe3d6ad', 'caa3f4c3-5483-463d-aa0e-6a5c72f2da8b', '1', '', 0, '2020-10-14 11:36:28', '', '2020-10-14 11:36:28', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 'c77d2ebb-0b05-11eb-94be-507b9d8ea68f', 'dia', '21232f297a57a5a743894a0e4a801fc3', '', '4cb41d80-9b35-4904-bcca-28f0be9dac77', '', '', '0', 'web', 0, '2020-10-14 16:52:21', '', '2020-10-14 16:52:21', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_stock_blood`
--

CREATE TABLE `t_stock_blood` (
  `no` int(11) NOT NULL,
  `stock_blood_id` varchar(100) NOT NULL,
  `rs_id` varchar(100) NOT NULL,
  `blood_aWb` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_aPrc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_aTc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `boold_abWb` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_abPrc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_abTc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `boold_bWb` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_bPrc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_bTc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_oWb` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `blood_oPrc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `boold_oTc` enum('Tersedia','Kosong','Penuh') NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_stock_room`
--

CREATE TABLE `t_stock_room` (
  `no` int(11) NOT NULL,
  `stock_room_id` varchar(100) NOT NULL,
  `rs_id` varchar(100) NOT NULL,
  `nicu` enum('Tersedia','Tunggu','Penuh') NOT NULL,
  `icu` enum('Tersedia','Tunggu','Penuh') NOT NULL,
  `ruang_perawatan` enum('Tersedia','Tunggu','Penuh') NOT NULL,
  `hcu` enum('Tersedia','Tunggu','Penuh') NOT NULL,
  `ruang_isolasi_ibu` enum('Tersedia','Tunggu','Penuh') NOT NULL,
  `ruang_isolasi_bayi` enum('Tersedia','Tunggu','Penuh') NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_stock_room`
--

INSERT INTO `t_stock_room` (`no`, `stock_room_id`, `rs_id`, `nicu`, `icu`, `ruang_perawatan`, `hcu`, `ruang_isolasi_ibu`, `ruang_isolasi_bayi`, `updated_date`, `updated_by`) VALUES
(6, '4129fc28-0ca4-11eb-94be-507b9d8ea68f', 'ff24274b-62b9-4a06-8c67-b330fcf5b78e', 'Tersedia', 'Tersedia', 'Tersedia', 'Tersedia', 'Tersedia', 'Tersedia', '2020-10-12 05:50:23', 'saya'),
(7, '98f26677-0c75-11eb-94be-507b9d8ea68f', '4cb41d80-9b35-4904-bcca-28f0be9dac77', 'Tunggu', 'Tersedia', 'Tersedia', 'Tersedia', 'Penuh', 'Tersedia', '2020-10-12 12:16:24', 'saya');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_position`
--
ALTER TABLE `m_position`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_role`
--
ALTER TABLE `m_role`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_rs`
--
ALTER TABLE `m_rs`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_schedule`
--
ALTER TABLE `m_schedule`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_type_blood`
--
ALTER TABLE `m_type_blood`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_type_room`
--
ALTER TABLE `m_type_room`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `t_stock_blood`
--
ALTER TABLE `t_stock_blood`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `t_stock_room`
--
ALTER TABLE `t_stock_room`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_menu`
--
ALTER TABLE `m_menu`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_position`
--
ALTER TABLE `m_position`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_role`
--
ALTER TABLE `m_role`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_rs`
--
ALTER TABLE `m_rs`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_schedule`
--
ALTER TABLE `m_schedule`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_type_blood`
--
ALTER TABLE `m_type_blood`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_type_room`
--
ALTER TABLE `m_type_room`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_stock_blood`
--
ALTER TABLE `t_stock_blood`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_stock_room`
--
ALTER TABLE `t_stock_room`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
