<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') == "login") {
			redirect(base_url("Dashboard"));
		}
		$this->load->model('m_login');
	}

	public function index()
	{
		$this->load->view('Login');
	}

	public function validateLogin()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));		

		$where = array(
			'username' => $username,
			'password' => $password,
		);

		$dataUser = $this->m_login->cek_login($where);

		if (!empty($dataUser)) {
			if($dataUser[0]->role_name != "super admin" || $dataUser[0]->rs_name == null){
				$data['error'] = true;
				$this->load->view('Login', $data);
			}else{
				$data_session = array(
					//profile
					'user_id' => $dataUser[0]->user_id,
					'role_id' => $dataUser[0]->role_id,
					'username' => $dataUser[0]->username,
					'no_hp' => $dataUser[0]->no_hp,			
					'rs_name' => $dataUser[0]->rs_name,
					'rs_type' => $dataUser[0]->rs_type,
					'level_maternal' => $dataUser[0]->level_maternal,
					'img_rs' => $dataUser[0]->img_rs,
					'status' => 'login',
					// Status Users
					'role' => $dataUser[0]->role_id,
					'role' => $dataUser[0]->role_name,
				);
				$this->session->set_userdata($data_session);
				redirect(base_url(("Dashboard")));
				// print_r($data_session);
			}
		} else {
			$data['error'] = true;
			$this->load->view('Login', $data);
		}
	}
}
