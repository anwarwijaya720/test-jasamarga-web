<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class InfoDoctor extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model('master/m_info_dockter');
	}
	
	public function index()
	{
		//cek status off
		$status_off = 0;
	
		$status_off = $this->cek_status_off('obgynAnak');
		if ( $status_off > 0) {
			// echo 'ada off';
			$data['obgynAnak']		= $this->m_info_dockter->obgyn_anak(2);
			$data['getRsObgynAnak']	= $this->m_info_dockter->get_data_rs('ms.obgyn = 2');
		} else {
			// echo 'tidak ada off';
			$data['obgynAnak']		= $this->m_info_dockter->obgyn_anak(1);
			$data['getRsObgynAnak']	= $this->m_info_dockter->get_data_rs('ms.obgyn = 1 or ms.anak = 1');
		}
		//===============//
		$status_off = $this->cek_status_off('ms.obgyn');
		if ( $status_off > 0) {
			// echo 'ada off';
			$data['obgyn']			= $this->m_info_dockter->obgyn(2);
			$data['getRsObgyn']		= $this->m_info_dockter->get_data_rs('ms.obgyn = 2');
		} else {
			// echo 'tidak ada off';
			$data['obgyn']			= $this->m_info_dockter->obgyn(1);
			$data['getRsObgyn']		= $this->m_info_dockter->get_data_rs('ms.obgyn = 1');
		}		
		//===============//
		$status_off = $this->cek_status_off('ms.anak');
		if ( $status_off > 0) {
			// echo 'ada off';
			$data['anak']			= $this->m_info_dockter->anak(2);
			$data['getRsAnak']		= $this->m_info_dockter->get_data_rs('ms.anak = 2');
		} else {
			// echo 'tidak ada off';
			$data['anak']			= $this->m_info_dockter->anak(1);
			$data['getRsAnak']		= $this->m_info_dockter->get_data_rs('ms.anak = 1');
		}
		//===============//
		$status_off = $this->cek_status_off('ms.jp');
		if ( $status_off > 0) {
			// echo 'ada off';
			$data['jantung']		= $this->m_info_dockter->jantung(2);
			$data['getRsJantung']	= $this->m_info_dockter->get_data_rs('ms.jp = 2');
		} else {
			// echo 'tidak ada off';
			$data['jantung']		= $this->m_info_dockter->jantung(1);
			$data['getRsJantung']	= $this->m_info_dockter->get_data_rs('ms.jp = 1');
		}
		//===============//
		$status_off = $this->cek_status_off('ms.pd');
		if ( $status_off > 0) {
			// echo 'ada off';
			$data['dalam']			= $this->m_info_dockter->dalam(2);
			$data['getRsDalam']		= $this->m_info_dockter->get_data_rs('ms.pd = 2');
		} else {
			// echo 'tidak ada off';
			$data['dalam']			= $this->m_info_dockter->dalam(1);
			$data['getRsDalam']		= $this->m_info_dockter->get_data_rs('ms.pd = 1');
		}
		//===============//		
		$status_off = $this->cek_status_off('ms.spp');
		if ( $status_off > 0) {
			// echo 'ada off';
			$data['paru']			= $this->m_info_dockter->paru(2);
			$data['getRsParu']		= $this->m_info_dockter->get_data_rs('ms.spp = 2');
		} else {
			// echo 'tidak ada off';
			$data['paru']			= $this->m_info_dockter->paru(1);
			$data['getRsParu']		= $this->m_info_dockter->get_data_rs('ms.spp = 1');
		}

		$data['view'] = 'conten/master/info_doctor';
		$this->load->view('templates/base_template_public', $data);

	}

	function cek_status_off($type)
	{
		if($type = "obgynAnak"){			
			$data = $this->m_info_dockter->status_off_ObgynAnak();		
		}else{
			$data = $this->m_info_dockter->status_off("'.$type.'");		
		}
		$total_off = 0;
		if(!empty($data)){		
			for ($i = 1; $i <= 24; $i++) {
				$total_off +=  $data[0]->{'hour_' . $i};
			}	
		}

		return $total_off;
		
	}
}