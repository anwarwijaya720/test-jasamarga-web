<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanRujukan extends CI_Controller {

    public function __construct() {
		parent::__construct();
        $this->load->model('master/m_laporan_rujukan');
	}

	public function index()
	{			
		$cekTrnsaction = $this->m_laporan_rujukan->getTransaction();
		if(!empty($cekTrnsaction)){
			//Penerima Rujukan
			$data['penerimaRujukan'] = $this->m_laporan_rujukan->laporan_penerima_rujukan();
			//Perujuk
			$data['perujuk'] = $this->m_laporan_rujukan->laporan_rujukan();
			//Perujuk
			$data['nakses_perujuk'] = $this->m_laporan_rujukan->laporan_nakses_perujuk_ibu();
			//Perujuk
			$data['nakses_total'] = $this->m_laporan_rujukan->laporan_nakses_perujuk_total();			
		}

		//GRAFIK 
		$data['obgyn_anak'] = $this->m_laporan_rujukan->stock_obgyn_anak();
		$data['obgyn'] = $this->m_laporan_rujukan->stock_obgyn();
		$data['anak'] = $this->m_laporan_rujukan->stock_sp_anak();
		$data['jantung'] = $this->m_laporan_rujukan->stock_sp_jantung();
		$data['pd'] = $this->m_laporan_rujukan->stock_sp_pd();
		$data['paru'] = $this->m_laporan_rujukan->stock_sp_paru();	
		//Ruangan
		$data['perawatan'] = $this->m_laporan_rujukan->stock_r_perawatan();
		$data['nicu'] = $this->m_laporan_rujukan->stock_r_nicu();
		$data['intensif'] = $this->m_laporan_rujukan->stock_r_instensif();	
		$data['hcu'] = $this->m_laporan_rujukan->stock_r_hcu();	
		$data['ponek'] = $this->m_laporan_rujukan->stock_r_ponek();	
		//Covid
		$data['covid'] = $this->m_laporan_rujukan->stock_r_covid();

		$data['view'] = 'conten/public/laporan_rujukan';
		$this->load->view('templates/base_template_public', $data);
    }
}