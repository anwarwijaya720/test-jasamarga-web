<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class InfoBlood extends CI_Controller {

    public function __construct() {
		parent::__construct();
	
        $this->load->model('master/m_info_blood');
	}

	public function index()
	{
		//=========== A ================
		$data['aWb'] = $this->m_info_blood->info_blood('blood_aWb', 'Stok_aWb');				
		$data['aPrc'] = $this->m_info_blood->info_blood('blood_aPrc', 'Stok_aPrc');				
		$data['aTc'] = $this->m_info_blood->info_blood('blood_aTc', 'Stok_aTc');
		//=========== AB ================				
		$data['abWb'] = $this->m_info_blood->info_blood('blood_abWb', 'Stok_abWb');				
		$data['abPrc'] = $this->m_info_blood->info_blood('blood_abPrc', 'Stok_abPrc');				
		$data['abTc'] = $this->m_info_blood->info_blood('blood_abTc', 'Stok_abTc');
		//=========== B ================				
		$data['bWb'] = $this->m_info_blood->info_blood('blood_bWb', 'Stok_bWb');
		$data['bPrc'] = $this->m_info_blood->info_blood('blood_bPrc', 'Stok_bPrc');	
		$data['bTc'] = $this->m_info_blood->info_blood('blood_bTc', 'Stok_bTc');
		//=========== O ================				
		$data['oWb'] = $this->m_info_blood->info_blood('blood_oWb', 'Stok_oWb');				
		$data['oPrc'] = $this->m_info_blood->info_blood('blood_oPrc', 'Stok_oPrc');				
		$data['oTc'] = $this->m_info_blood->info_blood('blood_oTc', 'Stok_oTc');
		//=========== RS A ================				
		$data['rs_aWb'] = $this->m_info_blood->info_rs('blood_aWb');				
		$data['rs_aPrc'] = $this->m_info_blood->info_rs('blood_aPrc');				
		$data['rs_aTc'] = $this->m_info_blood->info_rs('blood_aTc');
		//=========== RS AB ================				
		$data['rs_abWb'] = $this->m_info_blood->info_rs('blood_abWb');				
		$data['rs_abPrc'] = $this->m_info_blood->info_rs('blood_abPrc');
		$data['rs_abTc'] = $this->m_info_blood->info_rs('blood_abTc');
		//=========== RS B ================				
		$data['rs_bWb'] = $this->m_info_blood->info_rs('blood_bWb');				
		$data['rs_bPrc'] = $this->m_info_blood->info_rs('blood_bPrc');				
		$data['rs_bTc'] = $this->m_info_blood->info_rs('blood_bTc');
		//=========== RS O ================				
		$data['rs_oWb'] = $this->m_info_blood->info_rs('blood_oWb');				
		$data['rs_oPrc'] = $this->m_info_blood->info_rs('blood_oPrc');		
		$data['rs_oTc'] = $this->m_info_blood->info_rs('blood_oTc');

		$data['view'] = 'conten/public/info_blood';
        $this->load->view('templates/base_template_public', $data);        
    }
}