<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('m_login');
		$this->load->library('form_validation');
		$this->load->model('master/m_users');
	}

	public function index()
	{
		$data['getRs'] = $this->m_users->getRs();
		$data['view'] = 'conten/master/users';
		$this->load->view('templates/base_template', $data);
	}

	public function getAllUsers()
	{
		$list = $this->m_users->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$noUrut = $no++ + 1;
			$row = array();
			$row[] = $noUrut;
			$row[] = $person->username;
			$row[] = $person->no_hp;
			if (!empty($person->rs_name)) {
				$row[] = $person->rs_name;
			} else {
				$row[] = $person->institusi;
			}
			$row[] = $person->role_name;
			$row[] = $person->profesi;
			if ($person->approve != 0) {
				$row[] = 'approve';
				//add html for action
				$row[] = '
						<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person(' . "'" . $person->user_id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>
					';
			} else {
				$row[] = null;
				//add html for action
				$row[] = '
						<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person(' . "'" . $person->user_id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
						<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person(' . "'" . $person->user_id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>
					';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_users->count_all(),
			"recordsFiltered" => $this->m_users->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function deletUser($user_id)
	{
		$session_name = $this->session->userdata('username');
		$date_format = date("Y-m-d h:i:s");
		$where = array(
			'user_id' => $user_id,
		);
		$data = array(
			'status' => '2',
			'delete_date' => $date_format,
			'delete_by' => $session_name
		);
		$delete = $this->m_users->delete($where, $data);
		if ($delete == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

	function approveUser($user_id)
	{
		$session_name = $this->session->userdata('username');
		$where = array(
			'user_id' => $user_id,
		);
		$data = array(
			'status' => '1',
			'approve' => '1',
			'approve_by' => $session_name
		);
		$delete = $this->m_users->approve($data, $where);
		if ($delete == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

	function ajax_add($save_method)
	{
		$this->_validate($save_method);
		$session_name = $this->session->userdata('username');
		$rsName = $this->input->post('instansi');
		$where = array(
			'rs_name' => $rsName,
		);
		$response = $this->m_users->getRsWhere($where);
		if (!empty($response)) {
			$this->db->set('user_id', 'UUID()', FALSE);
			$data = array(
				'username' => $this->input->post('username'),
				'no_hp' => $this->input->post('noHp'),
				'password' => md5($this->input->post('password')),
				'rs_id' => $response[0]->rs_id,
				'profesi' => $this->input->post('position'),
				'role_id' => 'f6085309-6f8d-4ce7-a32f-05330fe3d6ad',
				'approve' => 1,
				'status' => '1',
				'device' => 'web',
				'created_by' => $session_name,
			);
		} else {
			$this->db->set('rs_id', 'UUID()', FALSE);
			$dt = array(
				'rs_name' => $rsName,
			);
			$response = $this->m_users->addRs($dt);
			if ($response == 1) {
				$responseRs = $this->m_users->getRsWhere($dt);
				$this->db->set('user_id', 'UUID()', FALSE);
				$data = array(
					'username' => $this->input->post('username'),
					'no_hp' => $this->input->post('noHp'),
					'password' => md5($this->input->post('password')),
					'rs_id' => $responseRs[0]->rs_id,
					'profesi' => $this->input->post('position'),
					'role_id' => 'f6085309-6f8d-4ce7-a32f-05330fe3d6ad',
					'approve' => 1,
					'status' => '1',
					'device' => 'web',
					'created_by' => $session_name,
				);
			}
		}
		$insert = $this->m_users->save($data);
		if ($insert == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

	public function ajax_update($save_method)
	{
		$this->_validate($save_method);
		$session_name = $this->session->userdata('username');
		$date_format = date("Y-m-d h:i:s");
		$userId = $this->input->post('usersId');
		$username = $this->input->post('username');
		$rsName = $this->input->post('instansi');
		$noHp = $this->input->post('noHp');
		$position = $this->input->post('position');

		// $userId = '035114e1-1906-11eb-b70b-507b9d8ea68f';
		// $username = 'saya';	
		// $rsName = 'Rs Duit';
		// $noHp = '0998708971189';
		// $position = 'bidan';

		$where = array(
			'rs_name' => $rsName,
		);
		$res = $this->m_users->getRsWhere($where);
		if (!empty($res)) {
			$data = array(
				'username' => $username,
				'no_hp' => $noHp,
				'rs_id' => $res[0]->rs_id,
				'profesi' => $position,
				'approve' => '1',
				'status' => '1',
				'approve_by' => $session_name,
				'approve_date' => $date_format
			);
		} else {
			$this->db->set('rs_id', 'UUID()', FALSE);
			$dt = array(
				'rs_name' => $rsName,
				'created_by' => $session_name,
			);
			$this->m_users->addRs($dt);
			$where = array(
				'rs_name' => $rsName,
			);
			$responseRs = $this->m_users->getRsWhere($where);
			$data = array(
				'username' => $username,
				'no_hp' => $noHp,
				'rs_id' => $responseRs[0]->rs_id,
				'profesi' => $position,
				'approve' => '1',
				'status' => '1',
				'approve_by' => $session_name,
				'approve_date' => $date_format
			);
		}
		$where = array(
			'user_id' => $userId,
		);
		$response = $this->m_users->edit($data, $where);
		if ($response == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

	function edit_user($user_id)
	{
		$where = array('user_id' => $user_id);
		$data = $this->m_users->getWhere($where);
		echo json_encode($data);
	}

	function edit_user_action($save_method)
	{
		$this->_validate($save_method);
		$username = $this->input->post('username');
		$data = array(
			'username' => $this->input->post('username'),
			'no_hp' => $this->input->post('noHp'),
			'password' => md5($this->input->post('password')),
			'rs_id' => $this->input->post('rs'),
			'profesi' => $this->input->post('position'),
			'role_id' => $this->input->post('role'),
		);
		$where = array(
			'username' => $username
		);
		$response = $this->m_users->edit($data, $where);
		if ($response == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

	function _validate($save_method)
	{

		$username = $this->input->post('username');
		$noHp = $this->input->post('noHp');
		$instansi = $this->input->post('instansi');
		$position = $this->input->post('position');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$user_id = $this->input->post('usersId');

		// $save_method = 'add';
		// $username = 'saya';
		// $noHp = '08897817312378268';
		// $position = 'jaskjakls';
		// $instansi = 'haksjhajks';
		// $password = '';
		// $confirm_password = '';

		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if ($username == '') {
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'First Username is required';
			$data['status'] = FALSE;
		} else {
			if (strlen($username) < 2) {
				$data['inputerror'][] = 'username';
				$data['error_string'][] = 'Invalid value Mimium 3 Characters';
				$data['status'] = FALSE;
			}
		}

		$whereName = array(
			'username' => $username,
		);
		$whereId = array(
			'user_id' => $user_id,
		);
		$resName = $this->m_users->getUsersWhere($whereName);
		$resId = $this->m_users->getUsersWhere($whereId);
		if (!empty($resName[0]->username)) {
			if ($save_method == 'update') {
				if ($resName[0]->username == $username) {						
					if($resId[0]->username != $username){
						$data['inputerror'][] = 'username';
						$data['error_string'][] = 'username already exists';
						$data['status'] = FALSE;
					}			
				}
			} else {
				if ($resName[0]->username == $username) {
					$data['inputerror'][] = 'username';
					$data['error_string'][] = 'username already exists';
					$data['status'] = FALSE;
				}
			}
		}

		if ($save_method == "add") {

			if ($password == '') {
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'First password is required';
				$data['status'] = FALSE;
			} else {

				if (strlen($password) < 4) {
					$data['inputerror'][] = 'password';
					$data['error_string'][] = 'Invalid value Mimium 5 Characters';
					$data['status'] = FALSE;
				}
			}

			if ($confirm_password == '') {
				$data['inputerror'][] = 'confirm_password';
				$data['error_string'][] = 'First password is required';
				$data['status'] = FALSE;
			}

			if ($confirm_password != $password) {
				$data['inputerror'][] = 'confirm_password';
				$data['error_string'][] = 'Invalid Confirm Password';
				$data['status'] = FALSE;
			}
		}

		if ($noHp == '') {
			$data['inputerror'][] = 'noHp';
			$data['error_string'][] = 'First No Henphone is required';
			$data['status'] = FALSE;
		} else {
			if (strlen($noHp) <= 10) {
				$data['inputerror'][] = 'noHp';
				$data['error_string'][] = 'Invalid value Mimium 10 Characters';
				$data['status'] = FALSE;
			}
		}

		if ($instansi == '') {
			$data['inputerror'][] = 'instansi';
			$data['error_string'][] = 'Please Select Hospital';
			$data['status'] = FALSE;
		}

		if ($position == '') {
			$data['inputerror'][] = 'position';
			$data['error_string'][] = 'First position is required';
			$data['status'] = FALSE;
		}

		if ($data['status'] === FALSE) {
			echo json_encode($data);
			exit();
		}
	}

	//============================EXAMPLE========================================//

	private function _validate_string($string)
	{
		$allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		for ($i = 0; $i < strlen($string); $i++) {
			if (strpos($allowed, substr($string, $i, 1)) === FALSE) {
				return FALSE;
			}
		}

		return TRUE;
	}

	private function _validate_number($string)
	{
		$allowed = "0123456789";
		for ($i = 0; $i < strlen($string); $i++) {
			if (strpos($allowed, substr($string, $i, 1)) === FALSE) {
				return FALSE;
			}
		}

		return TRUE;
	}

	// function coba(){

	// 	header('Content-Type: application/json');

	// 	// $required = 'required: true';
	// 	$ms_required = 'tidak boleh kosong';

	// 	// date1: {
	// 	// 	required: true,
	// 	// 	date: true,
	// 	// },

	// 	$data['rules'] = array();
	// 	$data['messages'] = array();

	// 	$data['rules']['username'] = array(
	// 		'required' => true,
	// 		'minlength'=> 5			
	// 	);
	// 	$data['messages']['username'] = array(
	// 		'required' => 'tidak boleh kosong',
	// 		'minlength' => 'minimal 5 karakter'
	// 	);

	// 	$data['rules']['pass'] = array(
	// 		'required' => true,
	// 		'minlength'=> 8			
	// 	);
	// 	$data['messages']['pass'] = array(
	// 		'required' => 'tidak boleh kosong',
	// 		'minlength' => 'minimal 8 karakter'
	// 	);

	// 	//add the header here
	// 	//  echo json_encode( $arr );

	// 	echo json_encode($data);

	// }

	// function example()
	// {
	// 	//   $email = $this->input->post('email');
	// 	//   $username = $this->input->post('username');
	// 	//   $password = $this->input->post('password');
	// 	//   $confirm_password = $this->input->post('confirm_password');
	// 	//   $role = $this->input->post('role');	

	// 	// $email = 'saya@saya.com';
	// 	$username = '';
	// 	$password = '';
	// 	$confirm_password = '';
	// 	$role = '';

	// 	$data = array();
	// 	$data['error_string'] = array();
	// 	$data['inputerror'] = array();
	// 	$data['status'] = TRUE;

	// $where = array(
	// 	'email' => $email,
	// );

	// $response = $this->m_users->getWhere($where);

	// if ($email == '') {
	// 	$data['inputerror'][] = 'email';
	// 	$data['error_string'][] = 'First Email is required';
	// 	$data['status'] = FALSE;
	// } else {
	// 	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
	// 	if ($this->form_validation->run() != FALSE) {
	// 		if (!empty($response[0]->email)) {
	// 			if ($response[0]->email == $email) {
	// 				$data['inputerror'][] = 'email';
	// 				$data['error_string'][] = 'email already exists';
	// 				$data['status'] = FALSE;
	// 			}
	// 		}
	// 	} else {
	// 		$data['inputerror'][] = 'email';
	// 		$data['error_string'][] = 'format email is wrong';
	// 		$data['status'] = FALSE;
	// 	}
	// 	// }

	// 	if ($username == '') {
	// 		$data['inputerror'][] = 'username';
	// 		$data['error_string'][] = 'First Username is required';
	// 		$data['status'] = FALSE;
	// 	} else {
	// 		if (strlen($username) < 4) {
	// 			$data['inputerror'][] = 'username';
	// 			$data['error_string'][] = 'Invalid value Mimium 5 Characters';
	// 			$data['status'] = FALSE;
	// 		}
	// 	}

	// 	// if (!empty($response[0]->username)) {
	// 	// 	if ($response[0]->username == $username) {
	// 	// 		$data['inputerror'][] = 'username';
	// 	// 		$data['error_string'][] = 'username already exists';
	// 	// 		$data['status'] = FALSE;
	// 	// 	}
	// 	// }

	// 	if ($password == '') {
	// 		$data['inputerror'][] = 'password';
	// 		$data['error_string'][] = 'First password is required';
	// 		$data['status'] = FALSE;
	// 	} else {

	// 		if (strlen($password) < 4) {
	// 			$data['inputerror'][] = 'password';
	// 			$data['error_string'][] = 'Invalid value Mimium 5 Characters';
	// 			$data['status'] = FALSE;
	// 		}
	// 	}

	// 	if ($confirm_password == '') {
	// 		$data['inputerror'][] = 'confirm_password';
	// 		$data['error_string'][] = 'First password is required';
	// 		$data['status'] = FALSE;
	// 	}

	// 	if ($confirm_password != $password) {
	// 		$data['inputerror'][] = 'confirm_password';
	// 		$data['error_string'][] = 'Invalid Confirm Password';
	// 		$data['status'] = FALSE;
	// 	}

	// 	if ($role == '') {
	// 		$data['inputerror'][] = 'role';
	// 		$data['error_string'][] = 'First name is required';
	// 		$data['status'] = FALSE;
	// 	}

	// 	if ($data['status'] === FALSE) {
	// 		echo json_encode($data);
	// 		exit();
	// 	}
	// }

}
