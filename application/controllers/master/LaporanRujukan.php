<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class LaporanRujukan extends CI_Controller {

    public function __construct() {
        parent::__construct();        
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login"));
		}		
        $this->load->model('master/m_laporan_rujukan');
	}

	public function index()
	{				
		$cekTrnsaction = $this->m_laporan_rujukan->getTransaction();
		if(!empty($cekTrnsaction)){
			//Penerima Rujukan
			$data['penerimaRujukan'] = $this->m_laporan_rujukan->laporan_penerima_rujukan();
			//Perujuk
			$data['perujuk'] = $this->m_laporan_rujukan->laporan_rujukan();
			//Perujuk
			$data['nakses_perujuk'] = $this->m_laporan_rujukan->laporan_nakses_perujuk_ibu();
			//Perujuk
			$data['nakses_total'] = $this->m_laporan_rujukan->laporan_nakses_perujuk_total();			
		}

		//GRAFIK 
		$data['obgyn_anak'] = $this->m_laporan_rujukan->stock_obgyn_anak();
		$data['obgyn'] = $this->m_laporan_rujukan->stock_obgyn();
		$data['anak'] = $this->m_laporan_rujukan->stock_sp_anak();
		$data['jantung'] = $this->m_laporan_rujukan->stock_sp_jantung();
		$data['pd'] = $this->m_laporan_rujukan->stock_sp_pd();
		$data['paru'] = $this->m_laporan_rujukan->stock_sp_paru();	
		//Ruangan
		$data['perawatan'] = $this->m_laporan_rujukan->stock_r_perawatan();
		$data['nicu'] = $this->m_laporan_rujukan->stock_r_nicu();
		$data['intensif'] = $this->m_laporan_rujukan->stock_r_instensif();	
		$data['hcu'] = $this->m_laporan_rujukan->stock_r_hcu();	
		$data['ponek'] = $this->m_laporan_rujukan->stock_r_ponek();	
		//Covid
		$data['covid'] = $this->m_laporan_rujukan->stock_r_covid();

		$data['view'] = 'conten/master/laporan_rujukan';
		$this->load->view('templates/base_template_public', $data);
		
	}
	
	function export(){
		$where = $this->input->post('selectTime');
		if(!empty($where)|| $where != ''){
			$res = $this->m_laporan_rujukan->export($where);
		}

		if(!empty($res)){
			$spreadsheet = new Spreadsheet();
			$Colum  = $spreadsheet->getActiveSheet();
			$excel    = $spreadsheet->setActiveSheetIndex(0);

			// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
			$style_col = array(
				'font' => array('bold' => true), // Set font nya jadi bold
				'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);

			// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
			$style_row = array(
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', "REKAPITULASI"); // Set kolom A1 dengan tulisan "DATA SISWA"
			$Colum->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
			$Colum->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
			$Colum->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
			$Colum->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
			// Buat header tabel nya pada baris ke 3
			$excel->setCellValue('A3', "NO"); 
			$excel->setCellValue('B3', "INSTANSI"); 
			$excel->setCellValue('C3', "KEBUTUHAN"); 
			$excel->setCellValue('D3', "JUMLAH"); 
			$excel->setCellValue('E3', "TANGGAL"); 
			// Apply style header yang telah kita buat tadi ke masing-masing kolom header
			$excel->getStyle('A3')->applyFromArray($style_col);
			$excel->getStyle('B3')->applyFromArray($style_col);
			$excel->getStyle('C3')->applyFromArray($style_col);
			$excel->getStyle('D3')->applyFromArray($style_col);
			$excel->getStyle('E3')->applyFromArray($style_col);

			//=================================================================//
			$no = 1; 
			$numrow = 4; 
			foreach ($res as $data) { 
				$excel->setCellValue('A' . $numrow, $no);
				$excel->setCellValue('B' . $numrow, $data->rs_name);
				$excel->setCellValue('C' . $numrow, $data->kebutuhan);
				$excel->setCellValue('D' . $numrow, $data->total);
				$excel->setCellValue('E' . $numrow, $data->tanggal);
				
				$excel->getStyle('A' . $numrow)->applyFromArray($style_row);
				$excel->getStyle('B' . $numrow)->applyFromArray($style_row);
				$excel->getStyle('C' . $numrow)->applyFromArray($style_row);
				$excel->getStyle('D' . $numrow)->applyFromArray($style_row);
				$excel->getStyle('E' . $numrow)->applyFromArray($style_row);

				$no++; 
				$numrow++; 
			}
			// Set width kolom
			$excel->getColumnDimension('A')->setWidth(5); // Set width kolom A
			$excel->getColumnDimension('B')->setWidth(40); // Set width kolom B
			$excel->getColumnDimension('C')->setWidth(30); // Set width kolom C
			$excel->getColumnDimension('D')->setWidth(10); // Set width kolom D
			$excel->getColumnDimension('E')->setWidth(20); // Set width kolom D

			// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
			$excel->getDefaultRowDimension()->setRowHeight(-1);
			// Set orientasi kertas jadi LANDSCAPE
			$excel->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
			// Set judul file excel nya
			$excel->setTitle("Report Laporan Rujukan");

			$writer = new Xlsx($spreadsheet);

			$filename = 'Laporan Rujukan';

			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			header('Cache-Control: max-age=0');

			$writer->save('php://output');
			// echo json_encode(array("status" => TRUE));			
		}else{
			$this->index();
		}

	}
    
}
?>