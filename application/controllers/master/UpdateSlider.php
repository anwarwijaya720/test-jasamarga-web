<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UpdateSlider extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('master/m_slider');
	}

	public function index()
	{
		$data['view'] = 'conten/master/slider';
		$this->load->view('templates/base_template', $data);
	}

	function slider()
	{
		$data = $this->m_slider->getSlider();
		echo json_encode($data);
	}

	function save()
	{
		$imgSlider = "";
		$config['upload_path'] = "./assets/images/slider";
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('berkas')) {
			$error = array('error' => $this->upload->display_errors());			
		} else {
			$data = array('upload_data' => $this->upload->data());
			$img = $data['upload_data']['file_name'];			
			$imgSlider = URL_IMAGE.'assets/images/slider/'.$img;
		}

		$session    = $this->session->userdata('username');
		$date       = date("Y-m-d h:i:s");
		$des		= $this->input->post('des');
		
		$this->db->set('slider_id', 'UUID()', FALSE);
		$data = array(
			'image' => $imgSlider,
			'deskripsi' => $des,
			'insert_date' => $date,
			'insert_by' => $session,
		);

		$this->m_slider->save($data);			
        redirect('master/UpdateSlider');
	}

	function edit($slider_id){
		$where = array('slider_id' => $slider_id);
		$data['getSlider'] = $this->m_slider->getSliderId($where);		
		$data['view'] = 'conten/master/slider_update';
		$this->load->view('templates/base_template', $data);		
	}

	function editAction($slider_id){

		$des	   	= $this->input->post('des');
		$session    = $this->session->userdata('username');
		$date       = date("Y-m-d h:i:s");

		$imgSlider = "";
		$config['upload_path'] = "./assets/images/slider";
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
			
		if (!$this->upload->do_upload('berkas')) {
			$error = array('error' => $this->upload->display_errors());			
		} else {
			$data = array('upload_data' => $this->upload->data());
			$img = $data['upload_data']['file_name'];
			$imgSlider = URL_IMAGE.'assets/images/slider/'.$img;
		}
		
		$data = array(
			'deskripsi' => $des,
			'insert_date' => $date,
			'insert_by' => $session,
		);

		if(!empty($imgSlider)){
            $data['image'] = $imgSlider;
		}
		
		$where = array(
			'slider_id' => $slider_id
		);

		$this->m_slider->update($data, $where);
		
        redirect('master/UpdateSlider');

	}

	function delete($slider_id)
	{
		$where = array('slider_id' => $slider_id);
		$data['get'] = $this->m_slider->getImgSlider($where);
		$data = $data['get'];
		if(!empty($data[0]->slider_img)){
			$cekImgInFolder = file_exists("assets/images/slider/".$data[0]->slider_img);
			if ($cekImgInFolder != null) {
				unlink("assets/images/slider/".$data[0]->slider_img);
			}						
		}
		
		$response = $this->m_slider->delete($where);
		if ($response == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}
}
			