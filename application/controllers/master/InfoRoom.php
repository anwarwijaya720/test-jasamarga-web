<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class InfoRoom extends CI_Controller {

    public function __construct() {
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login"));
		}
        $this->load->model('master/m_info_room');
	}

	public function index()
	{
		//===== NICU ==========================//
		$data['nicu']			= $this->m_info_room->stock_nicu();
		$data['getNicu']		= $this->m_info_room->get_nicu();
		//===== Ruang Intensif ================//		
		$data['icu']			= $this->m_info_room->stock_icu();
		$data['getIcu']			= $this->m_info_room->get_icu();
		//===== Ruang HCU ======================//
		$data['hcu']			= $this->m_info_room->stock_hcu();
		$data['getHcu']			= $this->m_info_room->get_hcu();
		//===== Ruang Perawatan ================//
		$data['ward']			= $this->m_info_room->stock_ward();
		$data['getWard']		= $this->m_info_room->get_ward();
		//===== Ruang Isolasi IGD Ponek ========//
		$data['igdPonek']		= $this->m_info_room->stock_igd_ponek();
		$data['getIgdPonek']	= $this->m_info_room->get_igd_ponek();	
		//===== Covid ==========================//
		$data['covid']			= $this->m_info_room->covid();
		$data['getCovid']		= $this->m_info_room->getCovid();	

		$data['view'] 			= 'conten/master/info_room';
		$this->load->view('templates/base_template_public', $data);
		
	}

}

?>