<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {
		parent::__construct();

        $this->load->model('master/m_register');
	}

	public function index()
	{
		$data['view'] = 'conten/master/register';
		$this->load->view('templates/base_template', $data);
    }

    function getUsers(){
		$data = $this->m_register->getUsers();
		echo json_encode($data);
    }
    
}

?>