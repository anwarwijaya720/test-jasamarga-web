<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('master/m_profile');
	}

	public function index()
	{
		$user_id = $this->session->userdata('user_id');
		$where = array(
			'user_id' => $user_id
		);
		$data['getRs'] = $this->m_profile->getUser($where);
		$data['view'] = 'conten/master/profile';
		$this->load->view('templates/base_template', $data);
	}

	function Save()
	{
		$username = $this->input->post('user');
		$noTlp = $this->input->post('noTlp');
		$pass = md5($this->input->post('pass'));
		$conPass = $this->input->post('conPass');

		$user_id = $this->session->userdata('user_id');
		$where = array(
			'user_id' => $user_id
		);

		$data = array(
			'username' => $username,
			'no_hp' => $noTlp,
		);
		
		if (!empty($pass)) {
			if ($conPass == $pass) {
				$data['password'] = $pass;
			}
		}

		$response = $this->m_profile->update($data, $where);
		if ($response == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}
}
