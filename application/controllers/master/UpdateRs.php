<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UpdateRs extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("Login"));
        }
        $this->load->model('master/m_rs');
    }

    public function index()
    {
        $data['view'] = 'conten/master/rs';
        $this->load->view('templates/base_template', $data);
    }

    function getRs()
    {
        $data = $this->m_rs->getRs();
        echo json_encode($data);
    }

    function viewRs(){
        $url = base_url("master/UpdateRs/edit/");
        $list = $this->m_rs->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->rs_name;
            $imgRs = null;
            if(!empty($field->img_rs)){
               $imgRs = $field->img_rs;
            }else{
               $imgRs = base_url(img_default);
            }
            $row[] = '<img src="'.$imgRs.'"style="height:50px;width:50px;" />';
            $row[] = $field->rs_type;
            $row[] = $field->level_maternal;
            $row[] = $field->no_hp_rs;
            $row[] = $field->long;
            $row[] = $field->lat;
            $row[] = $field->address;
            $row[] = '
                        <a href="'.$url.''.$field->rs_id.'" class="btn btn-info btn-sm item_edit">Edit</a>
                    ';
                    
            $data[] = $row;

        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_rs->count_all(),
            "recordsFiltered" => $this->m_rs->count_filtered(),
            "data" => $data,
        );

        //output dalam format JSON
        echo json_encode($output);
    }

    function edit($rsId)
    {
        $where = array(
            'rs_id' => $rsId,
        );
        $data['getRs'] = $this->m_rs->getRsWhere($where);
        $data['view'] = 'conten/master/rs_update';
        $this->load->view('templates/base_template', $data);
    }

    function add(){
        $data['view'] = 'conten/master/rs_add';
        $this->load->view('templates/base_template', $data);
    }

    function addAction(){

        $imgRs = "";
		$config['upload_path'] = "./assets/images/rs";
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '5000';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('berkas')) {
			$error = array('error' => $this->upload->display_errors());			
		} else {
			$data = array('upload_data' => $this->upload->data());
			$img = $data['upload_data']['file_name'];			
			$imgRs = URL_IMAGE.'assets/images/rs/'.$img;
		}

        $session    = $this->session->userdata('username');
        $date       = date("Y-m-d h:i:s");
        $rsNme      = $this->input->post('rsName');
        $tipe       = $this->input->post('tipe');
        $tlp        = $this->input->post('tlp');
        $maternal   = $this->input->post('maternal');
        $long       = $this->input->post('long');
        $lat        = $this->input->post('lat');
        $alamat     = $this->input->post('alamat');        
        
        $this->db->set('rs_id', 'UUID()', FALSE);
        $data = array(
            'rs_name' => $rsNme,
            'rs_type' => $tipe,
            'no_hp_rs' => $tlp,
            'level_maternal' => $maternal,
            'long' => $long,
            'lat' => $lat,
            'address' => $alamat,
            'img_rs' =>  $imgRs,
            'updated_date' => $date,
            'updated_by' => $session,
        );
        $this->m_rs->add($data);
        redirect('master/UpdateRs');

    }

    function editAction($rsId)
    {
        $newImgRs = "";
        $imgBoxRs = $this->input->post('imgBoxRs');

        $config['upload_path'] = "./assets/images/rs";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('berkas')) {
            $error = array('error' => $this->upload->display_errors());
            // print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $ImgRs = $data['upload_data']['file_name'];
            $newImgRs = $ImgRs;
            if ($imgBoxRs != $newImgRs) {
                $cekImgInFolder = file_exists("assets/images/rs/" . $imgBoxRs);
                if ($cekImgInFolder != null) {
                    unlink("assets/images/rs/" . $imgBoxRs);
                }
            }
        }

        $session    = $this->session->userdata('username');
        $date       = date("Y-m-d h:i:s");
        $rsNme      = $this->input->post('rsName');
        $tipe       = $this->input->post('tipe');
        $tlp        = $this->input->post('tlp');
        $maternal   = $this->input->post('maternal');
        $long       = $this->input->post('long');
        $lat        = $this->input->post('lat');
        $alamat     = $this->input->post('alamat');        

        $data = array(
            'rs_name' => $rsNme,
            'rs_type' => $tipe,
            'no_hp_rs' => $tlp,
            'level_maternal' => $maternal,
            'long' => $long,
            'lat' => $lat,
            'address' => $alamat,
            'updated_date' => $date,
            'updated_by' => $session,
        );
        
        if(!empty($newImgRs)){
            $data['img_rs'] = URL_IMAGE.'assets/images/rs/'.$newImgRs;
        }

        $where = array('rs_id' => $rsId);
        $this->m_rs->update($data, $where);
        redirect('master/UpdateRs');
    }
}
