<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');        
        $this->load->model('master/M_Setting');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login"));
		}		
        $this->load->model('m_login');
	}

	public function index()
	{
        $data['getAllRole'] = $this->M_Setting->getSelectRole();
        $data['getRole'] = $this->M_Setting->getUserRole();
		$data['view'] = 'conten/master/setting';
		$this->load->view('templates/base_template', $data);
    }

    
}

?>