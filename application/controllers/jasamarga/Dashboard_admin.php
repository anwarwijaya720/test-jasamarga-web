<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_admin extends CI_Controller {

    public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['view'] = 'jasamarga/dashboard_admin';
		$this->load->view('templates/base_template', $data);
	}

}


?>