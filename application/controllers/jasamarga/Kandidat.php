<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kandidat extends CI_Controller {

    public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['view'] = 'jasamarga/kandidat';
		$this->load->view('templates/base_template', $data);		
	}

	public function detail()
	{
		$data['view'] = 'jasamarga/kandidat_detail';
		$this->load->view('templates/base_template', $data);		
	}

}

?>