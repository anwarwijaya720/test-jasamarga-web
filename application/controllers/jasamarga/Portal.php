<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

    public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['view'] = 'Index';
		$this->load->view('templates/base_template', $data);		
	}

}


?>