<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ExamplePagination extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('report/m_pagination');
	}

	public function index()
	{
		$data['getRs'] = $this->m_pagination->getRs();
		$data['view'] = 'conten/report/pagination';
		$this->load->view('templates/base_template', $data);
	}

	function getUsers(){
		// $data = $this->m_pagination->getRs();
		// echo json_encode($data);
		
		// Search term
		$searchTerm = $this->input->post('searchTerm');

		// Get users
		$response = $this->m_pagination->getData($searchTerm);
  
		echo json_encode($response);
		// print_r($response);
	}
}