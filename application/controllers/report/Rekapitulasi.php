<?php

defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Rekapitulasi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('report/m_rekapitulasi');
	}

	public function index()
	{
		$data['getRs'] = $this->m_rekapitulasi->getRs();
		$data['view'] = 'conten/report/rekapitulasi';
		$this->load->view('templates/base_template', $data);
	}

	function getUsers()
	{
		$data = $this->m_rekapitulasi->getRekapitulasi();
		echo json_encode($data);
	}

	function export()
	{
		$date 		= date_create($this->input->post('date1'));
		$dateTo 	= date_create($this->input->post('date2'));

		$date1 		= date_format($date, "Y-m-d");
		$date2 		= date_format($dateTo, "Y-m-d");

		$rs 		= $this->input->post('rs');
		$indikator 	= $this->input->post('indikator');

		$res = null;

		$cekTransaction = $this->m_rekapitulasi->cekTransaction();
		if (!empty($cekTransaction)) {
			//MOM
			if ($indikator == "rujukMom") {
				$res = $this->m_rekapitulasi->getRekapitulasiMom($date1, $date2, $rs);
			}
			if ($indikator == "rujukMomOnsite") {
				$res = $this->m_rekapitulasi->getRekapitulasiMomOnsite($date1, $date2, $rs);
			}
			if ($indikator == "rujukMomRoom") {
				$res = $this->m_rekapitulasi->getRekapitulasiMomRoom($date1, $date2, $rs);
			}
			//NEONATAl
			if ($indikator == "rujukNeonatal") {
				$res = $this->m_rekapitulasi->getRekapitulasiNeonatal($date1, $date2, $rs);
			}
			if ($indikator == "rujukNeonatalOnsite") {
				$res = $this->m_rekapitulasi->getRekapitulasiNeonatalOnsite($date1, $date2, $rs);
			}
			if ($indikator == "rujukNeonatalRoom") {
				$res = $this->m_rekapitulasi->getRekapitulasiNeonatalRoom($date1, $date2, $rs);
			}
			// DLL
			if ($indikator == "updateLayanan") {
				$this->multiSheet($rs, $date1, $date2);
			}
			if ($indikator == "rujukBatal") {
				$res = $this->m_rekapitulasi->getRekapitulasiBatal($date1, $date2, $rs);
			}
		} else {
			redirect(base_url(("report/Rekapitulasi")));
		}

		if (!empty($res)) {

			$spreadsheet = new Spreadsheet();
			$Colum  = $spreadsheet->getActiveSheet();
			$excel    = $spreadsheet->setActiveSheetIndex(0);

			// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
			$style_col = array(
				'font' => array('bold' => true), // Set font nya jadi bold
				'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);
			// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
			$style_row = array(
				'alignment' => array(
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
				),
				'borders' => array(
					'top' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border top dengan garis tipis
					'right' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),  // Set border right dengan garis tipis
					'bottom' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN), // Set border bottom dengan garis tipis
					'left' => array('style'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN) // Set border left dengan garis tipis
				)
			);
			$spreadsheet->setActiveSheetIndex(0)->setCellValue('A1', "REKAPITULASI"); // Set kolom A1 dengan tulisan "DATA SISWA"
			if ($indikator != "rujukBatal" || $indikator == "updateLayanan") {
				$Colum->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
			} else {
				$Colum->mergeCells('A1:D1');
			}
			$Colum->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
			$Colum->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
			$Colum->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
			// Buat header tabel nya pada baris ke 3
			if ($indikator != "rujukBatal" || $indikator == "updateLayanan") {
				$excel->setCellValue('A3', "NO");
				$excel->setCellValue('B3', "INSTANSI");
				$excel->setCellValue('C3', "KEBUTUHAN ");
				$excel->setCellValue('D3', "TOTAL");
				$excel->setCellValue('E3', "TANGGAL");
			} else {
				$excel->setCellValue('A3', "NO");
				$excel->setCellValue('B3', "INSTANSI");
				$excel->setCellValue('C3', "TOTAL");
				$excel->setCellValue('D3', "TANGGAL");
			}
			// Apply style header yang telah kita buat tadi ke masing-masing kolom header

			$excel->getStyle('A3')->applyFromArray($style_col);
			$excel->getStyle('B3')->applyFromArray($style_col);
			$excel->getStyle('C3')->applyFromArray($style_col);
			$excel->getStyle('D3')->applyFromArray($style_col);
			$excel->getStyle('E3')->applyFromArray($style_col);

			//=================================================================//
			$no = 1;
			$numrow = 4;
			foreach ($res as $data) {

				if ($indikator != "rujukBatal" || $indikator == "updateLayanan") {
					$excel->setCellValue('A' . $numrow, $no);
					$excel->setCellValue('B' . $numrow, $data->rs_name);
					$excel->setCellValue('C' . $numrow, $data->kebutuhan);
					$excel->setCellValue('D' . $numrow, $data->total);
					$excel->setCellValue('E' . $numrow, $data->tanggal);

					$excel->getStyle('A' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('B' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('C' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('D' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('E' . $numrow)->applyFromArray($style_row);
				} else {
					$excel->setCellValue('A' . $numrow, $no);
					$excel->setCellValue('B' . $numrow, $data->rs_name);
					$excel->setCellValue('C' . $numrow, $data->total);
					$excel->setCellValue('D' . $numrow, $data->tanggal);

					$excel->getStyle('A' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('B' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('C' . $numrow)->applyFromArray($style_row);
					$excel->getStyle('D' . $numrow)->applyFromArray($style_row);
				}

				$no++;
				$numrow++;
			}

			// Set width kolom
			$excel->getColumnDimension('A')->setWidth(5);
			$excel->getColumnDimension('B')->setWidth(40);
			$excel->getColumnDimension('C')->setWidth(30);
			$excel->getColumnDimension('D')->setWidth(10);
			$excel->getColumnDimension('E')->setWidth(20);

			// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
			$excel->getDefaultRowDimension()->setRowHeight(-1);
			// Set orientasi kertas jadi LANDSCAPE
			$excel->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
			// Set judul file excel nya
			$excel->setTitle("Report Rekapitulasi");

			$writer = new Xlsx($spreadsheet);
			$filename = 'Rekapitulasi';
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			header('Cache-Control: max-age=0');
			$writer->save('php://output');
		} else {
			redirect(base_url(("report/Rekapitulasi")));
		}
	}

	function multiSheet($rs, $date1, $date2)
	{

		$room = $this->m_rekapitulasi->rekapRoom($rs, $date1, $date2);
		$blood = $this->m_rekapitulasi->rekapBlood($rs, $date1, $date2);
		$doc = $this->m_rekapitulasi->rekapDoc($rs, $date1, $date2);

		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Add some data
		$row_1 = $spreadsheet->setActiveSheetIndex(0);
		//Colum
		$row_1->setCellValue('A1', 'NO');
		$row_1->setCellValue('B1', 'INSATANSI');
		$row_1->setCellValue('C1', 'UPDATED DATE');

		if (!empty($room)) {
			$no = 1;
			$numrow = 2;
			foreach ($room as $data) {
				$row_1->setCellValue('A' . $numrow, $no);
				$row_1->setCellValue('B' . $numrow, $data->rs_name);
				$row_1->setCellValue('C' . $numrow, $data->updated_date);

				$no++;
				$numrow++;
			}
		}
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Room');

		$spreadsheet->createSheet();
		// Add some data
		$row_2 = $spreadsheet->setActiveSheetIndex(1);
		//Colum
		$row_2->setCellValue('A1', 'NO');
		$row_2->setCellValue('B1', 'INSATANSI');
		$row_2->setCellValue('C1', 'UPDATED DATE');

		// Mengambil Data
		if (!empty($blood)) {
			$no = 1;
			$numrow = 2;
			foreach ($blood as $data) {
				$row_2->setCellValue('A' . $numrow, $no);
				$row_2->setCellValue('B' . $numrow, $data->rs_name);
				$row_2->setCellValue('C' . $numrow, $data->updated_date);

				$no++;
				$numrow++;
			}
		}
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Blood');

		$spreadsheet->createSheet();
		// Add some data
		$row_3 = $spreadsheet->setActiveSheetIndex(2);
		//Colum
		$row_3->setCellValue('A1', 'NO');
		$row_3->setCellValue('B1', 'INSATANSI');
		$row_3->setCellValue('C1', 'UPDATED DATE');

		if (!empty($doc)) {
			$no = 1;
			$numrow = 2;
			foreach ($doc as $data) {
				$row_3->setCellValue('A' . $numrow, $no);
				$row_3->setCellValue('B' . $numrow, $data->rs_name);
				$row_3->setCellValue('C' . $numrow, $data->update_date);

				$no++;
				$numrow++;
			}
		}
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Doctor');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Rekapitulasi.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
	}

	// ============== Users ===================// 
	function export_super_users()
	{
		$spreadsheet = new Spreadsheet(1);
		$Colum  = $spreadsheet->getActiveSheet();
		$row    = $spreadsheet->setActiveSheetIndex(0);

		$Colum->setTitle('Oke Bos');
		$Colum->setCellValue('A1', 'Website Stats Page');
		$Colum->getStyle("A1")->getFont()->setSize(16);
		//Colum
		$row->setCellValue('A1', 'NO');
		$row->setCellValue('B1', 'NAME');
		$row->setCellValue('C1', 'NO TLP');
		$row->setCellValue('D1', 'CREATED DATE');
		$row->setCellValue('E1', 'CREATED BY');

		// Mengambil Data
		$data_product = $this->m_rekapitulasi->getUsers();
		$no = 1;
		$numrow = 2;
		foreach ($data_product as $data) {
			$row->setCellValue('A' . $numrow, $no);
			$row->setCellValue('B' . $numrow, $data->username);
			$row->setCellValue('C' . $numrow, $data->no_hp);
			$row->setCellValue('D' . $numrow, $data->created_date);
			$row->setCellValue('E' . $numrow, $data->created_by);

			$no++;
			$numrow++;
		}

		$styleArray = [
			'font' => [
				'bold' => true,
			],
			'alignment' => [
				// 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				// 'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
		];

		// $numrow = $numrow - 1;
		$Colum->getStyle('A1:E' . $numrow)->applyFromArray($styleArray);
		// Set width kolom
		$row->getColumnDimension('A')->setWidth(5); // Set width kolom A
		$row->getColumnDimension('B')->setWidth(30); // Set width kolom B
		$row->getColumnDimension('C')->setWidth(30); // Set width kolom C
		$row->getColumnDimension('D')->setWidth(30); // Set width kolom D
		$row->getColumnDimension('E')->setWidth(30); // Set width kolom D
		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$row->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$row->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

		// $writer = new Xlsx($spreadsheet);
		// $filename = 'Report Users';
		// header('Content-Type: application/vnd.ms-excel');
		// header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		// header('Cache-Control: max-age=0');
		// $writer->save('php://output');
		$extension = 'Xlsx';
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, $extension);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment; filename=\"nganu.{$extension}\"");
		$writer->save('php://output');
		exit();
	}

	public function anu()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		$writer = new Xlsx($spreadsheet);
		$writer->save('hello world666.xlsx');
	}
}
