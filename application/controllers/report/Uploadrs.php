<?php

defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class Uploadrs extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('report/m_uploadrs');
    }

    function index(){
		$data['view'] = 'conten/report/uploadrs';
		$this->load->view('templates/base_template', $data);
	}
	
	function upload(){
		$file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		if(isset($_FILES['berkas']['name']) && in_array($_FILES['berkas']['type'], $file_mimes)) {
		
			$arr_file = explode('.', $_FILES['berkas']['name']);
			$extension = end($arr_file);
		
			if('csv' == $extension) {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			}
		
			$spreadsheet = $reader->load($_FILES['berkas']['tmp_name']);
			
			$sheetData = $spreadsheet->getActiveSheet()->toArray();				

			for($i = 1;$i < count($sheetData);$i++)
			{
				$kode_rs = $sheetData[$i]['0'];
				$rs_name = $sheetData[$i]['1'];
				$long = $sheetData[$i]['2'];
				$lat = $sheetData[$i]['3'];
				$conLong = str_replace("'"," ",$long);  
				$conLat = str_replace('"'," ",$lat);

				$data = array(
					"kode_rs" => $kode_rs,
					"rs_name" => $rs_name,
					// "long" => $conLong == '' || $conLong == null ? $i   : $conLong,
					// "lat" => $conLat == '' || $conLat == null ? $i : $conLat					
					"long" => $conLong,
					"lat" => $conLat
				);				
				$this->db->set('rs_id', 'UUID()', FALSE);
				$this->m_uploadrs->save($data);
			}
		
			
			echo "upload data oke";

		}

		// $res = $this->m_uploadrs->inFile();
		// print_r($res);
	}


}