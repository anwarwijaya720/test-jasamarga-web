<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class StockBlood extends CI_Controller {

    public function __construct() {
        parent::__construct();        
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login"));
		}		
        $this->load->model('m_login');
        $this->load->model('transaction/m_blood');
	}

	public function index()
	{
		$data['getRs'] = $this->m_blood->getRs();		
		$data['view'] = 'conten/transaction/stock_blood';
		$this->load->view('templates/base_template', $data);
	}
		
	function getSelectRs($rsId){
		$where = array('mr.rs_id' => $rsId);
		$data = $this->m_blood->getRsWhere($where);
		echo json_encode($data);
	}
	
	function Save()
	{
		$date_format = date("Y-m-d h:i:s");
		$session_name = $this->session->userdata('username');
		$rs_id = $this->input->post('rs');
		$where = array('rs_id'  => $rs_id);
		$result = $this->m_blood->getWhere($where);

		// Blood A
		$aWb = $this->input->post('aWb');
		$aPrc = $this->input->post('aPrc');
		$aTc = $this->input->post('aTc');
		// Blood AB
		$abWb = $this->input->post('abWb');
		$abPrc = $this->input->post('abPrc');
		$abTc = $this->input->post('abTc');
		// Blood B
		$bWb = $this->input->post('bWb');
		$bPrc = $this->input->post('bPrc');
		$bTc = $this->input->post('bTc');
		// Blood O
		$oWb = $this->input->post('oWb');
		$oPrc = $this->input->post('oPrc');
		$oTc = $this->input->post('oTc');

		if (!empty($result)) {
			// $this->db->set('stock_blood_id', 'UUID()', FALSE);
			$data = array(
				//blood A
				'blood_aWb' => $aWb,
				'blood_aPrc' => $aPrc,
				'blood_aTc' => $aTc,
				//blood B
				'blood_abWb' => $abWb,
				'blood_abPrc' => $abPrc,
				'blood_abTc' => $abTc,
				//blood AB
				'blood_bWb' => $bWb,
				'blood_bPrc' => $bPrc,
				'blood_bTc' => $bTc,
				//blood O				
				'blood_oWb' => $oWb,
				'blood_oPrc' => $oPrc,
				'blood_oTc' => $oTc,
				//=========//
				'updated_date' => $date_format,
				'updated_by' => $session_name,
			);
			$response=$this->m_blood->update_stock_blood($data, $where);		
		} else {			
			$this->db->set('stock_blood_id', 'UUID()', FALSE);
			$data = array(
				'rs_id' => $rs_id,
				//blood A
				'blood_aWb' => $aWb,
				'blood_aPrc' => $aPrc,
				'blood_aTc' => $aTc,
				//blood B
				'blood_abWb' => $abWb,
				'blood_abPrc' => $abPrc,
				'blood_abTc' => $abTc,
				//blood AB
				'blood_bWb' => $bWb,
				'blood_bPrc' => $bPrc,
				'blood_bTc' => $bTc,
				//blood O				
				'blood_oWb' => $oWb,
				'blood_oPrc' => $oPrc,
				'blood_oTc' => $oTc,
				//=========//
				'updated_date' => $date_format,
				'updated_by' => $session_name,
			);			
			$response=$this->m_blood->save_stock_blood($data);
		}
		if ($response == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}
    
}

?>