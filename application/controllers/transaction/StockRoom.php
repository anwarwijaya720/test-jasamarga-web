<?php

defined('BASEPATH') or exit('No direct script access allowed');

class StockRoom extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('m_login');
		$this->load->model('transaction/m_room');
	}

	public function index()
	{
		$data['getRs'] = $this->m_room->getRs();
		$data['view'] = 'conten/transaction/stock_room';
		$this->load->view('templates/base_template', $data);
	}

	function getStock($rsId){
		$where = array('mr.rs_id' => $rsId);
		$data = $this->m_room->getRoom($where);
		echo json_encode($data);
	}

	function Save()
	{
		date_default_timezone_set('Asia/Jakarta');
		$session_name = $this->session->userdata('username');
		$date_format = date("Y-m-d h:i:s");
		$rs_id = $this->input->post('rs');
		$where = array('rs_id'  => $rs_id);
		$result = $this->m_room->getStockRoom($where);

		$nicu = $this->input->post('roomNICU');
		$icu = $this->input->post('roomICU');
		$hcu = $this->input->post('roomHCU');
		$ward = $this->input->post('ward');
		$mom = $this->input->post('roomIsolationMother');
		$beby = $this->input->post('roomIsloationBebies');
		$igdPonek = $this->input->post('roomIsloationIgdPonek');
		$icuCovid = $this->input->post('roomIcuCovid');
		$receivingCovid = $this->input->post('roomReceivingCovid');
		$picuCOvid = $this->input->post('roomPicuCovid');
		$nicuCovid = $this->input->post('roomNicuCovid');

		if (!empty($result[0]->rs_id)) {
			$data = array(
				'icu'  => $icu,
				'nicu' => $nicu,
				'hcu' => $hcu,
				'ruang_perawatan' => $ward,
				'ruang_isolasi_ibu' => $mom,
				'ruang_isolasi_bayi' => $beby,
				'isolasi_igd_ponek' => $igdPonek,
				'icu_covid' => $icuCovid,
				'perina_covid' => $receivingCovid,
				'picu_covid' => $picuCOvid,
				'nicu_covid' => $nicuCovid,
				'updated_date' => $date_format,
				'updated_by' => $session_name,
			);
			$response=$this->m_room->update_stock_room($data, $where);			
		} else {			
			$this->db->set('stock_room_id', 'UUID()', FALSE);
			$data = array(
				'rs_id' => $rs_id,
				'icu'  => $icu,
				'nicu' => $nicu,
				'hcu' => $hcu,
				'ruang_perawatan' => $ward,
				'ruang_isolasi_ibu' => $mom,
				'ruang_isolasi_bayi' => $beby,
				'isolasi_igd_ponek' => $igdPonek,
				'icu_covid' => $icuCovid,
				'perina_covid' => $receivingCovid,
				'picu_covid' => $picuCOvid,
				'nicu_covid' => $nicuCovid,
				'updated_date' => $date_format,
				'updated_by' => $session_name,
			);			
			$response=$this->m_room->save_stock_room($data);				
		}
		if ($response == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}
}
