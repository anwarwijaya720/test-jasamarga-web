<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DokterSp extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login"));
		}
		$this->load->model('m_login');
		$this->load->model('transaction/m_dokter');
	}

	public function index()
	{
		$data['getRs'] = $this->m_dokter->getRs();
		$data['getSp'] = $this->m_dokter->getSp();
		$data['view'] = 'conten/transaction/dokter_sp';
		$this->load->view('templates/base_template', $data);
	}

	function coba(){
		$date = "11/04/2020";
		$dateSchedul = "";
		if(!empty($date)){
			$format = date_create($date);
			$dateSchedul = date_format($format, "Y-m-d");
		}else{
			$dateSchedul = null;
		}

		if(!empty($date)){
			echo $dateSchedul;			
			echo "oke";
		}else{
			echo $dateSchedul;			
			echo "gagal	";
		}
		// echo $dateSchedul;
	}

	function schedule(){
		$rsId = $this->input->post('rs');
		$spId = $this->input->post('sp');
		$date = $this->input->post('dateSchedule');
		$dateSchedul = "";
		if(!empty($date)){
			$format = date_create($date);
			$dateSchedul = date_format($format, "Y-m-d");
		}else{
			$dateSchedul = null;
		}

		// $rsId = 'ff24274b-62b9-4a06-8c67-b330fcf5b78e';
		// $spId = 'de36648b-4747-47a5-a251-aeb437fa2443';
		// $date1 = '2020-11-05';

		$res = $this->m_dokter->getDoctor($spId);

		$where = array(
			'rs_id' => $rsId,
			'date' => $dateSchedul,
		);

		if(!empty($res)){
			if($res[0]->doctor_name == "SP.Obgyn + SP.An") { $where['obgyn_anak'] = 1; }
			if($res[0]->doctor_name == "Obgyn") { $where['obgyn'] = 1; }
			if($res[0]->doctor_name == "SP.Anak") { $where['anak'] = 1; }
			if($res[0]->doctor_name == "SP.PD") { $where['pd'] = 1; }
			if($res[0]->doctor_name == "SP.JP") { $where['jp'] = 1; }
			if($res[0]->doctor_name == "SP.P") { $where['spp'] = 1; }
			if($res[0]->doctor_name == "Anastesi") { $where['anastesi'] = 1; }
			if($res[0]->doctor_name == "SP.Dalam") { $where['dalam'] = 1; }
		}

		$where['status'] = 'onsite';
		$data['getOnsite'] = $this->m_dokter->getScheduleByStatus($where);

		unset($where['onsite']);
		$where['status'] = 'oncall';
		$data['getOcall'] = $this->m_dokter->getScheduleByStatus($where);

		unset($where['oncall']);
		$where['status'] = 'off';
		$data['getOff'] = $this->m_dokter->getScheduleByStatus($where);	

		$data['getPrwt'] = null;
		if (!empty($res)) {
			if ($res[0]->doctor_name == "Anastesi") {
				$where['status'] = 'prwt';
				$data['getPrwt'] = $this->m_dokter->getScheduleByStatus($where);
			}
		}

		if($data['getOnsite'] != null || $data['getOcall'] != null || $data['getOff'] != null || $data['getPrwt'] != null){
			$data['status'] = TRUE;
		}else{
			$data['status'] = FALSE;
		}

		echo json_encode($data);

	}

	function update()
	{
		$session_name = $this->session->userdata('username');
		$rsId = $this->input->post('rs');
		$spId = $this->input->post('sp');
		$date = date_create($this->input->post('dateSchedule'));;
		$dateSchedule = date_format($date, "Y-m-d");;
		
		// $rsId = 'ff24274b-62b9-4a06-8c67-b330fcf5b78e';
		// $spId = 'de36648b-4747-47a5-a251-aeb437fa2443';		
		// $date1 = '2020-11-02';

		// jam 1
		$onsite_1 = $this->input->post('onSite1');
		$prwt_1 = $this->input->post('prwt1');
		$oncall_1 = $this->input->post('onCall1');
		$off_1 = $this->input->post('off1');
		// jam 2
		$onsite_2 = $this->input->post('onSite2');
		$prwt_2 = $this->input->post('prwt2');
		$oncall_2 = $this->input->post('onCall2');
		$off_2 = $this->input->post('off2');
		// jam 3
		$onsite_3 = $this->input->post('onSite3');
		$prwt_3 = $this->input->post('prwt3');
		$oncall_3 = $this->input->post('onCall3');
		$off_3 = $this->input->post('off3');
		// jam 3
		$onsite_4 = $this->input->post('onSite4');
		$prwt_4 = $this->input->post('prwt4');
		$oncall_4 = $this->input->post('onCall4');
		$off_4 = $this->input->post('off4');
		// jam 5
		$onsite_5 = $this->input->post('onSite5');
		$prwt_5 = $this->input->post('prwt5');
		$oncall_5 = $this->input->post('onCall5');
		$off_5 = $this->input->post('off5');		
		// jam 6
		$onsite_6 = $this->input->post('onSite6');
		$prwt_6 = $this->input->post('prwt6');
		$oncall_6 = $this->input->post('onCall6');
		$off_6 = $this->input->post('off6');
		// jam 7
		$onsite_7 = $this->input->post('onSite7');
		$prwt_7 = $this->input->post('prwt7');
		$oncall_7 = $this->input->post('onCall7');
		$off_7 = $this->input->post('off7');
		// jam 8
		$onsite_8 = $this->input->post('onSite8');
		$prwt_8 = $this->input->post('prwt8');
		$oncall_8 = $this->input->post('onCall8');
		$off_8 = $this->input->post('off8');
		// jam 9
		$onsite_9 = $this->input->post('onSite9');
		$prwt_9 = $this->input->post('prwt9');
		$oncall_9 = $this->input->post('onCall9');
		$off_9 = $this->input->post('off9');
		// jam 10
		$onsite_10 = $this->input->post('onSite10');
		$prwt_10 = $this->input->post('prwt10');
		$oncall_10 = $this->input->post('onCall10');
		$off_10 = $this->input->post('off10');
		// jam 11
		$onsite_11 = $this->input->post('onSite11');
		$prwt_11 = $this->input->post('prwt1');
		$oncall_11 = $this->input->post('onCall11');
		$off_11 = $this->input->post('off11');
		// jam 12
		$onsite_12 = $this->input->post('onSite12');
		$prwt_12 = $this->input->post('prwt12');
		$oncall_12 = $this->input->post('onCall12');
		$off_12 = $this->input->post('off12');
		// jam 13
		$onsite_13 = $this->input->post('onSite13');
		$prwt_13 = $this->input->post('prwt13');
		$oncall_13 = $this->input->post('onCall13');
		$off_13 = $this->input->post('off13');
		// jam 13
		$onsite_14 = $this->input->post('onSite14');
		$prwt_14 = $this->input->post('prwt14');
		$oncall_14 = $this->input->post('onCall14');
		$off_14 = $this->input->post('off14');
		// jam 15
		$onsite_15 = $this->input->post('onSite15');
		$prwt_15 = $this->input->post('prwt15');
		$oncall_15 = $this->input->post('onCall15');
		$off_15 = $this->input->post('off15');		
		// jam 16
		$onsite_16 = $this->input->post('onSite16');
		$prwt_16 = $this->input->post('prwt16');
		$oncall_16 = $this->input->post('onCall16');
		$off_16 = $this->input->post('off16');
		// jam 17
		$onsite_17 = $this->input->post('onSite17');
		$prwt_17 = $this->input->post('prwt17');
		$oncall_17 = $this->input->post('onCall17');
		$off_17 = $this->input->post('off17');
		// jam 18
		$onsite_18 = $this->input->post('onSite18');
		$prwt_18 = $this->input->post('prwt18');
		$oncall_18 = $this->input->post('onCall18');
		$off_18 = $this->input->post('off18');
		// jam 19
		$onsite_19 = $this->input->post('onSite19');
		$prwt_19 = $this->input->post('prwt19');
		$oncall_19 = $this->input->post('onCall19');
		$off_19 = $this->input->post('off19');
		// jam 20
		$onsite_20 = $this->input->post('onSite20');
		$prwt_20 = $this->input->post('prwt20');
		$oncall_20 = $this->input->post('onCall20');
		$off_20 = $this->input->post('off20');
		// jam 21
		$onsite_21 = $this->input->post('onSite21');
		$prwt_21 = $this->input->post('prwt21');
		$oncall_21 = $this->input->post('onCall21');
		$off_21 = $this->input->post('off21');
		// jam 22
		$onsite_22 = $this->input->post('onSite22');
		$prwt_22 = $this->input->post('prwt22');
		$oncall_22 = $this->input->post('onCall22');
		$off_22 = $this->input->post('off22');
		// jam 23
		$onsite_23 = $this->input->post('onSite23');
		$prwt_23 = $this->input->post('prwt23');
		$oncall_23 = $this->input->post('onCall23');
		$off_23 = $this->input->post('off23');
		// jam 24
		$onsite_24 = $this->input->post('onSite24');
		$prwt_24 = $this->input->post('prwt24');
		$oncall_24 = $this->input->post('onCall24');
		$off_24 = $this->input->post('off24');
		
		//Example
		// $onsite_1 = 1;
		// $onsite_2 = 1;
		// $onsite_3 = 0;
		// $onsite_4 = 0;					

		//save schedule
		$data = array(
			'rs_id' => $rsId,
			'spesialis_id' => $spId,
			'date' => $dateSchedule,
			'updated_by' => $session_name
		);
		
		$where = array(
			'rs_id' => $rsId,
			'date' => $dateSchedule,
		);

		$responDC = $this->m_dokter->getDoctor($spId);
		
		if(!empty($responDC)){
			//save
			if($responDC[0]->doctor_name == "SP.Obgyn + SP.An") { $data['obgyn_anak'] = 1; }
			if($responDC[0]->doctor_name == "Obgyn") { $data['obgyn'] = 1; }
			if($responDC[0]->doctor_name == "SP.Anak") { $data['anak'] = 1; }
			if($responDC[0]->doctor_name == "SP.PD") { $data['pd'] = 1; }
			if($responDC[0]->doctor_name == "SP.JP") { $data['jp'] = 1; }
			if($responDC[0]->doctor_name == "SP.P") { $data['spp'] = 1; }
			if($responDC[0]->doctor_name == "Anastesi") { $data['anastesi'] = 1; }
			if($responDC[0]->doctor_name == "SP.Dalam") { $data['dalam'] = 1; }
			//update
			if($responDC[0]->doctor_name == "SP.Obgyn + SP.An") { $where['obgyn_anak'] = 1; }
			if($responDC[0]->doctor_name == "Obgyn") { $where['obgyn'] = 1; }
			if($responDC[0]->doctor_name == "SP.Anak") { $where['anak'] = 1; }
			if($responDC[0]->doctor_name == "SP.PD") { $where['pd'] = 1; }
			if($responDC[0]->doctor_name == "SP.JP") { $where['jp'] = 1; }
			if($responDC[0]->doctor_name == "SP.P") { $where['spp'] = 1; }
			if($responDC[0]->doctor_name == "Anastesi") { $where['anastesi'] = 1; }
			if($responDC[0]->doctor_name == "SP.Dalam") { $where['dalam'] = 1; }
		}

		$response_model = 0;
		// $responSc = null;
		
		//=========================== ONSITE ==================================//
		if($onsite_1 == 1) { $data['hour_1'] = 1; } else { $data['hour_1'] = 0; }
		if($onsite_2 == 1) { $data['hour_2'] = 1; } else { $data['hour_2'] = 0; }
		if($onsite_3 == 1) { $data['hour_3'] = 1; } else { $data['hour_3'] = 0; }
		if($onsite_4 == 1) { $data['hour_4'] = 1; } else { $data['hour_4'] = 0; }
		if($onsite_5 == 1) { $data['hour_5'] = 1; } else { $data['hour_5'] = 0; }
		if($onsite_6 == 1) { $data['hour_6'] = 1; } else { $data['hour_6'] = 0; }
		if($onsite_7 == 1) { $data['hour_7'] = 1; } else { $data['hour_7'] = 0; }
		if($onsite_8 == 1) { $data['hour_8'] = 1; } else { $data['hour_8'] = 0; }
		if($onsite_9 == 1) { $data['hour_9'] = 1; } else { $data['hour_9'] = 0; }
		if($onsite_10 == 1) { $data['hour_10'] = 1; } else { $data['hour_10'] = 0; }
		if($onsite_11 == 1) { $data['hour_11'] = 1; } else { $data['hour_11'] = 0; }
		if($onsite_12 == 1) { $data['hour_12'] = 1; } else { $data['hour_12'] = 0; }
		if($onsite_13 == 1) { $data['hour_13'] = 1; } else { $data['hour_13'] = 0; }
		if($onsite_14 == 1) { $data['hour_14'] = 1; } else { $data['hour_14'] = 0; }
		if($onsite_15 == 1) { $data['hour_15'] = 1; } else { $data['hour_15'] = 0; }
		if($onsite_16 == 1) { $data['hour_16'] = 1; } else { $data['hour_16'] = 0; }
		if($onsite_17 == 1) { $data['hour_17'] = 1; } else { $data['hour_17'] = 0; }
		if($onsite_18 == 1) { $data['hour_18'] = 1; } else { $data['hour_18'] = 0; }
		if($onsite_19 == 1) { $data['hour_19'] = 1; } else { $data['hour_19'] = 0; }
		if($onsite_20 == 1) { $data['hour_20'] = 1; } else { $data['hour_20'] = 0; }
		if($onsite_21 == 1) { $data['hour_21'] = 1; } else { $data['hour_21'] = 0; }
		if($onsite_22 == 1) { $data['hour_22'] = 1; } else { $data['hour_22'] = 0; }
		if($onsite_23 == 1) { $data['hour_23'] = 1; } else { $data['hour_23'] = 0; }
		if($onsite_24 == 1) { $data['hour_24'] = 1; } else { $data['hour_24'] = 0; }		
		$data['status'] = 'onsite';		
		$where['status'] = 'onsite';
		$responSc = $this->m_dokter->getWhereSchedule($where);
		if(!empty($responSc)){
			$response_onsite = $this->m_dokter->update_schedule($data, $where);			
		}else{
			$response_onsite = $this->m_dokter->save($data);				
		}

		if($response_onsite == 1){
			$response_model = 1;
		}
		//=========================== ONSITE AND ==================================//	

		//=========================== ONCALL ==================================//
		if($oncall_1 == 1) { $data['hour_1'] = 1; } else { $data['hour_1'] = 0; }
		if($oncall_2 == 1) { $data['hour_2'] = 1; } else { $data['hour_2'] = 0; }
		if($oncall_3 == 1) { $data['hour_3'] = 1; } else { $data['hour_3'] = 0; }
		if($oncall_4 == 1) { $data['hour_4'] = 1; } else { $data['hour_4'] = 0; }
		if($oncall_5 == 1) { $data['hour_5'] = 1; } else { $data['hour_5'] = 0; }
		if($oncall_6 == 1) { $data['hour_6'] = 1; } else { $data['hour_6'] = 0; }
		if($oncall_7 == 1) { $data['hour_7'] = 1; } else { $data['hour_7'] = 0; }
		if($oncall_8 == 1) { $data['hour_8'] = 1; } else { $data['hour_8'] = 0; }
		if($oncall_9 == 1) { $data['hour_9'] = 1; } else { $data['hour_9'] = 0; }
		if($oncall_10 == 1) { $data['hour_10'] = 1; } else { $data['hour_10'] = 0; }
		if($oncall_11 == 1) { $data['hour_11'] = 1; } else { $data['hour_11'] = 0; }
		if($oncall_12 == 1) { $data['hour_12'] = 1; } else { $data['hour_12'] = 0; }
		if($oncall_13 == 1) { $data['hour_13'] = 1; } else { $data['hour_13'] = 0; }
		if($oncall_14 == 1) { $data['hour_14'] = 1; } else { $data['hour_14'] = 0; }
		if($oncall_15 == 1) { $data['hour_15'] = 1; } else { $data['hour_15'] = 0; }
		if($oncall_16 == 1) { $data['hour_16'] = 1; } else { $data['hour_16'] = 0; }
		if($oncall_17 == 1) { $data['hour_17'] = 1; } else { $data['hour_17'] = 0; }
		if($oncall_18 == 1) { $data['hour_18'] = 1; } else { $data['hour_18'] = 0; }
		if($oncall_19 == 1) { $data['hour_19'] = 1; } else { $data['hour_19'] = 0; }
		if($oncall_20 == 1) { $data['hour_20'] = 1; } else { $data['hour_20'] = 0; }
		if($oncall_21 == 1) { $data['hour_21'] = 1; } else { $data['hour_21'] = 0; }
		if($oncall_22 == 1) { $data['hour_22'] = 1; } else { $data['hour_22'] = 0; }
		if($oncall_23 == 1) { $data['hour_23'] = 1; } else { $data['hour_23'] = 0; }
		if($oncall_24 == 1) { $data['hour_24'] = 1; } else { $data['hour_24'] = 0; }
		unset($data['onsite']);
		unset($where['onsite']);
		$data['status'] = 'oncall';
		$where['status'] = 'oncall';
		$responSc = $this->m_dokter->getWhereSchedule($where);
		if(!empty($responSc)){
			$response_oncall = $this->m_dokter->update_schedule($data, $where);			
		}else{
			$response_oncall = $this->m_dokter->save($data);
		}

		if($response_oncall == 1){
			$response_model = 1; 
		}

		//=========================== ONCALL AND ==================================//
		
		//=========================== OFF ==================================//
		if($off_1 == 1) { $data['hour_1'] = 1; } else { $data['hour_1'] = 0; }
		if($off_2 == 1) { $data['hour_2'] = 1; } else { $data['hour_2'] = 0; }
		if($off_3 == 1) { $data['hour_3'] = 1; } else { $data['hour_3'] = 0; }
		if($off_4 == 1) { $data['hour_4'] = 1; } else { $data['hour_4'] = 0; }
		if($off_5 == 1) { $data['hour_5'] = 1; } else { $data['hour_5'] = 0; }
		if($off_6 == 1) { $data['hour_6'] = 1; } else { $data['hour_6'] = 0; }
		if($off_7 == 1) { $data['hour_7'] = 1; } else { $data['hour_7'] = 0; }
		if($off_8 == 1) { $data['hour_8'] = 1; } else { $data['hour_8'] = 0; }
		if($off_9 == 1) { $data['hour_9'] = 1; } else { $data['hour_9'] = 0; }
		if($off_10 == 1) { $data['hour_10'] = 1; } else { $data['hour_10'] = 0; }
		if($off_11 == 1) { $data['hour_11'] = 1; } else { $data['hour_11'] = 0; }
		if($off_12 == 1) { $data['hour_12'] = 1; } else { $data['hour_12'] = 0; }
		if($off_13 == 1) { $data['hour_13'] = 1; } else { $data['hour_13'] = 0; }
		if($off_14 == 1) { $data['hour_14'] = 1; } else { $data['hour_14'] = 0; }
		if($off_15 == 1) { $data['hour_15'] = 1; } else { $data['hour_15'] = 0; }
		if($off_16 == 1) { $data['hour_16'] = 1; } else { $data['hour_16'] = 0; }
		if($off_17 == 1) { $data['hour_17'] = 1; } else { $data['hour_17'] = 0; }
		if($off_18 == 1) { $data['hour_18'] = 1; } else { $data['hour_18'] = 0; }
		if($off_19 == 1) { $data['hour_19'] = 1; } else { $data['hour_19'] = 0; }
		if($off_20 == 1) { $data['hour_20'] = 1; } else { $data['hour_20'] = 0; }
		if($off_21 == 1) { $data['hour_21'] = 1; } else { $data['hour_21'] = 0; }
		if($off_22 == 1) { $data['hour_22'] = 1; } else { $data['hour_22'] = 0; }
		if($off_23 == 1) { $data['hour_23'] = 1; } else { $data['hour_23'] = 0; }
		if($off_24 == 1) { $data['hour_24'] = 1; } else { $data['hour_24'] = 0; }
		unset($data['oncall']);
		unset($where['oncall']);
		$data['status'] = 'off';			
		$where['status'] = 'off';
		$responSc = $this->m_dokter->getWhereSchedule($where);
		if(!empty($responSc)){
			$response_off = $this->m_dokter->update_schedule($data, $where);					
		}else{
			$response_off = $this->m_dokter->save($data);
		}

		if($response_off == 1){
			$response_model = 1; 
		}

		//=========================== OFF AND ==================================//			
		
		//=========================== OFF ==================================//
		if(!empty($responDC[0]->doctor_name)){
			if($responDC[0]->doctor_name == "Anastesi"){				
				//save schedule				
				if($prwt_1 == 1) { $data['hour_1'] = 1; } else { $data['hour_1'] = 0; }
				if($prwt_2 == 1) { $data['hour_2'] = 1; } else { $data['hour_2'] = 0; }
				if($prwt_3 == 1) { $data['hour_3'] = 1; } else { $data['hour_3'] = 0; }
				if($prwt_4 == 1) { $data['hour_4'] = 1; } else { $data['hour_4'] = 0; }
				if($prwt_5 == 1) { $data['hour_5'] = 1; } else { $data['hour_5'] = 0; }
				if($prwt_6 == 1) { $data['hour_6'] = 1; } else { $data['hour_6'] = 0; }
				if($prwt_7 == 1) { $data['hour_7'] = 1; } else { $data['hour_7'] = 0; }
				if($prwt_8 == 1) { $data['hour_8'] = 1; } else { $data['hour_8'] = 0; }
				if($prwt_9 == 1) { $data['hour_9'] = 1; } else { $data['hour_9'] = 0; }
				if($prwt_10 == 1) { $data['hour_10'] = 1; } else { $data['hour_10'] = 0; }
				if($prwt_11 == 1) { $data['hour_11'] = 1; } else { $data['hour_11'] = 0; }
				if($prwt_12 == 1) { $data['hour_12'] = 1; } else { $data['hour_12'] = 0; }
				if($prwt_13 == 1) { $data['hour_13'] = 1; } else { $data['hour_13'] = 0; }
				if($prwt_14 == 1) { $data['hour_14'] = 1; } else { $data['hour_14'] = 0; }
				if($prwt_15 == 1) { $data['hour_15'] = 1; } else { $data['hour_15'] = 0; }
				if($prwt_16 == 1) { $data['hour_16'] = 1; } else { $data['hour_16'] = 0; }
				if($prwt_17 == 1) { $data['hour_17'] = 1; } else { $data['hour_17'] = 0; }
				if($prwt_18 == 1) { $data['hour_18'] = 1; } else { $data['hour_18'] = 0; }
				if($prwt_19 == 1) { $data['hour_19'] = 1; } else { $data['hour_19'] = 0; }
				if($prwt_20 == 1) { $data['hour_20'] = 1; } else { $data['hour_20'] = 0; }
				if($prwt_21 == 1) { $data['hour_21'] = 1; } else { $data['hour_21'] = 0; }
				if($prwt_22 == 1) { $data['hour_22'] = 1; } else { $data['hour_22'] = 0; }
				if($prwt_23 == 1) { $data['hour_23'] = 1; } else { $data['hour_23'] = 0; }
				if($prwt_24 == 1) { $data['hour_24'] = 1; } else { $data['hour_24'] = 0; }
				unset($data['off']);
				unset($where['off']);
				$data['status'] = 'prwt';
				$where['status'] = 'prwt';
				$responSc = $this->m_dokter->getWhereSchedule($where);
				if(!empty($responSc)){
					$response_prwt = $this->m_dokter->update_schedule($data, $where);					
				}else{
					$response_prwt = $this->m_dokter->save($data);
				}	
		
				if($response_prwt == 1){
					$response_model = 1; 
				}	

			}
		}
		//=========================== OFF AND ==================================//		

		if ($response_model == 1) {
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array("status" => FALSE));
		}

	}
}
