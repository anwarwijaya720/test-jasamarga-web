<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
		parent::__construct();
		// $this->load->model('m_dashboard');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login"));
		}		
        $this->load->model('m_login');
	}

	public function index()
	{
		// $data['view'] = 'Index';
		// $this->load->view('templates/base_template', $data);
		$data['view'] = 'jasamarga/dashboard_admin';
		$this->load->view('templates/base_template', $data);
	}
		
	public function logout(){
		$username = $this->session->userdata('username');
		$status	  = 0;
		$data = array(
			'status' => $status,
		);
		$where = array(
			'username' => $username,
		);
		$this->m_login->updatedStatusLogin($data, $where);		
		session_destroy();
		redirect(base_url("Login"));
	}

}


?>