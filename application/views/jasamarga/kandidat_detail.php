<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Data Kandidat</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Kandidat</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Kandidat</h3>
                    </div>

                    <div class="card-body">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Nama Lengkap*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputName" placeholder="Anisa Rahma">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Jenis Kelamin*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputKelamin" placeholder="Perempuan">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Tempat Lahir*</p>
                                        <div class="col-sm-6">
                                            <div class="input-group date" id="date1" data-target-input="nearest">
                                                <input type="text" name="date1" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#date1" required>
                                                <div class="input-group-append" data-target="#date1" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Agama*</p>
                                        <div class="col-sm-6">
                                            <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                <option value="">Islam</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Email*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputEmail" placeholder="punyasaya@gmail.com">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Nomor Hp*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputNohp" placeholder="08999888777">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">                                    
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Nomor KTP*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputKtp" placeholder="12456241725165">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">File KTP*</p>
                                        <div class="col-sm-6">
                                            <a href="#" class="link-primary">file_ktp.fdp</a>
                                        </div>
                                    </div>                                   
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Nama Bank*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputKtp" placeholder="BCA">
                                        </div>
                                    </div>                                   
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Nomor Bank*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputKtp" placeholder="167261786">
                                        </div>
                                    </div>                                  
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Alamat Domisili*</p>
                                        <div class="col-sm-6">
                                        <textarea type="text" class="form-control" id="inputKtp" placeholder="Jalan Kenanga No.12 Jakarta Selatan"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <P style="margin-top: 20px; font-size: 18px;"><Strong>DATA PENDIDIKAN</Strong></P>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Pendidikan*</p>
                                        <div class="col-sm-6">
                                            <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                <option value="">S1(Sarjana)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Universitas*</p>
                                        <div class="col-sm-6">
                                            <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                <option value="">Mercu Buana</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Tahun Lulus*</p>
                                        <div class="col-sm-6">
                                            <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">                                   
                                    <div class="form-group row" style="margin-top: 60px;">
                                        <p for="input" class="col-sm-3 col-form-label">Jurusan*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputKtp" placeholder="Teknik Informatika">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            <p class="form-check-label" for="flexCheckDefault">
                                                Masih berkuliah sampai saat ini
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <P style="margin-top: 20px; font-size: 18px;"><Strong>LAIN-LAIN</Strong></P>
                            <div class="row">
                                <div class="col-md-6">                                    
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">File CSV*</p>
                                        <div class="col-sm-6">
                                            <a href="#" class="link-primary">curiculum_vite.fdp</a>
                                        </div>
                                    </div>                                     
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Pas Foto*</p>
                                        <div class="col-sm-6">
                                            <a href="#" class="link-primary">pas_foto.png</a>
                                        </div>
                                    </div>  
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">                               
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Fortofolio*</p>
                                        <div class="col-sm-6">
                                            <a href="#" class="link-primary">project_satu.pdf</a>
                                        </div>
                                    </div> 
                                </div>
                            </div>

                            <P style="margin-top: 20px; font-size: 18px;"><Strong>DATA AKUN MAGANG</Strong></P>
                            <div class="row">
                                <div class="col-md-6">                                    
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Lowongan*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="input" placeholder="Programmer">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">                                    
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Tipe Manggang*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="input" placeholder="project based">
                                        </div>
                                    </div>                            
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Priode Magang*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="input" placeholder="1/1/2021-30/3/2021">
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                            
                            <P style="margin-top: 20px; font-size: 18px;"><Strong>PROSES SELEKSI</Strong></P>
                            <div class="row">
                                <div class="col-md-6">                                    
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Unit Kerja*</p>
                                        <div class="col-sm-6">
                                            <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                <option value="">ITE</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Hasil Assesmen*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="input">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Rengking*</p>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="input">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <p for="input" class="col-sm-3 col-form-label">Hasil*</p>
                                        <div class="col-sm-6">
                                            <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding: 20px;">                                
                                <button type="button" class="btn btn-success" style="margin-right: 20px;">Tidak Lolos</button>
                                <button type="button" class="btn btn-danger">Proses Ke Unit</button>
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>

    <!-- /.content -->
</div> 