<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Kandidat Magang</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Kandidat</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Filter</h3>
                        </div>

                        <div class="card-body">

                            <!-- <form role="form" id="formRekap"> -->
                            <form role="form" id="formRekap" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                    <option value="">PMMB</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                    <option value="">Batch 1 2021</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                        <option value="">Teknik Informatika</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group" style="margin-left: 5px;">
                                                <select class="form-control select2bs4" name="indikator" style="width: 100%;" required>
                                                    <option selected="selected" value="">--- Status Kandidat ---</option>
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>

                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Export To Excel</button>
                                    <button type="submit" class="btn btn-warning">Filter Data</button>
                                    <button type="submit" class="btn btn-success">Kirim Notifikasi Email</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table id="mydata" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="checkall" /></th>
                                <th>No</th>
                                <th>Nama Kandidat</th>
                                <th>Jurusan</th>
                                <th>Tanggal Registrasi</th>
                                <th>Unit Kerja</th>
                                <th>Hasil Asessmen</th>
                                <th>Rengking</th>
                                <th>Status</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="show_data">
                            <?php
                                $detail = base_url("jasamarga/Kandidat/detail");
                                for($x=1; $x <= 20; $x++){
                                    echo('
                                            <tr>
                                            <td><input type="checkbox" class="checkthis" name="checkboxName" value="'.$x.'" /></td>
                                            <td>'.$x.'</td>
                                            <td>Anisa Rahma</td>
                                            <td>DKV</td>
                                            <td>20/12/2020</td>
                                            <td>CCO</td>
                                            <td>80</td>
                                            <td>4</td>
                                            <td>Rebiew JlI</td>
                                            <td>Terkirim</td>
                                            <td><a type="submit" class="btn btn-primary" href="'.$detail.'">Detail</a></td>
                                        </tr>
                                    ');
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div><!-- /.container-fluid -->
    </section>

    <!-- /.content -->
</div>


<script>
    $(document).ready(function() {
        $('#mydata').DataTable();
    });

    $(document).ready(function() {
        $("#mydata #checkall").click(function() {
            if ($("#mydata #checkall").is(':checked')) {
                $("#mydata input[type=checkbox]").each(function() {
                    $(this).prop("checked", true);
                });

            } else {
                $("#mydata input[type=checkbox]").each(function() {
                    $(this).prop("checked", false);
                });
            }
        });

        $("[data-toggle=tooltip]").tooltip();
    });
</script>