<nav class="navbar navbar-expand-lg navbar-light" style="background-color: grey;">
  <!-- <a class="navbar-brand" href="#">Navbar</a> -->  
  <img src="<?php echo base_url("assets/images/logo/jasamarga-icon.png"); ?>" style="height: 50px;">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 50px;">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a style="color: blue; font-weight: bold;" class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a style="color: white; font-weight: bold;" class="nav-link" href="#">Lowongan</a>
      </li>
      <li class="nav-item">
        <a style="color: blue; font-weight: bold;" class="nav-link" href="#">Informasi</a>
      </li>
      <li class="nav-item">
        <a style="color: blue; font-weight: bold;" class="nav-link" href="#">FAQ</a>
      </li>
      <li class="nav-item">
        <a style="color: blue; font-weight: bold;" class="nav-link" href="#">Kontak</a>
      </li>
      <li class="nav-item">
        <a style="color: blue; font-weight: bold;" class="nav-link" href="#">Info Perusahaan</a>
      </li>
      <li class="nav-item">
        <a style="color: blue; font-weight: bold;" class="nav-link" href="#">Masuk</a>
      </li>
    </ul>
  </div>
</nav>

<div style="margin-top: 20px; margin-left: 20px;">
  <p>Home/Lowongan/Registrasi</p>
</div>

<section class="content">
    <div class="container-fluid">
    
        <div class="col-12">
            <div class="card">

                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Nama Lengkap*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputName" placeholder="Anisa Rahma">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Jenis Kelamin*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKelamin" placeholder="Perempuan">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Tempat Lahir*</p>
                                    <div class="col-sm-6">
                                        <div class="input-group date" id="date1" data-target-input="nearest">
                                            <input type="text" name="date1" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#date1" required>
                                            <div class="input-group-append" data-target="#date1" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Agama*</p>
                                    <div class="col-sm-6">
                                        <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                            <option value="">Islam</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Email*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputEmail" placeholder="punyasaya@gmail.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Nomor Hp*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputNohp" placeholder="08999888777">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">                                    
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Nomor KTP*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKtp" placeholder="12456241725165">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">File KTP*</p>
                                    <div class="col-sm-6">
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="validatedCustomFile">Cari...</label>
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                        </div>
                                    </div>
                                </div>                                   
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Nama Bank*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKtp" placeholder="BCA">
                                    </div>
                                </div>                                   
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Nomor Bank*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKtp" placeholder="167261786">
                                    </div>
                                </div>                                  
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Alamat Domisili*</p>
                                    <div class="col-sm-6">
                                    <textarea type="text" class="form-control" id="inputKtp" placeholder="Jalan Kenanga No.12 Jakarta Selatan"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <P style="margin-top: 20px; font-size: 18px;"><Strong>DATA PENDIDIKAN</Strong></P>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Pendidikan*</p>
                                    <div class="col-sm-6">
                                        <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                            <option value="">S1(Sarjana)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Universitas*</p>
                                    <div class="col-sm-6">
                                        <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                            <option value="">Mercu Buana</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Tahun Lulus*</p>
                                    <div class="col-sm-6">
                                        <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">                                   
                                <div class="form-group row" style="margin-top: 60px;">
                                    <p for="input" class="col-sm-3 col-form-label">Jurusan*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKtp" placeholder="Teknik Informatika">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <p class="form-check-label" for="flexCheckDefault">
                                            Masih berkuliah sampai saat ini
                                        </p>
                                    </div>
                                </div>                                                                  
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Semester*</p>
                                    <div class="col-sm-6">
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <P style="margin-top: 20px; font-size: 18px;"><Strong>PENGALAMAN ORGANISASI</Strong></P>
                        <div class="row">
                            <div class="col-md-6">                                    
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Organisasi*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKtp" placeholder="">
                                    </div>
                                </div>                                     
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Tahun*</p>
                                    <div class="col-sm-6">
                                        <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                            <!-- <option value="">S1(Sarjana)</option> -->
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Jabatan*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="inputKtp" placeholder="">
                                    </div>
                                </div>  
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">                               
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Deskripsi Kegiatan*</p>
                                    <div class="col-sm-6">
                                        <textarea type="text" class="form-control" id="input"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">File*</p>
                                    <div class="col-sm-6">                                        
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="validatedCustomFile">Cari...</label>
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>

                        <P style="margin-top: 40px; font-size: 18px;"><Strong>KEAHLIAN YAN DIMILIKI</Strong></P>
                        <div class="row">
                            <div class="col-md-12">                                    
                                <div class="form-group row">
                                    <p for="input" class="col-sm-1 col-form-label">Kahlian</p>
                                    <div class="col-sm-9" style="margin-left: 50px;">
                                        <textarea type="text" class="form-control" id="input" style="height: 100px;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <P style="margin-top: 20px; font-size: 18px;"><Strong>LAIN-LAIN</Strong></P>
                        <div class="row">
                            <div class="col-md-6">                                    
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">File CV*</p>
                                    <div class="col-sm-6">
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="validatedCustomFile">Cari...</label>
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                        </div> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Potofolio*</p>
                                    <div class="col-sm-6">
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="validatedCustomFile">Cari...</label>
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">                                    
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Pas Foto*</p>
                                    <div class="col-sm-6">
                                        <div class="custom-file">
                                            <label class="custom-file-label" for="validatedCustomFile">Cari...</label>
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                        </div> 
                                    </div>
                                </div>                              
                            </div>
                        </div>                   
                        
                        <P style="margin-top: 20px; font-size: 18px;"><Strong>SOSIAL MEDAI</Strong></P>
                        <div class="row">
                            <div class="col-md-6">  
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Linkdin*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Facebook*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="input">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">  
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Instagram*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <p for="input" class="col-sm-3 col-form-label">Twitter*</p>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="input">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <P style="margin-top: 20px; font-size: 18px;"><Strong>MENGENAL JASAMARGA DARI</Strong></P>
                        <div class="row" style="margin-left: 50px;">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                        <label class="form-check-label" for="exampleRadios1">
                                            Default radio
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            Default radio
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">  
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            Default radio
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            Default radio
                                        </label>
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-md-4">  
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
                                        <label class="form-check-label" for="exampleRadios1">
                                            Default radio
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                        <label class="form-check-label" for="exampleRadios1">
                                            Default radio
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" style="text-align: center; margin-top: 25px;">
                            <p>1. ........................................................................</p>
                            <p>2. ........................................................................</p>
                            <p>3. ........................................................................</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1" style="text-align: left;">
                                    Dengan ini saya menyetujui sayatan dan ketentuan</br>
                                    Magang yang ada ketentuan di PT.Jasa Marga (Persero) TBK.
                                </label>
                            </div>                            
                            <button type="submit" class="btn btn-primary" style="margin-top: 20px;">Kirim Lamaran</button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        
    </div><!-- /.container-fluid -->
</section>

<!-- Footer -->
<footer class="page-footer" style="background-color: #00008B; height: 50px;">        
    <div class="row" style="padding-left: 50px; margin-right: 50px;">
        <div class="col-sm-6" style="margin-top: 12px;">
            <p style="color: white;">PT JASA MARGA (PERSERO) Tbk.</p>
        </div>
        <div class="col-sm-6" style="color: white; text-align: right; margin-top: 13px;">
            <!-- Facebook -->
            <i class="fab fa-facebook-f" style="font-size: 25px; margin-right: 10px;"></i>

            <!-- Twitter -->
            <i class="fab fa-twitter" style="font-size: 25px; margin-right: 10px;"></i>

            <!-- Instagram -->
            <i class="fab fa-instagram" style="font-size: 25px; margin-right: 10px;"></i>

            <!-- Linkedin -->
            <i class="fab fa-youtube" style="font-size: 25px; margin-right: 10px;"></i>
        </div>
    </div>
</footer>
<!-- Footer -->

