<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="form-row" style="float: right; margin-right: 40px; margin-top: 20px;">
        <div class="col-auto my-1">
          <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
            <!-- <option selected></option> -->
            <option value="1">2021</option>
            <option value="2">2022</option>
            <option value="3">2023</option>
          </select>
        </div>
      </div>

      <div class="container">
          <div class="row justify-content-center">
              <div class="col-8 text-center">              
                <p id="titleHome">JUMLAH PEMAGANG BERDASARKAN TIPE MAGANG</p>
                <!-- <canvas id="chartMagang" width="400" height="200"></canvas>-->
                <canvas id="chartMagang"></canvas>
              </div>
          </div>
      </div>
    
      <div class="row" style="padding-top: 20px;">

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PMMB</span>
              <span class="info-box-number">25 Orang</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PROJECT BASE</span>
              <span class="info-box-number">50 Orang</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">OPERATION</span>
              <span class="info-box-number">20 Orang</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">INTENSHIP</span>
              <span class="info-box-number">5 Orang</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

      </div>

      <div class="row">
        
        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTAL PEMAGANG</span>
              <span class="info-box-number">100 Orang</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon elevation-1" style="background-color: #6495ED;"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">JUMLAH PUBLISH MAGANG</span>
              <span class="info-box-number">10 Lowongan</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon elevation-1" style="background-color: #CCCCFF"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">UNIT PERUSAHAAN</span>
              <span class="info-box-number">10 Unit</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon elevation-1" style="background-color: #FF7F50"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">LEMBAGA PENDIDIKAN</span>
              <span class="info-box-number">2,000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
        <div class="col-12 col-sm-6 col-md-3">
          <div class="info-box mb-3">
            <span class="info-box-icon elevation-1" style="background-color: #DE3163"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PEMAGANG AKTIF</span>
              <span class="info-box-number">70 Orang</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

      </div>

      <div class="row" style="text-align: center;">
        <div class="col-md-6">
          <p><strong>INFORMASI LOWONGAN</strong></p>
            <div>
              <canvas id="myChart" width="400" height="200"></canvas>
            </div>
        </div>
        <div class="col-md-6">          
          <p><strong>INFORMASI JURUSAN</strong></p>
          <div>
            <canvas id="myChart2" width="400" height="250"></canvas>
          </div>
        </div>
      </div>
    

    </div><!-- /.container-fluid -->
  </section>

  <!-- /.content -->
</div>

<style>
#titleHome{  
  padding-top: 20px;
  font-size: 20px;
  text-align: center;
  font-weight: bold;
  color: #FF7F50;
}
#box{
  border-style: solid;
  border-color: blue;  
}
.vertical-center {
  min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
  min-height: 100vh; /* These two lines are counted as one :-)       */

  display: flex;
  align-items: center;
}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>

var ctx = document.getElementById('chartMagang').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustis", "September", "Oktober", "November", "Desember"],
      datasets: [{
          data: [86,114,106,106,107,111,133,120,100,90,80,70],
          label: "PMMB",
          backgroundColor: "#6495ED",
          borderWidth:2
        }, { 
          data: [70,90,44,60,83,90,100,90,80,60,50,100],
          label: "Project Based",
          backgroundColor: "#FF7F50",
          borderWidth:2
        }, { 
          data: [10,21,60,44,17,21,17,20,40,30,15,10],
          label: "Operation",
          backgroundColor:"#40E0D0",
          borderWidth:2
        },{ 
          data: [0,0,0,0,0,0,17,20,40,30,15,10],
          label: "Interenship",
          backgroundColor:"#FFBF00",
          borderWidth:2
        }
      ]
    },
  });  

var ctx = $('#myChart').get(0).getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Internet', 'Kampus', 'Instagram', 'Facebook', 'Twitter', 'Lainnya'],
        datasets: [{
            label: '# of Votes',            
            data: [12, 19, 3, 5, 10, 12,],
            backgroundColor: "#6495ED",
            borderWidth: 1
        }]
    },    
    options: {
      legend: {
          display: false
      },
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

var ctx = $('#myChart2').get(0).getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Teknis Split', 'Teknik Informatika', 'Arsitektur', 'Administrasi Publik', 'Ilmu Komunikasi', 'Teknik Elektro', 'Manajemen', 'Akutansi', 'Sistem Informasi', 'Ilmu Hukum'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 10, 3, 9, 5, 15, 13],
            backgroundColor: "#6495ED",
            borderWidth: 1
        }]
    },
    options: {
      legend: {
        display: false
      },
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>
