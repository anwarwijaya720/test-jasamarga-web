<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    
      <div style="text-align: right; padding-top: 20px; padding-bottom: 20px;">      
        <button type="submit" class="btn btn-warning" style="margin-top: 20px;">Cetak Profile</button>
      </div>

      <div class="col-12">
        <div class="row" style="margin: 10px;">

          <div class="row col-md-5">
              <div class="card col-md-12">                
                <div class="card-body" style="margin: 10px;">                    

                    <div class="row">
                      <div class="col-md-8">                        
                        <a style="font-size: 18px;"><strong>Ahmad</strong></a></br>
                        <a>Universitas Indonesia</a></br>
                        <a>JMLI</a>
                      </div>
                      <div class="col" style="text-align: right; margin-top: 20px;">
                        <i class="far fa-grin-alt" style="font-size: 50px; margin-right: 10px;"></i>
                        <!-- <i class="fa fa-user-circle-o" aria-hidden="true"></i> -->
                      </div>
                    </div>

                    <div class="row" style="margin-top: 15px;">
                      <div class="col-md-12">

                          <div class="row">                          
                            <div class="col-md-5">
                                <a class="col-md-4">Nomor Telpon</a> 
                            </div>                      
                            <a> : </a><a> 617621726</a></br>
                          </div>
                          
                          <div class="row">                          
                            <div class="col-md-5">
                                <a class="col-md-4">Emal</a> 
                            </div>                      
                            <a> : </a><a> punyasaya@gmail.com</a></br>
                          </div> 
                          
                          <div class="row">                          
                            <div class="col-md-5">
                                <a class="col-md-4">Jenis Kelamin</a> 
                            </div>                      
                            <a> : </a><a> Laki-Laki</a></br>
                          </div> 
                          
                          <div class="row">                          
                            <div class="col-md-5">
                                <a class="col-md-4">Instagram</a> 
                            </div>                      
                            <a> : </a><a> punyasaya07</a></br>
                          </div> 
                          
                          <div class="row">                          
                            <div class="col-md-5">
                                <a class="col-md-4">Twitter</a> 
                            </div>                      
                            <a> : </a><a> punyasaya07</a></br>
                          </div> 
                          
                          <div class="row">                          
                            <div class="col-md-5">
                                <a class="col-md-4">Alamat</a> 
                            </div>                      
                            <a> : </a>
                            <a> Jakarta Selatan</a></br>
                          </div> 

                      </div>                      
                    </div>

                    <div class="row" style="padding: 20px; margin-top: 20px;">
                      <canvas id="marksChart" width="600" height="400"></canvas>
                    </div>

                </div>
              </div>
          </div>

          <div class="row col-md-7" style="margin-left: 10px;">
            <div class="col-md-12">
              <div class="container">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="card" style="text-align: center; padding: 10px;">
                      <p class="fontNialiTxt">Nili Akhir</p>
                      <p class="fontNialiValue">83</p>
                    </div>
                  </div>
                  <div class="col-sm-4">                  
                    <div class="card" style="text-align: center; padding: 10px;">
                      <p class="fontNialiTxt">JUMLAH PROJECT</p>
                      <p class="fontNialiValue">12</p>
                    </div>
                  </div>
                  <div class="card col-sm-4 align-items-center d-flex justify-content-center"> 
                    <div class="row">
                      <div class="col-md-5" style="text-align: center;">
                          <i class="far fa-grin-alt" style="font-size: 50px; margin-right: 10px; margin-top: 40px;"></i>
                      </div>
                      <div class="col-md-7" style="text-align: center; padding: 10px;">
                        <p class="fontNialiTxt">MENTOR</p>
                        <p >Bagus Kusuma</p>
                      </div>
                    </div>  
                  </div>
                </div>
              </div> 

              <div style="margin-left: 10px;">
                <a>Hai, AHMAD</a></br>
                <a>Jangan lupa absen masuk dan absen Keluar ya !</a>
              </div>

              <div class="row">
                <div class="col" style="padding: 80px;">
                  <div class="card card-block d-flex" style="height: 120px; background-color: #40E0D0; font-size: 20px; font-weight: bold; color: white;">
                    <div class="card-body align-items-center d-flex justify-content-center">
                      MASUK
                    </div>
                  </div>
                </div>
                <div class="col" style="padding: 80px;">                  
                  <div class="card card-block d-flex" style="height: 120px; background-color: #DE3163; font-size: 20px; font-weight: bold; color: white;">
                    <div class="card-body align-items-center d-flex justify-content-center">
                      KELUAR
                    </div>
                  </div>
                </div>
              </div>
                          
              <div class="Row col-md-12 align-items-center d-flex justify-content-cente" style="background-color: #2874A6; height: 100px; padding-top: 10px;">                

                  <div class="col-md-3">
                    <div class="card card-block d-flex" style="height: 50px">
                      <div class="card-body align-items-center d-flex justify-content-center">
                        <a style="color: blue; font-weight: bold; font-size: 20px;">4 JAM</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4" style="text-align: center; color: white;">
                        <a>Jam Masuk</a></br>
                        <a>08:00</a>
                  </div>                  

                  <div class="col-md-1" style="text-align: center; color: white;">
                        <a>|</a></br>
                        <a>|</a></br>
                        <a>|</a></br>
                  </div>

                  <div class="col-md-4" style="text-align: center; color: white;">
                        <a>Jam Masuk</a></br>
                        <a>08:00</a>
                  </div>

              </div>

            </div>
          </div>

        </div>
      </div>
      
      <div class="card" style="margin-top: 30px;">
        <div style="margin: 10px;">          
          <button type="submit" class="btn btn-warning" style="margin-top: 20px;">Export Excel</button>
          <button type="submit" class="btn btn-info" style="margin-top: 20px;" data-toggle="modal" data-target="#exampleModal">Tambah Project</button>
        </div>
          <div class="card-body">
              <table id="mydata" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Judul Project</th>
                          <th>Minggu</th>
                          <th>Tanggal Mulai</th>
                          <th>Tanggal Selesai</th>                          
                          <th>Status</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody id="show_data">
                      <?php
                          for($x=1; $x <= 20; $x++){
                              echo('
                                      <tr>
                                      <td>'.$x.'</td>
                                      <td>Ahmad</td>
                                      <td>laporan Bulanan</td>
                                      <td>1</td>
                                      <td>12/7/2021</td>
                                      <td>13/7/2021</td>
                                      <td>Selesai</td>
                                      <td>
                                          <a type="submit" class="btn btn-primary" id="infoColor" data-toggle="modal" data-target="#exampleModal">Detail</a>
                                          <a type="submit" class="btn btn-info infoColor" id="infoColor" data-toggle="modal" data-target="#exampleModal">Ubah</a>
                                      </td>
                                  </tr>
                              ');
                          }
                      ?>
                  </tbody>
              </table>
          </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Data Project</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">Minggu</p>
                  <div class="col-sm-8">
                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                      <!-- <option selected>Choose...</option> -->
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                    </select>
                  </div>
              </div>

              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">Judul Project</p>
                  <div class="col-sm-8">                    
                      <input type="text" class="form-control form-control-sm" id="colFormLabelSm">
                  </div>
              </div>

              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">Tanggal Mulai</p>
                  <div class="col-sm-8">
                    <div class="input-group date" id="date1" data-target-input="nearest">
                        <input type="text" name="date1" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#date1" required>
                        <div class="input-group-append" data-target="#date1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>                    
                  </div>
              </div>

              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">Tanggal Selesai</p>
                  <div class="col-sm-8">
                    <div class="input-group date" id="date2" data-target-input="nearest">
                        <input type="text" name="date2" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#date2" required>
                        <div class="input-group-append" data-target="#date2" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>                    
                  </div>
              </div>

              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">Deskripsi</p>
                  <div class="col-sm-8">
                    <textarea type="text" class="form-control form-control-sm" id="colFormLabelSm" style="height: 100px;"> </textarea>
                  </div>
              </div>
              
              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">File</p>
                  <div class="col-sm-8">
                      <div class="custom-file">
                          <label class="custom-file-label" for="validatedCustomFile">Cari...</label>
                          <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                      </div>
                  </div>
              </div>

              <div class="form-group row">
                  <p for="input" class="col-sm-4 col-form-label">Status</p>
                  <div class="col-sm-8">
                      <select id="inputState" class="form-control">
                        <!-- <option selected>Choose...</option> -->
                        <option>Selesai</option>
                        <option>Belum Selesai</option>
                      </select>                    
                  </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="button" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

  <!-- /.content -->
</div>

<style>
#infoColor{
  color: white;
}

.fontNialiTxt{
  font-size: 16px;
  font-weight: bold;
}

.fontNialiValue{
  font-size: 30px;
  font-weight: bold;
}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>
    $(document).ready(function() {
        $('#mydata').DataTable();
    });

    var ctx = $('#marksChart').get(0).getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'radar',
        data: {
            labels: ['AHLAK', 'ABSENSI', 'INNOVATION', 'SEDIGN THINKING', 'PERFORMANCE'],
            datasets: [{
                label: '# of Votes',            
                data: [32, 32, 28, 12, 15],
                backgroundColor: "#6495ED",
                borderWidth: 1
            }]
        },    
        options: {
          legend: {
              display: false
          },
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

</script>
