<div class="content-header">
    <div class="container-fluid">

        <div class="row" style="margin: 10px;">
            <div class="row mb-8">
                <div style="margin-left: 10px;">
                    <img src="<?php echo base_url("assets/images/logo/LogoMatneo.png"); ?>" style="height: 40px;">
                </div>
                <div style="margin-left: 10px;">
                    <h4 class="m-0 text-dark"><strong>Info Ketersediaan Darah</strong></h4>
                    <strong><a>MatneoSafe</a></strong>
                </div>
            </div>
            <div style="position: absolute; right: 20px;">
                <strong>
                    <p id="jam_gadang" class="text-red text-uppercase"></p>
                </strong>
                <div class="text-right" style="margin-top: -10px;">
                    <strong>
                        <p id="jam_gadang" class="text-red text-uppercase"></p>
                    </strong>
                    <div class="text-right" style="margin-top: -10px;">
                        <a href="#" class="btn btn-sm btn-danger">
                            <i class="far fa-hand-pointer"> </i>
                            Klik 3 Kali Dapat Rumah Sakit
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Main content -->
<section class="content" style="margin-top: 20px;">
    <div class="container-fluid">

        <div class="row">

            <div class="col-md-3">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" style="text-align: center; font-size: 12px;">
                        <strong><a style="font-size: 20px;">Gol Darah A</a></strong>
                    </div>
                    <div class="card-body">

                        <div class="row" style="text-align: center;">

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">WB</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $aWb[0]->Stok_aWb ?> Rs</a></strong>
                                <?php
                                    $y = $aWb[0]->Stok_aWb;
                                    $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                    $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                    $audio = base_url('assets/images/mp3/audio.mpeg');
                                    if ($y != 0) {                                        
                                        if(!empty($rs_aWb)){
                                            foreach ($rs_aWb as $data) {
                                                echo ('
                                                        <div class="col-sm-12" style="background-color:#4FBBF5;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                    ');
                                            }
                                        }
                                    } else {
                                        echo ('
                                                <div style="text-align: center;">
                                                    <img src=' . $img_data_null . ' id="imgStyle">
                                                    <audio autoplay loop>
                                                        <source src="'.$audio.'" type="audio/mpeg">
                                                    </audio>                                                                   
                                                </div>
                                            ');
                                    }
                                ?>

                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">PRC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $aPrc[0]->Stok_aPrc; ?> Rs</a></strong>

                                <?php
                                    $y = $aPrc[0]->Stok_aPrc;
                                    $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                    $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                    $audio = base_url('assets/images/mp3/audio.mpeg');
                                    if ($y != 0) {
                                        if(!empty($rs_aPrc)){
                                            foreach ($rs_aPrc as $data) {
                                                echo ('
                                                        <div class="col-sm-12" style="background-color:#4FBBF5;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                    ');
                                            }
                                        }
                                    } else {
                                        echo ('
                                                <div style="text-align: center;">
                                                    <img src=' . $img_data_null . ' id="imgStyle">
                                                    <audio autoplay loop>
                                                        <source src="'.$audio.'" type="audio/mpeg">
                                                    </audio>                                                                   
                                                </div>
                                            ');
                                    }
                                ?>
                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">TC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $aTc[0]->Stok_aTc; ?> Rs</a></strong>

                                <?php
                                    $y = $aTc[0]->Stok_aTc;
                                    $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                    $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                    $audio = base_url('assets/images/mp3/audio.mpeg');
                                    if ($y != 0) {                                        
                                        if(!empty($rs_aTc)){
                                            foreach ($rs_aTc as $data) {
                                                echo ('
                                                        <div class="col-sm-12" style="background-color:#4FBBF5;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                    ');
                                            }
                                        }
                                    } else {
                                        echo ('
                                                <div style="text-align: center;">
                                                    <img src=' . $img_data_null . ' id="imgStyle">
                                                    <audio autoplay loop>
                                                        <source src="'.$audio.'" type="audio/mpeg">
                                                    </audio>                                                                   
                                                </div>
                                            ');
                                    }
                                ?>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" style="text-align: center; font-size: 12px;">
                        <strong><a style="font-size: 20px;">Gol Darah B</a></strong>
                    </div>
                    <div class="card-body" style="text-align: center;">

                        <div class="row" style="text-align: center;">

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">WB</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $bWb[0]->Stok_bWb; ?> Rs</a></strong>
                                <?php
                                    $y = $bWb[0]->Stok_bWb;
                                    $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                    $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                    $audio = base_url('assets/images/mp3/audio.mpeg');
                                    if ($y != 0) {          
                                        if(!empty($rs_bWb)){                                    
                                            foreach ($rs_bWb as $data) {
                                                echo ('
                                                        <div class="col-sm-12" style="background-color:#78E56E;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                    ');
                                            }
                                        }
                                    } else {
                                        echo ('
                                                <div style="text-align: center;">
                                                    <img src=' . $img_data_null . ' id="imgStyle">
                                                    <audio autoplay loop>
                                                        <source src="'.$audio.'" type="audio/mpeg">
                                                    </audio>                                                                   
                                                </div>
                                            ');
                                    }
                                ?>

                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">PRC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $bPrc[0]->Stok_bPrc; ?> Rs</a></strong>

                                <?php
                                    $y = $bPrc[0]->Stok_bPrc;
                                    $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                    $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                    $audio = base_url('assets/images/mp3/audio.mpeg');
                                    if ($y != 0) {  
                                        if(!empty($rs_bPrc)){            
                                            foreach ($rs_bPrc as $data) {
                                                echo ('
                                                        <div class="col-sm-12" style="background-color:#78E56E;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                    ');
                                            }
                                        }
                                    } else {
                                        echo ('
                                                <div style="text-align: center;">
                                                    <img src=' . $img_data_null . ' id="imgStyle">
                                                    <audio autoplay loop>
                                                        <source src="'.$audio.'" type="audio/mpeg">
                                                    </audio>                                                                   
                                                </div>
                                            ');
                                    }
                                ?>
                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">TC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $bTc[0]->Stok_bTc; ?> Rs</a></strong>

                                <?php
                                    $y = $bTc[0]->Stok_bTc;
                                    $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                    $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                    $audio = base_url('assets/images/mp3/audio.mpeg');
                                    if ($y != 0) {
                                        if(!empty($rs_bTc)){                                    
                                            foreach ($rs_bTc as $data) {
                                                echo ('
                                                        <div class="col-sm-12" style="background-color:#78E56E;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                    ');
                                            }
                                        }
                                    } else {
                                        echo ('
                                                <div style="text-align: center;">
                                                    <img src=' . $img_data_null . ' id="imgStyle">
                                                    <audio autoplay loop>
                                                        <source src="'.$audio.'" type="audio/mpeg">
                                                    </audio>                                                                   
                                                </div>
                                            ');
                                    }
                                ?>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" style="text-align: center; font-size: 12px;">
                        <strong><a style="font-size: 20px;">Gol Darah AB</a></strong>
                    </div>
                    <div class="card-body">

                        <div class="row" style="text-align: center;">

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">WB</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $abWb[0]->Stok_abWb; ?> Rs</a></strong>
                                <?php
                                $y = $abWb[0]->Stok_abWb;
                                $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                $audio = base_url('assets/images/mp3/audio.mpeg');
                                if ($y != 0) {
                                    if(!empty($rs_abWb)){                                                                        
                                        foreach ($rs_abWb as $data) {
                                            echo ('
                                                    <div class="col-sm-12" style="background-color:#F7CD61;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('
                                            <div style="text-align: center;">
                                                <img src=' . $img_data_null . ' id="imgStyle">
                                                <audio autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                                   
                                            </div>
                                        ');
                                }
                                ?>

                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">PRC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $abPrc[0]->Stok_abPrc; ?> Rs</a></strong>

                                <?php
                                $y = $abPrc[0]->Stok_abPrc;
                                $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                $audio = base_url('assets/images/mp3/audio.mpeg');
                                if ($y != 0) {
                                    if(!empty($rs_abPrc)){                                                                       
                                        foreach ($rs_abPrc as $data) {
                                            echo ('
                                                    <div class="col-sm-12" style="background-color:#F7CD61;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('
                                            <div style="text-align: center;">
                                                <img src=' . $img_data_null . ' id="imgStyle">
                                                <audio autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                                   
                                            </div>
                                        ');
                                }
                                ?>
                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">TC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $abTc[0]->Stok_abTc; ?> Rs</a></strong>

                                <?php
                                $y = $abTc[0]->Stok_abTc;
                                $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                $audio = base_url('assets/images/mp3/audio.mpeg');
                                if ($y != 0) {         
                                    if(!empty($rs_abTc)){                                                              
                                        foreach ($rs_abTc as $data) {
                                            echo ('
                                                    <div class="col-sm-12" style="background-color:#F7CD61;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('
                                            <div style="text-align: center;">
                                                <img src=' . $img_data_null . ' id="imgStyle">
                                                <audio autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                                   
                                            </div>
                                        ');
                                }
                                ?>

                            </div>

                        </div>


                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" style="text-align: center; font-size: 12px;">
                        <strong><a style="font-size: 20px;">Gol Darah O</a></strong>
                    </div>
                    <div class="card-body">

                        <div class="row" style="text-align: center;">

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">WB</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $oWb[0]->Stok_oWb ?> Rs</a></strong>
                                <?php
                                $y = $oWb[0]->Stok_oWb;
                                $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                $audio = base_url('assets/images/mp3/audio.mpeg');
                                if ($y != 0) {            
                                    if(!empty($rs_oWb)){                                                                                              
                                        foreach ($rs_oWb as $data) {
                                            echo ('
                                                    <div class="col-sm-12" style="background-color:#F58A81;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('
                                            <div style="text-align: center;">
                                                <img src=' . $img_data_null . ' id="imgStyle">
                                                <audio autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                                   
                                            </div>
                                        ');
                                }
                                ?>

                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">PRC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $oPrc[0]->Stok_oPrc; ?> Rs</a></strong>

                                <?php
                                $y = $oPrc[0]->Stok_oPrc;
                                $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                $audio = base_url('assets/images/mp3/audio.mpeg');
                                if ($y != 0) {  
                                    if(!empty($rs_oPrc)){                                                                                                                                                                
                                        foreach ($rs_oPrc as $data) {
                                            echo ('
                                                    <div class="col-sm-12" style="background-color:#F58A81;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('
                                            <div style="text-align: center;">
                                                <img src=' . $img_data_null . ' id="imgStyle">
                                                <audio autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                                   
                                            </div>
                                        ');
                                }
                                ?>
                            </div>

                            <div class="col-md-4">
                                <strong><a id="infoStyleTitle">TC</a></strong>
                                <br />
                                <strong><a id="infoStyleTitle">Tersedia: <?php echo $oTc[0]->Stok_oTc; ?> Rs</a></strong>

                                <?php
                                $y = $oTc[0]->Stok_oTc;
                                $img_data = base_url("assets/images/img/rs_siloam.jpg");
                                $img_data_null = base_url("assets/images/icon/clos_icon.png");
                                $audio = base_url('assets/images/mp3/audio.mpeg');
                                if ($y != 0) {    
                                    if(!empty($rs_oTc)){                                                                
                                        foreach ($rs_oTc as $data) {
                                            echo ('
                                                    <div class="col-sm-12" style="background-color:#F58A81;" id="infoTextStyle"><strong> '.$data->rs_name.' </strong></div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('
                                            <div style="text-align: center;">
                                                <img src=' . $img_data_null . ' id="imgStyle">
                                                <audio autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                                   
                                            </div>
                                        ');
                                }
                                ?>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

<style type="text/css">

  #infoStyleTitle{
      font-size: 14px;
  }
  #infoTextStyle{
    font-size: 12px; 
    border-radius: 10px; 
    padding: 5px;
    /* height: 40px;  */
    margin-top: 5px;
  }
  #imgStyle{
    width: 80px; height: 75px;
  }
  #styleCardMinHeight{
      min-height: 240px;
  }

</style>

<script>
    $(document).ready(function() {

        // REALTIME
        var port = "3000";
        var url = "147.139.182.120";
        var socketIoAddress = `http://${url}:${port}`;
        var socket = io(socketIoAddress);

        function getBlood(){
            $.ajax({
                url: "InfoBlood",
                type: "GET",
                // data: $('#dokterSp').serialize(),
                dataType: "JSON",
                success: function() {
                    // console.log(data);                
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: errorThrown,
                    });
                }
            });
        }

        socket.on('fromServer', data => {
            console.log(data);
            // getBlood();
            location.reload();
        });
        
    });
</script>
