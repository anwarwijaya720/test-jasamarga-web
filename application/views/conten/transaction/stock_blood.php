<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Ketersediaan Darah</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Stock Blood</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <form action="#" role="form" id="stockBlood">
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-3" style="margin-bottom: 20px;">
                                            <div class="form-group">
                                                <label>Pilih Rumah Sakit</label>
                                                <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;">
                                                    <option value="">---- Pilih ----</option>
                                                    <?php
                                                    foreach ($getRs as $rs) {
                                                        echo "<option value='" . $rs->rs_id . "'>" . $rs->rs_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <table id="mydata" class="table table-bordered table-striped">
                                            <thead>
                                                <tr style="text-align: center;">
                                                    <th>Golongan Darah</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="show_data">
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/red_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">A (WB)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="aWb">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/red_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">A (PRC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="aPrc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/red_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">A (TC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="aTc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/blue_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">AB (WB)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="abWb">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/blue_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">AB (PRC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="abPrc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/blue_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">AB (TC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="abTc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/yellow_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">B (WB)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="bWb">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/yellow_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">B (PRC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="bPrc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/yellow_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">B (TC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="bTc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/green_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">O (WB)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="oWb">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/green_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">O (PRC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="oPrc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/green_icon.png"); ?>" style="width: 50px; height: 50px;">
                                                        <strong style="font-size: 20px;">O (TC)</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <select class="form-control" name="oTc">
                                                                    <option value="">---- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ks" value="Kosong">Kosong</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" id="btnSaveBlood" class="btn btn-primary">Update</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<?php require('stock_blood_action.php') ?>