<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Ketersediaan Dr Spesialis</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Update Jadwal Dokter</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <!-- <form role="form" id="dokterSp" method="post" enctype="multipart/form-data" action="<?php echo base_url('transaction/DokterSp/update');?>">                             -->
                            <form role="form" id="dokterSp">
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Rumah Sakit</label>
                                                <select class="form-control select2bs4" name="rs" style="width: 100%;">
                                                    <option value="">---- Pilih ----</option>
                                                    <?php
                                                    foreach ($getRs as $rs) {
                                                        echo "<option value='" . $rs->rs_id . "'>" . $rs->rs_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Dokter</label>
                                                <select class="form-control select2bs4" id="spOption" name="sp" style="width: 100%;">
                                                    <option value="">---- Pilih ----</option>
                                                    <?php
                                                    foreach ($getSp as $sp) {
                                                        if($sp->doctor_name != 'SP.Obgyn + SP.An'){
                                                            echo "<option value='" . $sp->doctor_id . "'>" . $sp->doctor_name . "</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Tanggal</label>
                                                <div class="input-group date" id="dateSchedule" data-target-input="nearest">
                                                    <input type="text" name="dateSchedule" id="dateSchedule" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#dateSchedule">
                                                    <div class="input-group-append" data-target="#dateSchedule" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-8">
                                            <table id="tbSP" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr style="text-align: center;">
                                                        <th style="width: 140px;">Waktu</th>
                                                        <th>ON-SITE</th>
                                                        <th class="prwt">PRWT</th>
                                                        <th>ON-CALL</th>
                                                        <th>OFF</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="show_data" style="text-align: center;">
                                                    <tr style="background-color: darkseagreen;">
                                                        <td>
                                                            <strong style="margin-top: 12px;">Pilih Semua</strong>                                                            
                                                        </td>
                                                        <td>                                  
                                                            <div class="form-check">
                                                                <input class="form-check-input boxonSite" name="onSiteAll" id="onSiteAll"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                            </div>
                                                        </td>
                                                        <td class="prwt">                                                  
                                                            <div class="form-check">
                                                                <input class="form-check-input boxPrwt" name="prwtAll" id="prwtAll"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                            </div>
                                                        </td>
                                                        <td>                                                  
                                                            <div class="form-check">
                                                                <input class="form-check-input boxonCall" name="onCallAll" id="onCallAll"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                            </div>
                                                        </td>
                                                        <td>                                                  
                                                            <div class="form-check" id="off">
                                                                <input class="form-check-input boxOff" name="offAll" id="offAll"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    for ($x = 1; $x <= 9; $x++) {
                                                        $no = $x;
                                                        echo ('                                                        
                                                            <tr>
                                                                <td>
                                                                    <div class="card" style="width: 100px; height: 50px;">                                                        
                                                                        <strong style="margin-top: 12px;"><i class="fas fa-clock mr-1"></i> 0' . $no . ':00</strong>
                                                                    </div>
                                                                </td>
                                                                <td>                                  
                                                                    <div class="form-check">
                                                                        <input class="form-check-input boxonSite" name="onSite' . $no . '" id="onSite' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                                <td class="prwt">                                                  
                                                                    <div class="form-check">
                                                                        <input class="form-check-input boxPrwt" name="prwt' . $no . '" id="prwt' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                                <td>                                                  
                                                                    <div class="form-check">
                                                                        <input class="form-check-input boxonCall" name="onCall' . $no . '" id="onCall' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                                <td>                                                  
                                                                    <div class="form-check" id="off">
                                                                        <input class="form-check-input boxOff" name="off' . $no . '" id="off' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        ');
                                                    }
                                                    for ($x = 10; $x <= 24; $x++) {
                                                        $no = $x;
                                                        echo ('                                                        
                                                            <tr>
                                                                <td>
                                                                    <div class="card" style="width: 100px; height: 50px;">                                                        
                                                                        <strong style="margin-top: 12px;"><i class="fas fa-clock mr-1"></i> ' . $no . ':00</strong>
                                                                    </div>
                                                                </td>
                                                                <td>                                  
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" name="onSite' . $no . '" id="onSite' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                                <td class="prwt">                                                  
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" name="prwt' . $no . '" id="prwt' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                                <td>                                                  
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" name="onCall' . $no . '" id="onCall' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                                <td>                                                  
                                                                    <div class="form-check" id="off">
                                                                        <input class="form-check-input" name="off' . $no . '" id="off' . $no . '"  value="1" style="width: 20px; height: 20px;" type="checkbox" >                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        ');
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<?php 
    require('dokter_sp_action.php'); 
    require('dokter_sp_validate.php'); 
?>