<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Ketersediaan Ruangan</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Stock Room</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            
                            <!-- <form role="form" id="stockRoom" method="post" enctype="multipart/form-data" action="<?php echo base_url('transaction/StockRoom/save');?>">                                                         -->
                            <form action="#" role="form" id="stockRoom">
                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-sm-3" style="padding-bottom: 20px;">
                                            <div class="form-group">
                                                <label>Pilih Rumah Sakit</label>
                                                <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;">
                                                    <option value="">---- Pilih ----</option>
                                                    <?php
                                                    foreach ($getRs as $rs) {
                                                        echo "<option value='" . $rs->rs_id . "'>" . $rs->rs_name . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <table id="mydata" class="table table-bordered table-striped">
                                            <thead>
                                                <tr style="text-align: center;">
                                                    <th>Ruangan</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody id="show_data">
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/nicu.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> NICU</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomNICU" id="roomNICU" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</optio>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/icu.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> ICU</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomICU" id="roomICU" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/ruang_perawatan.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> HCU</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomHCU" id="roomHCU" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/hcu.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> Ruang Perawatan</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="ward" id="ward" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/perina_covid.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> Ruang Isolasi Ibu</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomIsolationMother" id="roomIsolationMother" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/picu_covid.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> Ruang Isolasi Bayi</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomIsloationBebies" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/isolasi_igd_ponek.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> Ruang Isolasi IGD Ponek</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomIsloationIgdPonek" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/icu_covid.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> ICU Covid</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomIcuCovid" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/perina.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> Perina Covid</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomReceivingCovid" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/picu2.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> PICU Covid</strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomPicuCovid" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <img src="<?php echo base_url("assets/images/icon/icon_room/nicu_covid.png"); ?>" style="width: 40px; height: 40px;">
                                                        <strong style="font-size: 17px; margin-left: 10px;"> NICU Covid </strong>
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select name="roomNicuCovid" class="form-control">
                                                                    <option value="">----- Pilih ------</option>
                                                                    <option name="tr" value="Tersedia">Tersedia</option>
                                                                    <option name="ph" value="Penuh">Penuh</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" id="btnSaveRoom" class="btn btn-primary">Update</button>
                                </div>

                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<div class="tampildata"></div>

</div>

<?php require('stock_room_action.php') ?>