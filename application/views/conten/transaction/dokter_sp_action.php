
<script>
    $(document).ready(function() {

        // REALTIME
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        var socket = io(socketIoAddress);

        // socket.on('fromServer', data => {
        //     console.log(data);
        // });

        //Date range picker
        // $("#dateSchedule").daterangepicker({
        //     singleDatePicker: true,
        //     showDropdowns: true,
        //     // autoUpdateInput: false,
        //     locale: {
        //         format: 'DD-MM-YYYY',
        //         firstDay: 1
        //     }
        // }, function(start, end) {
        //     console.log(start.format('DD-MM-YYYY'));
        //     ajaxPostSchedule();
        // });

        $("#dateSchedule").datetimepicker({
            format: "L",
        });
        $('#dateSchedule').on('change.datetimepicker', function(e){ 
            var formatedValue = e.date.format(e.date._f);
            console.log(formatedValue);
            ajaxPostSchedule();
        });

        $('select').change(function() {
            if ($(this).val() != "") {
                $(this).valid();
            }
        });

        $('#spOption').on('change', function() {
            var spId = $(this).val();
            if (spId == 'f5475789-a8f8-4aed-a1db-98ae68d77a69') {
                $('.prwt').show();
            } else {
                $('.prwt').hide();
            }
        });

        function ajaxPostSchedule() {
            $.ajax({
                url: "DokterSp/schedule",
                type: "POST",
                data: $('#dokterSp').serialize(),
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    console.log(data.status);
                    if (data.status == true) {
                        // REALTIME
                        socket.emit('koneksiSocket', {
                            waktu: "realtime"
                        });
                        //END//
                        var onsite = data.getOnsite;
                        if ($.trim(onsite)) {
                            if (onsite[0].hour_1 == "1") {
                                $('#onSite1').attr('checked', true);
                            }
                            if (onsite[0].hour_2 == "1") {
                                $('#onSite2').attr('checked', true);
                            }
                            if (onsite[0].hour_3 == "1") {
                                $('#onSite3').attr('checked', true);
                            }
                            if (onsite[0].hour_4 == "1") {
                                $('#onSite4').attr('checked', true);
                            }
                            if (onsite[0].hour_5 == "1") {
                                $('#onSite5').attr('checked', true);
                            }
                            if (onsite[0].hour_6 == "1") {
                                $('#onSite6').attr('checked', true);
                            }
                            if (onsite[0].hour_7 == "1") {
                                $('#onSite7').attr('checked', true);
                            }
                            if (onsite[0].hour_8 == "1") {
                                $('#onSite8').attr('checked', true);
                            }
                            if (onsite[0].hour_9 == "1") {
                                $('#onSite9').attr('checked', true);
                            }
                            if (onsite[0].hour_10 == "1") {
                                $('#onSite10').attr('checked', true);
                            }
                            if (onsite[0].hour_11 == "1") {
                                $('#onSite11').attr('checked', true);
                            }
                            if (onsite[0].hour_12 == "1") {
                                $('#onSite12').attr('checked', true);
                            }
                            if (onsite[0].hour_13 == "1") {
                                $('#onSite13').attr('checked', true);
                            }
                            if (onsite[0].hour_14 == "1") {
                                $('#onSite14').attr('checked', true);
                            }
                            if (onsite[0].hour_15 == "1") {
                                $('#onSite15').attr('checked', true);
                            }
                            if (onsite[0].hour_16 == "1") {
                                $('#onSite16').attr('checked', true);
                            }
                            if (onsite[0].hour_17 == "1") {
                                $('#onSite17').attr('checked', true);
                            }
                            if (onsite[0].hour_18 == "1") {
                                $('#onSite18').attr('checked', true);
                            }
                            if (onsite[0].hour_19 == "1") {
                                $('#onSite19').attr('checked', true);
                            }
                            if (onsite[0].hour_20 == "1") {
                                $('#onSite20').attr('checked', true);
                            }
                            if (onsite[0].hour_21 == "1") {
                                $('#onSite21').attr('checked', true);
                            }
                            if (onsite[0].hour_22 == "1") {
                                $('#onSite22').attr('checked', true);
                            }
                            if (onsite[0].hour_23 == "1") {
                                $('#onSite23').attr('checked', true);
                            }
                            if (onsite[0].hour_24 == "1") {
                                $('#onSite24').attr('checked', true);
                            }
                        }
                        var oncall = data.getOcall;
                        if ($.trim(oncall)) {
                            if (oncall[0].hour_1 == "1") {
                                $('#onCall1').attr('checked', true);
                            }
                            if (oncall[0].hour_2 == "1") {
                                $('#onCall2').attr('checked', true);
                            }
                            if (oncall[0].hour_3 == "1") {
                                $('#onCall3').attr('checked', true);
                            }
                            if (oncall[0].hour_4 == "1") {
                                $('#onCall4').attr('checked', true);
                            }
                            if (oncall[0].hour_5 == "1") {
                                $('#onCall5').attr('checked', true);
                            }
                            if (oncall[0].hour_6 == "1") {
                                $('#onCall6').attr('checked', true);
                            }
                            if (oncall[0].hour_7 == "1") {
                                $('#onCall7').attr('checked', true);
                            }
                            if (oncall[0].hour_8 == "1") {
                                $('#onCall8').attr('checked', true);
                            }
                            if (oncall[0].hour_9 == "1") {
                                $('#onCall9').attr('checked', true);
                            }
                            if (oncall[0].hour_10 == "1") {
                                $('#onCall10').attr('checked', true);
                            }
                            if (oncall[0].hour_11 == "1") {
                                $('#onCall11').attr('checked', true);
                            }
                            if (oncall[0].hour_12 == "1") {
                                $('#onCall12').attr('checked', true);
                            }
                            if (oncall[0].hour_13 == "1") {
                                $('#onCall13').attr('checked', true);
                            }
                            if (oncall[0].hour_14 == "1") {
                                $('#onCall14').attr('checked', true);
                            }
                            if (oncall[0].hour_15 == "1") {
                                $('#onCall15').attr('checked', true);
                            }
                            if (oncall[0].hour_16 == "1") {
                                $('#onCall16').attr('checked', true);
                            }
                            if (oncall[0].hour_17 == "1") {
                                $('#onCall17').attr('checked', true);
                            }
                            if (oncall[0].hour_18 == "1") {
                                $('#onCall18').attr('checked', true);
                            }
                            if (oncall[0].hour_19 == "1") {
                                $('#onCall19').attr('checked', true);
                            }
                            if (oncall[0].hour_20 == "1") {
                                $('#onCall20').attr('checked', true);
                            }
                            if (oncall[0].hour_21 == "1") {
                                $('#onCall21').attr('checked', true);
                            }
                            if (oncall[0].hour_22 == "1") {
                                $('#onCall22').attr('checked', true);
                            }
                            if (oncall[0].hour_23 == "1") {
                                $('#onCall23').attr('checked', true);
                            }
                            if (oncall[0].hour_24 == "1") {
                                $('#onCall24').attr('checked', true);
                            }
                        }
                        var off = data.getOff;
                        if ($.trim(off)) {
                            if (off[0].hour_1 == "1") {
                                $('#off1').attr('checked', true);
                            }
                            if (off[0].hour_2 == "1") {
                                $('#off2').attr('checked', true);
                            }
                            if (off[0].hour_3 == "1") {
                                $('#off3').attr('checked', true);
                            }
                            if (off[0].hour_4 == "1") {
                                $('#off4').attr('checked', true);
                            }
                            if (off[0].hour_5 == "1") {
                                $('#off5').attr('checked', true);
                            }
                            if (off[0].hour_6 == "1") {
                                $('#off6').attr('checked', true);
                            }
                            if (off[0].hour_7 == "1") {
                                $('#off7').attr('checked', true);
                            }
                            if (off[0].hour_8 == "1") {
                                $('#off8').attr('checked', true);
                            }
                            if (off[0].hour_9 == "1") {
                                $('#off9').attr('checked', true);
                            }
                            if (off[0].hour_10 == "1") {
                                $('#off10').attr('checked', true);
                            }
                            if (off[0].hour_11 == "1") {
                                $('#off11').attr('checked', true);
                            }
                            if (off[0].hour_12 == "1") {
                                $('#off12').attr('checked', true);
                            }
                            if (off[0].hour_13 == "1") {
                                $('#off13').attr('checked', true);
                            }
                            if (off[0].hour_14 == "1") {
                                $('#off14').attr('checked', true);
                            }
                            if (off[0].hour_15 == "1") {
                                $('#off15').attr('checked', true);
                            }
                            if (off[0].hour_16 == "1") {
                                $('#off16').attr('checked', true);
                            }
                            if (off[0].hour_17 == "1") {
                                $('#off17').attr('checked', true);
                            }
                            if (off[0].hour_18 == "1") {
                                $('#off18').attr('checked', true);
                            }
                            if (off[0].hour_19 == "1") {
                                $('#off19').attr('checked', true);
                            }
                            if (off[0].hour_20 == "1") {
                                $('#off20').attr('checked', true);
                            }
                            if (off[0].hour_21 == "1") {
                                $('#off21').attr('checked', true);
                            }
                            if (off[0].hour_22 == "1") {
                                $('#off22').attr('checked', true);
                            }
                            if (off[0].hour_23 == "1") {
                                $('#off23').attr('checked', true);
                            }
                            if (off[0].hour_24 == "1") {
                                $('#off24').attr('checked', true);
                            }
                        }
                        var prwt = data.getPrwt;
                        if ($.trim(prwt)) {
                            if (prwt[0].hour_1 == "1") {
                                $('#prwt1').attr('checked', true);
                            }
                            if (prwt[0].hour_2 == "1") {
                                $('#prwt2').attr('checked', true);
                            }
                            if (prwt[0].hour_3 == "1") {
                                $('#prwt3').attr('checked', true);
                            }
                            if (prwt[0].hour_4 == "1") {
                                $('#prwt4').attr('checked', true);
                            }
                            if (prwt[0].hour_5 == "1") {
                                $('#prwt5').attr('checked', true);
                            }
                            if (prwt[0].hour_6 == "1") {
                                $('#prwt6').attr('checked', true);
                            }
                            if (prwt[0].hour_7 == "1") {
                                $('#prwt7').attr('checked', true);
                            }
                            if (prwt[0].hour_8 == "1") {
                                $('#prwt8').attr('checked', true);
                            }
                            if (prwt[0].hour_9 == "1") {
                                $('#prwt9').attr('checked', true);
                            }
                            if (prwt[0].hour_10 == "1") {
                                $('#prwt10').attr('checked', true);
                            }
                            if (prwt[0].hour_11 == "1") {
                                $('#prwt11').attr('checked', true);
                            }
                            if (prwt[0].hour_12 == "1") {
                                $('#prwt12').attr('checked', true);
                            }
                            if (prwt[0].hour_13 == "1") {
                                $('#prwt13').attr('checked', true);
                            }
                            if (prwt[0].hour_14 == "1") {
                                $('#prwt14').attr('checked', true);
                            }
                            if (prwt[0].hour_15 == "1") {
                                $('#prwt15').attr('checked', true);
                            }
                            if (prwt[0].hour_16 == "1") {
                                $('#prwt16').attr('checked', true);
                            }
                            if (prwt[0].hour_17 == "1") {
                                $('#prwt17').attr('checked', true);
                            }
                            if (prwt[0].hour_18 == "1") {
                                $('#prwt18').attr('checked', true);
                            }
                            if (prwt[0].hour_19 == "1") {
                                $('#prwt19').attr('checked', true);
                            }
                            if (prwt[0].hour_20 == "1") {
                                $('#prwt20').attr('checked', true);
                            }
                            if (prwt[0].hour_21 == "1") {
                                $('#prwt21').attr('checked', true);
                            }
                            if (prwt[0].hour_22 == "1") {
                                $('#prwt22').attr('checked', true);
                            }
                            if (prwt[0].hour_23 == "1") {
                                $('#prwt23').attr('checked', true);
                            }
                            if (prwt[0].hour_24 == "1") {
                                $('#prwt24').attr('checked', true);
                            }
                        }
                    } else {
                        var no;
                        for (no = 1; no <= 24; no++) {
                            $('#onSite' + [no]).attr('checked', false);
                            $('#prwt' + [no]).attr('checked', false);
                            $('#onCall' + [no]).attr('checked', false);
                            $('#off' + [no]).attr('checked', false);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: errorThrown,
                    });
                }
            });
        }

        $.validator.setDefaults({
            submitHandler: function() {
                $.ajax({
                    url: "DokterSp/update",
                    type: "POST",
                    data: $('#dokterSp').serialize(),
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data);
                        if (data['status'] == true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Save',
                                text: 'Save data success!',
                            });
                        } else {
                            Swal.fire({
                                icon: 'warning',
                                title: 'Oops...',
                                text: 'Tidak Ada Perubahan Data!',
                            });
                        }
                        // location.reload();
                        // window.location.href = '<?php echo base_url('transaction/DokterSp') ?>';
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: errorThrown,
                        });
                    }
                });
            }
        });

        $('#dokterSp').validate({
            rules: {
                rs: {
                    required: true,
                },
                sp: {
                    required: true,
                },
                dateSchedule: {
                    required: true,
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });        

    });
</script>