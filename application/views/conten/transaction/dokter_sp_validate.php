<script>
    $(document).ready(function() {        

        // onSite
        $('#onSiteAll').change(function() {
            if ($(this).prop('checked')) {      
                $('#offAll').prop('checked', false);
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#onSite'+no).prop('checked', true);
                    $('#off'+no).prop('checked', false);
                }
            }else{
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#onSite'+no).prop('checked', false);
                }
            }
        });
        // prwt
        $('#prwtAll').change(function() {
            if ($(this).prop('checked')) {      
                $('#offAll').prop('checked', false);
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#prwt'+no).prop('checked', true);
                    $('#off'+no).prop('checked', false);
                }
            }else{
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#prwt'+no).prop('checked', false);
                }
            }
        });
        // onCall
        $('#onCallAll').change(function() {
            if ($(this).prop('checked')) {                
                $('#offAll').prop('checked', false);
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#onCall'+no).prop('checked', true);
                    $('#off'+no).prop('checked', false);
                }
            }else{
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#onCall'+no).prop('checked', false);
                }
            }
        });
        // off
        $('#offAll').change(function() {
            if ($(this).prop('checked')) {
                //headerbox
                $('#onSiteAll').prop('checked', false);
                $('#prwtAll').prop('checked', false);
                $('#onCallAll').prop('checked', false);

                var no;
                for (no = 0; no <= 24; no++) {
                    $('#off'+no).prop('checked', true);
                    //child
                    $('#onSite'+no).prop('checked', false);
                    $('#prwt'+no).prop('checked', false);
                    $('#onCall'+no).prop('checked', false);
                }
            }else{
                var no;
                for (no = 0; no <= 24; no++) {
                    $('#off'+no).prop('checked', false);
                }
            }
        });

        // JAM 1
        $('#off1').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite1').prop('checked', false);
                $('#onCall1').prop('checked', false);
                $('#prwt1').prop('checked', false);
            }
        });
        $('#onSite1').change(function() {
            if ($(this).prop('checked')) {
                $('#off1').prop('checked', false);
            }
        });
        $('#onCall1').change(function() {
            if ($(this).prop('checked')) {
                $('#off1').prop('checked', false);
            }
        });
        $('#prwt1').change(function() {
            if ($(this).prop('checked')) {
                $('#off1').prop('checked', false);
            }
        });

        // JAM 2
        $('#off2').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite2').prop('checked', false);
                $('#onCall2').prop('checked', false);
                $('#prwt2').prop('checked', false);
            }
        });
        $('#onSite2').change(function() {
            if ($(this).prop('checked')) {
                $('#off2').prop('checked', false);
            }
        });
        $('#onCall2').change(function() {
            if ($(this).prop('checked')) {
                $('#off2').prop('checked', false);
            }
        });
        $('#prwt2').change(function() {
            if ($(this).prop('checked')) {
                $('#off2').prop('checked', false);
            }
        });

        // JAM 3
        $('#off3').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite3').prop('checked', false);
                $('#onCall3').prop('checked', false);
                $('#prwt3').prop('checked', false);
            }
        });
        $('#onSite3').change(function() {
            if ($(this).prop('checked')) {
                $('#off3').prop('checked', false);
            }
        });
        $('#onCall3').change(function() {
            if ($(this).prop('checked')) {
                $('#off3').prop('checked', false);
            }
        });
        $('#prwt3').change(function() {
            if ($(this).prop('checked')) {
                $('#off3').prop('checked', false);
            }
        });

        // JAM 4
        $('#off4').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite4').prop('checked', false);
                $('#onCall4').prop('checked', false);
                $('#prwt4').prop('checked', false);
            }
        });
        $('#onSite4').change(function() {
            if ($(this).prop('checked')) {
                $('#off4').prop('checked', false);
            }
        });
        $('#onCall4').change(function() {
            if ($(this).prop('checked')) {
                $('#off4').prop('checked', false);
            }
        });
        $('#prwt4').change(function() {
            if ($(this).prop('checked')) {
                $('#off4').prop('checked', false);
            }
        });

        // JAM 5
        $('#off5').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite5').prop('checked', false);
                $('#onCall5').prop('checked', false);
                $('#prwt5').prop('checked', false);
            }
        });
        $('#onSite5').change(function() {
            if ($(this).prop('checked')) {
                $('#off5').prop('checked', false);
            }
        });
        $('#onCall5').change(function() {
            if ($(this).prop('checked')) {
                $('#off5').prop('checked', false);
            }
        });
        $('#prwt5').change(function() {
            if ($(this).prop('checked')) {
                $('#off5').prop('checked', false);
            }
        });

        // JAM 6
        $('#off6').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite6').prop('checked', false);
                $('#onCall6').prop('checked', false);
                $('#prwt6').prop('checked', false);
            }
        });
        $('#onSite6').change(function() {
            if ($(this).prop('checked')) {
                $('#off6').prop('checked', false);
            }
        });
        $('#onCall6').change(function() {
            if ($(this).prop('checked')) {
                $('#off6').prop('checked', false);
            }
        });
        $('#prwt6').change(function() {
            if ($(this).prop('checked')) {
                $('#off6').prop('checked', false);
            }
        });

        // JAM 7
        $('#off7').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite7').prop('checked', false);
                $('#onCall7').prop('checked', false);
                $('#prwt7').prop('checked', false);
            }
        });
        $('#onSite7').change(function() {
            if ($(this).prop('checked')) {
                $('#off7').prop('checked', false);
            }
        });
        $('#onCall7').change(function() {
            if ($(this).prop('checked')) {
                $('#off7').prop('checked', false);
            }
        });
        $('#prwt7').change(function() {
            if ($(this).prop('checked')) {
                $('#off7').prop('checked', false);
            }
        });

        // JAM 8
        $('#off8').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite8').prop('checked', false);
                $('#onCall8').prop('checked', false);
                $('#prwt8').prop('checked', false);
            }
        });
        $('#onSite8').change(function() {
            if ($(this).prop('checked')) {
                $('#off8').prop('checked', false);
            }
        });
        $('#onCall8').change(function() {
            if ($(this).prop('checked')) {
                $('#off8').prop('checked', false);
            }
        });
        $('#prwt8').change(function() {
            if ($(this).prop('checked')) {
                $('#off8').prop('checked', false);
            }
        });

        // JAM 9
        $('#off9').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite9').prop('checked', false);
                $('#onCall9').prop('checked', false);
                $('#prwt9').prop('checked', false);
            }
        });
        $('#onSite9').change(function() {
            if ($(this).prop('checked')) {
                $('#off9').prop('checked', false);
            }
        });
        $('#onCall9').change(function() {
            if ($(this).prop('checked')) {
                $('#off9').prop('checked', false);
            }
        });
        $('#prwt9').change(function() {
            if ($(this).prop('checked')) {
                $('#off9').prop('checked', false);
            }
        });

        // JAM 10
        $('#off10').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite10').prop('checked', false);
                $('#onCall10').prop('checked', false);
                $('#prwt10').prop('checked', false);
            }
        });
        $('#onSite10').change(function() {
            if ($(this).prop('checked')) {
                $('#off10').prop('checked', false);
            }
        });
        $('#onCall10').change(function() {
            if ($(this).prop('checked')) {
                $('#off10').prop('checked', false);
            }
        });
        $('#prwt10').change(function() {
            if ($(this).prop('checked')) {
                $('#off10').prop('checked', false);
            }
        });

        // JAM 11
        $('#off11').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite11').prop('checked', false);
                $('#onCall11').prop('checked', false);
                $('#prwt11').prop('checked', false);
            }
        });
        $('#onSite11').change(function() {
            if ($(this).prop('checked')) {
                $('#off11').prop('checked', false);
            }
        });
        $('#onCall11').change(function() {
            if ($(this).prop('checked')) {
                $('#off11').prop('checked', false);
            }
        });
        $('#prwt11').change(function() {
            if ($(this).prop('checked')) {
                $('#off11').prop('checked', false);
            }
        });

        // JAM 12
        $('#off12').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite12').prop('checked', false);
                $('#onCall12').prop('checked', false);
                $('#prwt12').prop('checked', false);
            }
        });
        $('#onSite12').change(function() {
            if ($(this).prop('checked')) {
                $('#off12').prop('checked', false);
            }
        });
        $('#onCall12').change(function() {
            if ($(this).prop('checked')) {
                $('#off12').prop('checked', false);
            }
        });
        $('#prwt12').change(function() {
            if ($(this).prop('checked')) {
                $('#off12').prop('checked', false);
            }
        });

        // JAM 13
        $('#off13').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite13').prop('checked', false);
                $('#onCall13').prop('checked', false);
                $('#prwt13').prop('checked', false);
            }
        });
        $('#onSite13').change(function() {
            if ($(this).prop('checked')) {
                $('#off13').prop('checked', false);
            }
        });
        $('#onCall13').change(function() {
            if ($(this).prop('checked')) {
                $('#off13').prop('checked', false);
            }
        });
        $('#prwt13').change(function() {
            if ($(this).prop('checked')) {
                $('#off13').prop('checked', false);
            }
        });

        // JAM 14
        $('#off14').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite14').prop('checked', false);
                $('#onCall14').prop('checked', false);
                $('#prwt14').prop('checked', false);
            }
        });
        $('#onSite14').change(function() {
            if ($(this).prop('checked')) {
                $('#off14').prop('checked', false);
            }
        });
        $('#onCall14').change(function() {
            if ($(this).prop('checked')) {
                $('#off14').prop('checked', false);
            }
        });
        $('#prwt14').change(function() {
            if ($(this).prop('checked')) {
                $('#off14').prop('checked', false);
            }
        });

        // JAM 15
        $('#off15').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite15').prop('checked', false);
                $('#onCall15').prop('checked', false);
                $('#prwt15').prop('checked', false);
            }
        });
        $('#onSite15').change(function() {
            if ($(this).prop('checked')) {
                $('#off15').prop('checked', false);
            }
        });
        $('#onCall15').change(function() {
            if ($(this).prop('checked')) {
                $('#off15').prop('checked', false);
            }
        });
        $('#prwt15').change(function() {
            if ($(this).prop('checked')) {
                $('#off15').prop('checked', false);
            }
        });

        // JAM 16
        $('#off16').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite16').prop('checked', false);
                $('#onCall16').prop('checked', false);
                $('#prwt16').prop('checked', false);
            }
        });
        $('#onSite16').change(function() {
            if ($(this).prop('checked')) {
                $('#off16').prop('checked', false);
            }
        });
        $('#onCall16').change(function() {
            if ($(this).prop('checked')) {
                $('#off16').prop('checked', false);
            }
        });
        $('#prwt16').change(function() {
            if ($(this).prop('checked')) {
                $('#off16').prop('checked', false);
            }
        });

        // JAM 17
        $('#off17').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite17').prop('checked', false);
                $('#onCall17').prop('checked', false);
                $('#prwt17').prop('checked', false);
            }
        });
        $('#onSite17').change(function() {
            if ($(this).prop('checked')) {
                $('#off17').prop('checked', false);
            }
        });
        $('#onCall17').change(function() {
            if ($(this).prop('checked')) {
                $('#off17').prop('checked', false);
            }
        });
        $('#prwt17').change(function() {
            if ($(this).prop('checked')) {
                $('#off17').prop('checked', false);
            }
        });

        // JAM 18
        $('#off18').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite18').prop('checked', false);
                $('#onCall18').prop('checked', false);
                $('#prwt18').prop('checked', false);
            }
        });
        $('#onSite18').change(function() {
            if ($(this).prop('checked')) {
                $('#off18').prop('checked', false);
            }
        });
        $('#onCall18').change(function() {
            if ($(this).prop('checked')) {
                $('#off18').prop('checked', false);
            }
        });
        $('#prwt19').change(function() {
            if ($(this).prop('checked')) {
                $('#off19').prop('checked', false);
            }
        });

        // JAM 20
        $('#off20').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite20').prop('checked', false);
                $('#onCall20').prop('checked', false);
                $('#prwt20').prop('checked', false);
            }
        });
        $('#onSite20').change(function() {
            if ($(this).prop('checked')) {
                $('#off20').prop('checked', false);
            }
        });
        $('#onCall20').change(function() {
            if ($(this).prop('checked')) {
                $('#off20').prop('checked', false);
            }
        });
        $('#prwt20').change(function() {
            if ($(this).prop('checked')) {
                $('#off20').prop('checked', false);
            }
        });

        // JAM 21
        $('#off21').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite21').prop('checked', false);
                $('#onCall21').prop('checked', false);
                $('#prwt21').prop('checked', false);
            }
        });
        $('#onSite21').change(function() {
            if ($(this).prop('checked')) {
                $('#off21').prop('checked', false);
            }
        });
        $('#onCall21').change(function() {
            if ($(this).prop('checked')) {
                $('#off21').prop('checked', false);
            }
        });
        $('#prwt21').change(function() {
            if ($(this).prop('checked')) {
                $('#off21').prop('checked', false);
            }
        });

        // JAM 22
        $('#off22').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite22').prop('checked', false);
                $('#onCall22').prop('checked', false);
                $('#prwt22').prop('checked', false);
            }
        });
        $('#onSite22').change(function() {
            if ($(this).prop('checked')) {
                $('#off22').prop('checked', false);
            }
        });
        $('#onCall22').change(function() {
            if ($(this).prop('checked')) {
                $('#off22').prop('checked', false);
            }
        });
        $('#prwt22').change(function() {
            if ($(this).prop('checked')) {
                $('#off22').prop('checked', false);
            }
        });

        // JAM 23
        $('#off23').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite23').prop('checked', false);
                $('#onCall23').prop('checked', false);
                $('#prwt23').prop('checked', false);
            }
        });
        $('#onSite23').change(function() {
            if ($(this).prop('checked')) {
                $('#off23').prop('checked', false);
            }
        });
        $('#onCall23').change(function() {
            if ($(this).prop('checked')) {
                $('#off23').prop('checked', false);
            }
        });
        $('#prwt23').change(function() {
            if ($(this).prop('checked')) {
                $('#off23').prop('checked', false);
            }
        });

        // JAM 24
        $('#off24').change(function() {
            if ($(this).prop('checked')) {
                $('#onSite24').prop('checked', false);
                $('#onCall24').prop('checked', false);
                $('#prwt24').prop('checked', false);
            }
        });
        $('#onSite24').change(function() {
            if ($(this).prop('checked')) {
                $('#off24').prop('checked', false);
            }
        });
        $('#onCall24').change(function() {
            if ($(this).prop('checked')) {
                $('#off24').prop('checked', false);
            }
        });
        $('#prwt24').change(function() {
            if ($(this).prop('checked')) {
                $('#off24').prop('checked', false);
            }
        });

    });
</script>