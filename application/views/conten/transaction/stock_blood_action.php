<script>
    $(document).ready(function() {

        // REALTIME
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        var socket = io(socketIoAddress);

        $('select').change(function() {
            if ($(this).val() != "") {
                $(this).valid();
            }
        });

        $('#rsOption').on('change', function() {
            var rsId = $(this).val();
            if ($.trim(rsId)) {
                $.ajax({
                    url: "StockBlood/getSelectRs/" + rsId,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data) {
                        // console.log(data);
                        //BOOLD A
                        $('[name="aWb"]').val(data[0].blood_aWb);
                        $('[name="aPrc"]').val(data[0].blood_aPrc);
                        $('[name="aTc"]').val(data[0].blood_aTc);
                        //BOOLD AB
                        $('[name="abWb"]').val(data[0].blood_abWb);
                        $('[name="abPrc"]').val(data[0].blood_abPrc);
                        $('[name="abTc"]').val(data[0].blood_abTc);
                        //BOOLD B
                        $('[name="bWb"]').val(data[0].blood_bWb);
                        $('[name="bPrc"]').val(data[0].blood_bPrc);
                        $('[name="bTc"]').val(data[0].blood_bTc);
                        //BOOLD O
                        $('[name="oWb"]').val(data[0].blood_oWb);
                        $('[name="oPrc"]').val(data[0].blood_oPrc);
                        $('[name="oTc"]').val(data[0].blood_oTc);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Error deleting data',
                        });
                    }
                });
            } else {
                window.location.href = '<?php echo base_url('transaction/StockBlood') ?>';
            }

        });

        $.validator.setDefaults({
            submitHandler: function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Update Hospital Data",
                    icon: 'warning',
                    showCancelButton: true,
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: "stockBlood/Save",
                            type: "POST",
                            data: $('#stockBlood').serialize(),
                            dataType: "JSON",
                            success: function(data) {
                                if (data['status'] == true) {
                                    // REALTIME
                                    socket.emit('koneksiSocket', {
                                        waktu: "realtime"
                                    });
                                    //END//
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Save',
                                        text: 'Save data success!',
                                    });
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong!',
                                    });
                                }
                                // console.log(data);
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Error Save Data',
                                });
                            }
                        });
                    }
                });
            }
        });

        $('#stockBlood').validate({
            rules: {
                rs: {
                    required: true,
                },
                //Blood A
                aWb: {
                    required: true,
                },
                aPrc: {
                    required: true,
                },
                aTc: {
                    required: true,
                },
                //Blood AB
                abWb: {
                    required: true,
                },
                abPrc: {
                    required: true,
                },
                abTc: {
                    required: true,
                },
                //Blood B
                bWb: {
                    required: true,
                },
                bPrc: {
                    required: true,
                },
                bTc: {
                    required: true,
                },
                //Blood O
                oWb: {
                    required: true,
                },
                oPrc: {
                    required: true,
                },
                oTc: {
                    required: true,
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        socket.on('fromServer', data => {
            console.log(data);
        });

    });

    function reloadTable() {
        table.ajax.reload(null, false); //reload datatable ajax   
    }
</script>