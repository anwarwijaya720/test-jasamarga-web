<script>
    $(document).ready(function() {

        // REALTIME
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        var socket = io(socketIoAddress);

        $('select').change(function() {
            if ($(this).val() != "") {
                $(this).valid();
            }
        });

        $('#rsOption').on('change', function() {
            var rsId = $(this).val();
            if ($.trim(rsId)) {
                $.ajax({
                    url: "StockRoom/getStock/" + rsId,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data) {
                        if($.trim(data)){
                            $('[name="roomNICU"]').val(data[0].nicu);
                            $('[name="roomICU"]').val(data[0].icu);
                            $('[name="roomHCU"]').val(data[0].hcu);
                            $('[name="ward"]').val(data[0].ruang_perawatan);
                            $('[name="roomIsolationMother"]').val(data[0].ruang_isolasi_ibu);
                            $('[name="roomIsloationBebies"]').val(data[0].ruang_isolasi_bayi);
                            $('[name="roomIsloationIgdPonek"]').val(data[0].isolasi_igd_ponek);
                            $('[name="roomIcuCovid"]').val(data[0].icu_covid);
                            $('[name="roomReceivingCovid"]').val(data[0].perina_covid);
                            $('[name="roomPicuCovid"]').val(data[0].picu_covid);
                            $('[name="roomNicuCovid"]').val(data[0].nicu_covid);
                        }else{
                            $('[name="roomNICU"]').val(null);
                            $('[name="roomICU"]').val(null);
                            $('[name="roomHCU"]').val(null);
                            $('[name="ward"]').val(null);
                            $('[name="roomIsolationMother"]').val(null);
                            $('[name="roomIsloationBebies"]').val(null);
                            $('[name="roomIsloationIgdPonek"]').val(null);
                            $('[name="roomIcuCovid"]').val(null);
                            $('[name="roomReceivingCovid"]').val(null);
                            $('[name="roomPicuCovid"]').val(null);
                            $('[name="roomNicuCovid"]').val(null);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Error deleting data',
                        });
                    }
                });
            } else {
                window.location.href = '<?php echo base_url('transaction/StockRoom') ?>';
            }
        });

        $.validator.setDefaults({
            submitHandler: function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Update Hospital Data",
                    icon: 'warning',
                    showCancelButton: true,
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: "StockRoom/Save",
                            type: "POST",
                            data: $('#stockRoom').serialize(),
                            dataType: "JSON",
                            success: function(data) {                                
                                if (data['status'] == true) {
                                    //======== REALTIME ==========//
                                    socket.emit('koneksiSocket', {
                                        waktu: "realtime"
                                    });
                                    //==========================//
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Save',
                                        text: 'Save data success!',
                                    });
                                    location.reload();
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong!',
                                    });
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Error Save Data',
                                });
                            }
                        });
                    }
                });

            }
        });

        $('#stockRoom').validate({
            rules: {
                // rs: {
                //     required: true,
                // },
                // roomNICU: {
                //     required: true,
                // },
                // roomICU: {
                //     required: true,
                // },
                // roomHCU: {
                //     required: true,
                // },
                // ward: {
                //     required: true,
                // },
                // roomIsolationMother: {
                //     required: true,
                // },
                // roomIsloationBebies: {
                //     required: true,
                // },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        socket.on('fromServer', data => {
            console.log(data);
        });

    });
</script>