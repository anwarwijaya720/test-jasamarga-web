<script>
    $(document).ready(function() {

        tampil_data();
        $('#mydata').dataTable();
        //fungsi tampil barang
        function tampil_data() {
            $.ajax({
                type: 'ajax',
                url: "<?php echo base_url('report/Rekapitulasi/getUsers') ?>",
                // async: false,
                dataType: 'json',
                success: function(data) {
                    // console.log(data);
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        var no = i + 1;
                        html += '<tr>' +
                            '<td id="num">' + data[i].rs_name + '</td>' +
                            '<td>' + data[i].tanggal + '</td>' +
                            '<td>' + data[i].total + '</td>' +
                            '</tr>';

                    }
                    $('#show_data').html(html);
                }

            });
        }

        $('select').change(function() {
            if ($(this).val() != "") {
                $(this).valid();
            }
        });

        // $.validator.setDefaults({
        //     submitHandler: function() {
        //         // $.ajax({
        //         //     url: "Rekapitulasi/export",
        //         //     type: "POST",
        //         //     data: $('#formRekap').serialize(),
        //         //     dataType: "JSON",
        //         //     success: function(data) {
        //         //         // console.log(data);
        //         //         if (data['status'] == true) {
        //         //             Swal.fire({
        //         //                 icon: 'success',
        //         //                 title: 'Upload',
        //         //                 text: 'upload data success!',
        //         //             });
        //         //         } else {
        //         //             Swal.fire({
        //         //                 icon: 'error',
        //         //                 title: 'Oops...',
        //         //                 text: 'Something went wrong!',
        //         //             });
        //         //         }
        //         //     },
        //         //     error: function(jqXHR, textStatus, errorThrown) {
        //         //         Swal.fire({
        //         //             icon: 'error',
        //         //             title: 'Oops...',
        //         //             text: errorThrown,
        //         //         });
        //         //     }
        //         // });
        //     }
        // });
        // $('#formRekap').validate({
        //     rules: {
        //         // date1: {
        //         //     required: true,
        //         //     date: true,
        //         // },
        //         // date2: {
        //         //     required: true,
        //         //     date: true,
        //         // },
        //         // rs: {
        //         //     required: true,
        //         // },
        //         // jumlah: {
        //         //     required: true
        //         // }
        //     },
        //     messages: {
        //         // date1: {
        //         //     required: "Date cannot be empty",
        //         //     date: "Please enter date format correctly"
        //         // },
        //         // date2: {
        //         //     required: "Date cannot be empty",
        //         //     date: "Please enter date format correctly"
        //         // },
        //         // rs: {
        //         //     required: "Please select",
        //         // },
        //         // jumlah: {
        //         //     required: "Please select",
        //         // },
        //     },
        //     errorElement: 'span',
        //     errorPlacement: function(error, element) {
        //         error.addClass('invalid-feedback');
        //         element.closest('.form-group').append(error);
        //     },
        //     highlight: function(element, errorClass, validClass) {
        //         $(element).addClass('is-invalid');
        //     },
        //     unhighlight: function(element, errorClass, validClass) {
        //         $(element).removeClass('is-invalid');
        //     }
        // });

    });
</script>