<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Rekapitulasi</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Report</h3>
                        </div>

                        <div class="card-body">

                            <!-- <form role="form" id="formRekap"> -->
                            <form role="form" id="formRekap" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url('report/Rekapitulasi/export');?>"> 
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Dari:</label>
                                                <div class="input-group date" id="date1" data-target-input="nearest">
                                                    <input type="text" name="date1" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#date1" required>
                                                    <div class="input-group-append" data-target="#date1" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Sampai:</label>
                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <input type="text" name="date2" class="form-control datetimepicker-input" data-toggle="datetimepicker" data-target="#reservationdate" required>
                                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-md-6">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Pilih Rumah Sakit</label>
                                                    <select class="form-control select2bs4" id="rsOption" name="rs" style="width: 100%;" required>
                                                        <option value="">---- Pilih ----</option>
                                                        <?php
                                                        foreach ($getRs as $rs) {
                                                            echo "<option value='" . $rs->rs_id . "'>" . $rs->rs_name . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group" style="margin-left: 5px;"> 
                                                <label>Indikator</label>
                                                <select class="form-control select2bs4" name="indikator" style="width: 100%;" required>
                                                    <option selected="selected" value="">--- Pilih ---</option>
                                                    <option value="rujukMom" >Jumlah ibu yang dirujuk menggunakan aplikasi ini</option>
                                                    <option value="rujukMomOnsite">Jumlah ibu yang mendapatkan pelayanan dokter spesialis secara onsite</option>
                                                    <option value="rujukMomRoom">Jumlah ibu yang mendapatkan ruangan</option>
                                                    <option value="rujukNeonatal">Jumlah neonatal yang dirujuk menggunakan aplikasi ini</option>
                                                    <option value="rujukNeonatalOnsite">Jumlah neonatal yang mendapatkan pelayanan dokter spesialis secara onsite</option>
                                                    <option value="rujukNeonatalRoom">Jumlah neonatal yang mendapatkan ruangan</option>
                                                    <option value="updateLayanan">Jumlah rumah sakit yang patuh update ketersediaan layanan (< 9 Jam)</option>
                                                    <option value="rujukBatal">Jumlah perujuk yang melakukan batal rujuk</option>
                                                </select>
                                            </div>
                                            <!-- /.form-group -->
                                        </div>

                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Export To Excel</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table id="mydata" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Instansi</th>
                                <th>Tanggal</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody id="show_data">

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Instansi</th>
                                <th>Tanggal</th>
                                <th>Jumlah</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
    </section>

    <?php require('rekapilulasi_action.php') ?>