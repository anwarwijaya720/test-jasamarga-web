<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Example Pagination</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Users</h3>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">

                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group" id="mRs">
                                        <label class="control-label col-md-10">Asal Faskes</label>
                                        <div class="col-md-12">
                                            <select name="rs" class="form-control select2bs4" name="rs" style="width: 40%;">
                                                <option selected="selected" value="">---- Pilih ----</option>
                                                <?php
                                                foreach ($getRs as $rs) {
                                                    echo "<option value='" . $rs->rs_id . "'>" . $rs->rs_name . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="form-group" id="mRs">
                                        <label class="control-label col-md-10">Asal Faskes</label>
                                        <div class="col-md-12">
                                            <!-- <select id="itemName" class="form-control" style="width:500px" name="itemName">                                     -->
                                            <!-- <option value='0'>-- Select user --</option> -->
                                            <!-- </select> -->
                                            <select class="search form-control" name="search" style="width: 40%;">
                                                <option selected="selected" value="">---- Pilih ----</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <!-- <button id="target" type="button" class="btn btn-warning">Warning</button> -->

                            <!-- <table id="mytable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>No.Hp</th>
                                        <th>Instantsi</th>
                                        <th>Jenis Pengguna</th>
                                        <th>Profesi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="show_data">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>No.Hp</th>
                                        <th>Instantsi</th>
                                        <th>Jenis Pengguna</th>
                                        <th>Profesi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table> -->

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<?php require('pagination_action.php') ?>