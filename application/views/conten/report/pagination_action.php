<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {    

        $('#itemName2').select2({
            placeholder: '--- Select Item ---',
            ajax: {
                type: 'ajax',
                url: "<?php echo base_url('report/ExamplePagination/getUsers') ?>",
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    console.log(data)
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        $("#itemName").select2({
            ajax: {
                url: "<?php echo base_url('report/ExamplePagination/getUsers') ?>",
                type: "post",
                dataType: 'json',
                delay: 250,
                // data: function(params) {
                //     console.log("coba");
                //     return {
                //         searchTerm: params.term // search term
                //     };
                // },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.NAME,
                                id: item.ID
                            }
                        })
                    };
                },
                // cache: true
            }
        });

        $('.search').select2({
            placeholder: '--- Search User ---',
            ajax: {
                url: "<?php echo base_url('report/ExamplePagination/getUsers') ?>",
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        $("#target").click(function() {
            // $.ajax({
            //     type: 'ajax',
            //     url: "<?php echo base_url('report/ExamplePagination/getUsers') ?>",
            //     dataType: 'json',
            //     success: function(data) {
            //         console.log(data)
            //     }
            // });

            $("#itemName").select2({
                ajax: {
                    url: "<?php echo base_url('report/ExamplePagination/getUsers') ?>",
                    type: "post",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        console.log(params);
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    text: item.NAME,
                                    id: item.ID
                                }
                            })
                        };
                    },
                    // cache: true
                }
            });

        });

    });
</script>