<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Upload Master Rs</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <fieldset>
                <legend>Form Upload Excel</legend>
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url('report/Uploadrs/upload'); ?>">
                    <div class="form-group">
                        <label for="exampleInputFile">File Upload</label>
                        <input type="file" name="berkas" class="form-control" id="exampleInputFile" style="width: 300px; height: 45px;">
                    </div>
                    <button type="submit" class="btn btn-primary">Import</button>
                </form>
            </fieldset> 

        </div>
    </section>
</div>



