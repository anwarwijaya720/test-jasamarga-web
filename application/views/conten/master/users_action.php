<script type="text/javascript">
  var save_method; //for save method string
  var table;

  $(document).ready(function() {
    table = $('#mytable').DataTable({
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.      
      "responsive": true,
      "autoWidth": false,
      "order": [], //Initial no order.
      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url('master/Users/getAllUsers') ?>",
        "type": "POST"
      },
      //Set column definition initialisation properties.
      "columnDefs": [{
        "targets": [0], //last column
        "orderable": [0, 1], //set not orderable
      }, ],
    });


    $('select').change(function() {
      if ($(this).val() != "") {
        $(this).valid();
      }
    });

    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
    $("textarea").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });

  });

  function reloadTable() {
    table.ajax.reload(null, false); //reload datatable ajax   
  }

  function edit_person(user_id) {
    // alert(username);
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#btnSave').html('Approve');
    $('#mRs').show();

    //Ajax Load data from ajax
    $.ajax({
      url: "Users/edit_user/" + user_id,
      type: "GET",
      dataType: "JSON",
      success: function(data) {
        $('#pass').hide();
        $('#conPass').hide();
        $('[name="usersId"]').val(data[0].user_id);
        $('[name="username"]').val(data[0].username);
        if ($.trim(data[0].rs_name)) {
          $('[name="instansi"]').val(data[0].rs_name);
        } else {
          $('[name="instansi"]').val(data[0].institusi);
        }
        $('[name="rs"]').val(data[0].rs_id);
        $('[name="position"]').val(data[0].profesi);
        $('[name="noHp"]').val(data[0].no_hp);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit'); // Set title to Bootstrap modal title
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert('Error getting data from ajax');
      }
    });
  }

  function delete_person(user_id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
    }).then((result) => {
      console.log(result);
      if (result.value) {
        // FUNGSI DELETE
        $.ajax({
          url: "Users/deletUser/" + user_id,
          type: "POST",
          dataType: "JSON",
          success: function(data) {
            if (data['status'] == true) {
              // REALTIME
              var socketIoAddress = "<?php echo URL_SOCKET; ?>";
              var socket = io(socketIoAddress);

              socket.emit('koneksiSocket', {
                waktu: "realtime"
              }); // REALTIME

              Swal.fire({
                icon: 'success',
                title: 'Delete',
                text: 'delete data success!',
              });
              //if success reload ajax table
              reloadTable();
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
              });
              reloadTable();
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Error deleting data',
            });
          }
        });

      }
    });
  }

  function approve_person(user_id) {
    Swal.fire({
      title: 'Are you sure?',
      text: "Approve Users",
      icon: 'warning',
      showCancelButton: true,
    }).then((result) => {
      console.log(result);
      if (result.value) {
        // FUNGSI DELETE
        $.ajax({
          url: "Users/approveUser/" + user_id,
          type: "POST",
          dataType: "JSON",
          success: function(data) {
            if (data['status'] == true) {
              Swal.fire({
                icon: 'success',
                title: 'Delete',
                text: 'delete data success!',
              });
              //if success reload ajax table
              reloadTable();
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
              });
              reloadTable();
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Error deleting data',
            });
          }
        });
      }
    });
  }

  function addEstudiante() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Users'); // Set Title to Bootstrap modal title
    $('#pass').show();
    $('#conPass').show();
    $('#mRs').hide();
    $('#btnSave').html('Save');
  }

  function save() {
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled', true); //set button disable    
    var url;
    var msg;

    if (save_method == 'add') {
      url = "Users/ajax_add/" + save_method;
      msg = 'add user success!';
      msgErr = 'Error Add User';
      btnName = 'Save';
    } else {
      url = "Users/ajax_update/" + save_method;
      msg = 'updated user success!';
      msgErr = 'Error Updated User';
      btnName = 'Approve';
    }

    // ajax adding data to database
    $.ajax({
      url: url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data) {
        if (data.status) //if success close modal and reload ajax table
        {
          // REALTIME
          var socketIoAddress = "<?php echo URL_SOCKET; ?>";
          var socket = io(socketIoAddress);

          socket.emit('koneksiSocket', {
            waktu: "realtime"
          }); // REALTIME
          
          Swal.fire({
            icon: 'success',
            title: 'User',
            text: msg,
          });
          $('#modal_form').modal('hide');
          reloadTable();
        } else {
          for (var i = 0; i < data.inputerror.length; i++) {
            $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
          }
        }
        $('#btnSave').text(btnName); //change button text
        $('#btnSave').attr('disabled', false); //set button enable
      },
      error: function(jqXHR, textStatus, errorThrown) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: msgErr,
        });
        $('#btnSave').text(btnName); //change button text
        $('#btnSave').attr('disabled', false); //set button enable
        reloadTable();
      }
    });

  }
</script>