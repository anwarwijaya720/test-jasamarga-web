<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
    $(document).ready(function() {

        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        const socket = io(socketIoAddress);

        tampil_data();
        $('#mydata').dataTable();
        //fungsi tampil barang
        function tampil_data() {
            $.ajax({
                type: 'ajax',
                url: "<?php echo base_url('master/Register/getUsers') ?>",
                async: false,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        var no = i + 1;
                        html += '<tr>' +
                            '<td>' + no + '</td>' +
                            '<td id="num">' + data[i].username + '</td>' +
                            '<td>' + data[i].no_hp + '</td>' +
                            '<td>' + data[i].status + '</td>' +
                            '<td style="text-align:left;">' +
                            '<a href="javascript:;" class="btn btn-info btn-sm item_edit" onclick="delete_user("' + data[i].username + '")">Edit</a>' + ' ' +
                            '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_hapus" value="' + data[i].username + '">Delete</a>' +
                            '</td>' +
                            '</tr>';

                    }
                    $('#show_data').html(html);
                }

            });
        }

        socket.on('fromServer', data => {
            console.log(data);
            tampil_data();
        });

    });
</script>