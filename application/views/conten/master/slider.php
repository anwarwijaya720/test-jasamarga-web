<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Master Slider</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Slider</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- <button class="btn btn-success" onclick="addEstudiante()"><i class="glyphicon glyphicon-plus"></i>Add Slider</button> -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_form" >Add Slider</button>
                            <br /><br />
                            <table id="mydata" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Gambar</th>
                                        <th>Keterangan</th>
                                        <th>Created Date</th>
                                        <th>Created By</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="show_data">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Gambar</th>
                                        <th>Keterangan</th>
                                        <th>Created Date</th>
                                        <th>Created By</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Bootstrap modal -->
            <div class="modal fade" id="modal_form" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Add Slider</h3>
                            <button type="button" style="background-color: grey;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body form">
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('master/UpdateSlider/save');?>">
                                <div class="form-body">
                                    <div class="from-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="imgWrap">
                                                        <img id="imgView" class="card-img-top img-fluid">
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="custom-file">
                                                            <input type="file" name="berkas" id="inputFile" class="imgFile custom-file-input" aria-describedby="inputGroupFileAddon01" required>
                                                            <label class="custom-file-label" for="inputFile">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Deskripsi</label>
                                    <div class="col-md-12">
                                        <input name="des" placeholder="deskripsi" class="form-control" type="text" required>
                                        <span class="help-block" style="color: red;"></span>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<?php require('slider_action.php') ?>