<div class="content-header">
    <div class="container-fluid">

        <div class="row" style="margin: 10px;">
            <div class="row mb-8">
                <div style="margin-left: 10px;">
                    <img src="<?php echo base_url("assets/images/logo/LogoMatneo.png"); ?>" style="height: 50px;">
                </div>
                <div style="margin-left: 10px;">
                    <h1 class="m-0 text-dark"><strong>Laporan Rujukan</strong></h1>
                    <strong><a>MATNEO SAFE</a></strong>
                </div>
            </div>
            <div style="position: absolute; right: 20px;">
                <strong>
                    <p id="jam_gadang" class="text-red text-uppercase"></p>
                </strong>
                <div class="text-right" style="margin-top: -10px;">
                    <a href="#" class="btn btn-sm btn-danger">
                        <i class="far fa-hand-pointer"> </i>
                        Klik 3 Kali Dapat Rumah Sakit
                    </a>
                    <a href="<?php echo base_url("Dashboard");  ?>" class="btn btn-sm btn-secondary">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Main content -->
<section class="content" style="margin-top: 20px;">
    <div class="container-fluid">

        <div class="row" style="margin-left: 30px;">
            <form role="form" id="reportLR" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url('master/LaporanRujukan/export');?>"> 
                <div class="row">
                    <button type="submit" class="btn btn-success" style="height: 40px;">Export To Excel</button>
                    <div class="col-sm-2">
                        <div class="form-group" style="width: 170px;">
                            <select class="form-control" name="selectTime" required>
                                <option value="">------ Pilih ------</option>
                                <option name="tr" value="hari">Hari Ini</option>
                                <option name="ks" value="minggu">Minggu Ini</option>
                                <option name="ph" value="bulan">Bulan Ini</option>
                                <option name="ph" value="tahun">Tahun Ini</option>
                            </select>
                        </div>
                    </div>
                </div>
            </from>
        </div>
        <br />

        <div class="row">

            <div class="col-md-2">
                <div class="card" id="cardBox">
                    <div class="card-header" id="styleFontNavTitle">
                        <strong><a>Penerimaan Rujukan</a></strong>
                        <br />
                        <strong><a>Ibu Terbanyak</a></strong>
                    </div>
                    <div class="card-body">
                        <?php
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            $no = 1;
                            if(!empty($penerimaRujukan)){
                                $total = count($penerimaRujukan);
                                if ($total <= 10) {
                                    
                                    foreach ($penerimaRujukan as $data) {   
                                        
                                        $imgRs = null;
                                        if(!empty($data->img_rs)){
                                            $imgRs = $data->img_rs;
                                        }else{
                                            $imgRs = base_url(img_default);
                                        }

                                        echo ('
                                                <div class="card" style="padding-left: 5px;padding-bottom: 5px; background-color: #4FBBF5;">
                                                    <strong><a id="styleFontCradTitle">'.$no.'. '. $data->rs_name .'</a></strong>
                                                    <div>                                    
                                                        <img src=' . $imgRs . ' id="styleImg">  
                                                        <strong><a id="styleFontCradTotal">'.$data->total.' Ibu</a></strong>
                                                    </div>
                                                </div>
                                            ');
                                        $no++;
                                    }

                                }else{

                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');                                
                                    $counter = 1;
                                    foreach ($penerimaRujukan as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');

                                }
                            }else{
                                echo ('
                                        <div style="padding: 5px; text-align: center;">
                                            <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="cardBox">
                    <div class="card-header" id="styleFontNavTitle">
                        <strong><a>Penerimaan Rujukan</a></strong>
                        <br />
                        <strong><a>Neonatal Terbanyak</a></strong>
                    </div>
                    <div class="card-body">
                        <?php
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            $no = 1;
                            if(!empty($perujuk)){
                                $total = count($perujuk);
                                if ($total <= 10) {
                                    
                                    foreach ($perujuk as $data) {
                                        
                                        $imgRs = null;
                                        if(!empty($data->img_rs)){
                                            $imgRs = $data->img_rs;
                                        }else{
                                            $imgRs = base_url(img_default);
                                        }

                                        echo ('
                                                <div class="card" style="padding-left: 5px;padding-bottom: 5px; background-color: #78E56E;">
                                                    <strong><a id="styleFontCradTitle">'. $data->rs_name .'</a></strong>
                                                    <div>                                    
                                                        <img src=' . $imgRs . ' id="styleImg">  
                                                        <strong><a id="styleFontCradTotal">'.$data->total.' Neo</a></strong>
                                                    </div>
                                                </div>
                                            ');
                                        $no++;
                                    }
                                    
                                }else{

                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');                                
                                    $counter = 1;
                                    foreach ($perujuk as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                    
                                }

                            }else{
                                echo ('
                                        <div style="padding: 5px; text-align: center;">
                                            <img src=' . $img_data_null . ' id="styleIconClose">                                                                       
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="cardBox">
                    <div class="card-header" id="styleFontNavTitle">
                        <strong><a>Nakes Perujuk</a></strong>
                        <br />
                        <strong><a>Ibu Terbanyak</a></strong>
                    </div>
                    <div class="card-body">
                        <?php
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            $no = 1;
                            if(!empty($nakses_perujuk)){                                
                                $total = count($nakses_perujuk);
                                if ($total <= 10) {

                                    foreach ($nakses_perujuk as $data) {  
                                        
                                        $imgRs = null;
                                        if(!empty($data->img_rs)){
                                            $imgRs = $data->img_rs;
                                        }else{
                                            $imgRs = base_url(img_default);
                                        }
    
                                        echo ('
                                                <div class="card" style="padding-left: 5px;padding-bottom: 5px; background-color: #F7CD61;">
                                                    <strong><a id="styleFontCradTitle">'.$no.'. '. $data->rs_name .'</a></strong>
                                                    <div>                                    
                                                        <img src=' . $imgRs . ' id="styleImg">  
                                                        <strong><a id="styleFontCradTotal">'.$data->total.' Ibu</a></strong>
                                                    </div>
                                                </div>
                                            ');
                                        $no++;
                                    }

                                }else{

                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');                                
                                    $counter = 1;
                                    foreach ($nakses_perujuk as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');

                                }
                            }else{
                                echo ('
                                        <div style="padding: 5px; text-align: center;">
                                            <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="cardBox">
                    <div class="card-header" id="styleFontNavTitle">
                        <strong><a>Nakes Perujuk</a></strong>
                        <br />
                        <strong><a>Total Terbanyak</a></strong>
                    </div>
                    <div class="card-body">
                        <?php
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            $no = 1;
                            if(!empty($nakses_total)){
                                $total = count($nakses_total);
                                if ($total <= 10) {
                                    foreach ($nakses_total as $data) {
                                        
                                        $imgRs = null;
                                        if(!empty($data->img_rs)){
                                            $imgRs = $data->img_rs;
                                        }else{
                                            $imgRs = base_url(img_default);
                                        }

                                        echo ('
                                                <div class="card" style="padding-left: 5px;padding-bottom: 5px; background-color: #F58A81;">
                                                    <strong><a id="styleFontCradTitle">'.$no.'. '. $data->rs_name .'</a></strong>
                                                    <div>                                    
                                                        <img src=' . $imgRs . ' id="styleImg">  
                                                        <strong><a id="styleFontCradTotal">'.$data->total.' Ibu</a></strong>
                                                    </div>
                                                </div>
                                            ');
                                        $no++;
                                    }
                                }else{
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');                                
                                    $counter = 1;
                                    foreach ($nakses_total as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #F58A81;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #F58A81;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }
                            }else{
                                echo ('
                                        <div style="padding: 5px; text-align: center;">
                                            <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header" id="styleFontNavTitle">
                        <strong><a>Grafik</a></strong>
                        <br />
                        <strong><a>Kebutuhan Terbanyak</a></strong>
                    </div>
                    <div class="card-body">

                        <div class="chart">
                            <canvas id="barChart" style="min-height: 250px; height: 600px; max-width: 100%;"></canvas>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

<style type="text/css">
    #styleImg {
        width: 50px; 
        height: 30px; 
        border-radius: 10%;
    }
    #styleFontNavTitle{
        font-size: 12px; text-align: center;
    }
    #styleFontCradTitle{
        font-size: 10px;
    }
    #styleFontCradTotal{
        right: 5px; position: absolute; font-size: 14px;
    }
    #styleIconClose {
        width: 130px;
        height: 125px;
        margin-left: -10px;
    }
    #cardBox{
        min-height: 240px;
    }
</style>


<script>

    $(document).ready(function() {

        // document.querySelector('#audioplayer source').setAttribute('src', 'assets/images/mp3/audio.mpeg?rand=' + Math.random());                

        // REALTIME
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        var socket = io(socketIoAddress);

        socket.on('fromServer', data => {
            console.log(data);
            location.reload();
        });        

    });

    $(function() {

        var areaChartData = {
            labels: ['Sp Oby + SP An', 'Sp Obgyn', 'Sp Anak', 'Sp Jantung', 'SP PD', 'Sp Paru', 'R Perawatan', 'NICU', 'R Intensif', 'HCU', 'Isolasi IGD Ponek', 'Covid'],
            datasets: [{
                backgroundColor: 'rgba(244, 144, 128, 0.8)',
                borderColor: 'rgba(244, 144, 128, 0.8)',                
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [
                    <?php echo $obgyn_anak[0]->total ?>, 
                    <?php echo $obgyn[0]->total ?>, 
                    <?php echo $anak[0]->total ?>, 
                    <?php echo $jantung[0]->total ?>, 
                    <?php echo $pd[0]->total ?>, 
                    <?php echo $paru[0]->total ?>, 
                    <?php echo $perawatan[0]->total ?>, 
                    <?php echo $nicu[0]->total ?>, 
                    <?php echo $intensif[0]->total ?>,
                    <?php echo $hcu[0]->total ?>,
                    <?php echo $ponek[0]->total ?>,
                    <?php echo $covid[0]->total ?>                    
                ]
            }]
        }

        var opt = {
            events: false,
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 0
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                    }
                }
            },
            animation: {
                duration: 1,
                onComplete: function() {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function(dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function(bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x + 10, bar._model.y + 5);
                        });
                    });
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 30,
                    top: 0,
                    bottom: 0
                }
            }
        };

        var ctx = $('#barChart').get(0).getContext('2d');
        var myBarChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: areaChartData,
            options: opt
        });

    });
</script>