<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Master RS</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">   
            
            <a class="btn btn-primary" href="<?php echo base_url('master/UpdateRs/add'); ?>" role="button">Add RS</a><br/><br/>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Rumah Sakit</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="mydata" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Instansi</th>
                                        <th>Gambar</th>
                                        <th>Type</th>
                                        <th>Maternal</th>
                                        <th>Telpon</th>
                                        <th>Log</th>
                                        <th>Lat</th>
                                        <th>Alamat</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="show_data">
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Instansi</th>
                                        <th>Gambar</th>
                                        <th>Type</th>
                                        <th>Maternal</th>
                                        <th>Telpon</th>
                                        <th>Log</th>
                                        <th>Lat</th>
                                        <th>Alamat</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<?php require('rs_action.php')?>

