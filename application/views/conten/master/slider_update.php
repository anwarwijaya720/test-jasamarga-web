<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Slider</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Slider</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <?php foreach ($getSlider as $data) { ?>

                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('master/UpdateSlider/editAction/'.$data->slider_id);?>">

                                    <div class="box-body">

                                        <div class="form-body">
                                            <div class="from-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="imgWrap">                                                                
                                                                <img id="imgView" 
                                                                    src="<?php if(!empty($data->image)){
                                                                        echo $data->image;
                                                                    }?>"
                                                                class="card-img-top img-fluid">
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="custom-file">
                                                                    <input type="file" name="berkas" id="inputFile" class="imgFile custom-file-input" aria-describedby="inputGroupFileAddon01">
                                                                    <label class="custom-file-label" for="inputFile">Choose file</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Deskripsi</label>
                                            <div class="col-md-12">
                                                <input name="des" placeholder="deskripsi" class="form-control" type="text" value="<?php echo $data->deskripsi ?>" required>
                                                <span class="help-block" style="color: red;"></span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
                                    </div>

                                </form>

                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>


<style>
    #imgView {
        padding: 5px;
    }

    .loadAnimate {
        animation: setAnimate ease 2.5s infinite;
    }

    @keyframes setAnimate {
        0% {
            color: #000;
        }

        50% {
            color: transparent;
        }

        99% {
            color: transparent;
        }

        100% {
            color: #000;
        }
    }

    .custom-file-label {
        cursor: pointer;
    }
</style>

<script>
    $("#inputFile").change(function(event) {
        fadeInAdd();
        getURL(this);
    });

    $("#inputFile").on('click', function(event) {
        fadeInAdd();
    });

    function getURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var filename = $("#inputFile").val();
            filename = filename.substring(filename.lastIndexOf('\\') + 1);
            reader.onload = function(e) {
                debugger;
                $('#imgView').attr('src', e.target.result);
                $('#imgView').hide();
                $('#imgView').fadeIn(500);
                $('.custom-file-label').text(filename);
            }
            reader.readAsDataURL(input.files[0]);
        }
        $(".alert").removeClass("loadAnimate").hide();
    }

    function fadeInAdd() {
        fadeInAlert();
    }

    function fadeInAlert(text) {
        $(".alert").text(text).addClass("loadAnimate");
    }
</script>