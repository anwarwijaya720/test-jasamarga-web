<script>
    $(document).ready(function() {
        
        $(".form-control").on({
            keydown: function(e) {
                if (e.which === 32)
                    return false;
            },
            keyup: function() {
                this.value = this.value.toLowerCase();
            },
            change: function() {
                this.value = this.value.replace(/\s/g, "");

            }
        });

        $.validator.setDefaults({
            submitHandler: function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Update Profile Data",
                    icon: 'warning',
                    showCancelButton: true,
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: "Profile/Save",
                            type: "POST",
                            data: $('#profile').serialize(),
                            dataType: "JSON",
                            success: function(data) {
                                console.log(data);
                                if (data['status'] == true) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Save',
                                        text: 'Save data success!',
                                    });
                                    window.location.href = '<?php echo base_url('master/Profile') ?>';
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong!',
                                    });
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Error Save Data',
                                });
                            }
                        });
                    }
                });

            }
        });
        
        $("#profile").validate({
            rules: {
                user: {
                    required: true,
                    minlength: 5,
                },
                noTlp: {
                    required: true,
                    minlength: 10,
                },
            },
            messages: {
                user: {
                    required: "Date cannot be empty",
                    length: "Your username must be at least 5 characters long"
                },
                noTlp: {
                    required: "Date cannot be empty",
                    length: "Your password must be at least 10 characters long"
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

    });
</script>