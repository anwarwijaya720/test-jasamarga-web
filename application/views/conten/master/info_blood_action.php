<script>
    $(document).ready(function() {
        
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        const socket = io(socketIoAddress);

        $.ajax({
            url: "InfoBlood/example",
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error deleting data',
                });
            }
        });

    });
</script>