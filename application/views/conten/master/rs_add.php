<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Add Rs</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lengkapi Data Rumah Sakit</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('master/UpdateRs/addAction');?>">

                                    <div class="box-body">

                                        <div class="form-group">
                                            <label for="rsName">Instansi *</label>
                                            <input type="text" class="form-control" name="rsName" placeholder="Instansi" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="tipe">Tipe *</label>
                                            <input type="text" class="form-control" name="tipe" placeholder="tipe" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="tlp">No Telpon *</label>
                                            <input type="number" class="form-control" name="tlp" placeholder="no telpon" minlength="7" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="maternal">Level Maternal *</label>
                                            <input class="form-control" name="maternal" rows="3" placeholder="level maternal" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="long">Longitude *</label>
                                            <input class="form-control" name="long" rows="3" placeholder="longitude" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="lat">Latitude *</label>
                                            <input class="form-control" name="lat" rows="3" placeholder="latitude" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="alamat">Alamat *</label>
                                            <textarea class="form-control" name="alamat" rows="3" placeholder="alamat" required></textarea>
                                        </div>

                                        <div class="from-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="card">
                                                        <div class="imgWrap">
                                                            <img id="imgView" class="card-img-top img-fluid">
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="custom-file">
                                                                <input type="text" class="form-control" name="imgBoxRs">
                                                                <input type="file" name="berkas" id="inputFile" class="imgFile custom-file-input" aria-describedby="inputGroupFileAddon01" required>
                                                                <label class="custom-file-label" for="inputFile">Choose file</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!-- /.box-body -->
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
                                    </div>

                                </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>


<style>
    #imgView {
        padding: 5px;
    }

    .loadAnimate {
        animation: setAnimate ease 2.5s infinite;
    }

    @keyframes setAnimate {
        0% {
            color: #000;
        }

        50% {
            color: transparent;
        }

        99% {
            color: transparent;
        }

        100% {
            color: #000;
        }
    }

    .custom-file-label {
        cursor: pointer;
    }
</style>

<script>
    $("#inputFile").change(function(event) {
        fadeInAdd();
        getURL(this);
    });

    $("#inputFile").on('click', function(event) {
        fadeInAdd();
    });

    function getURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var filename = $("#inputFile").val();
            filename = filename.substring(filename.lastIndexOf('\\') + 1);
            reader.onload = function(e) {
                debugger;
                $('#imgView').attr('src', e.target.result);
                $('#imgView').hide();
                $('#imgView').fadeIn(500);
                $('.custom-file-label').text(filename);
            }
            reader.readAsDataURL(input.files[0]);
        }
        $(".alert").removeClass("loadAnimate").hide();
    }

    function fadeInAdd() {
        fadeInAlert();
    }

    function fadeInAlert(text) {
        $(".alert").text(text).addClass("loadAnimate");
    }
</script>