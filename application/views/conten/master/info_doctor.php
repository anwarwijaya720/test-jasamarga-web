
<div class="content-header">
    <div class="container-fluid">

        <div class="row" style="margin: 10px;">
            <div class="row mb-8">
                <div style="margin-left: 10px;">                        
                    <img src="<?php echo base_url("assets/images/logo/LogoMatneo.png"); ?>" style="height: 40px;">
                </div>
                <div style="margin-left: 10px;">
                    <h4 class="m-0 text-dark"><strong>Info Ketersediaan Layanan Dokter Spesialis</strong></h4>
                    <strong><a>MatneoSafe</a></strong>
                </div>
            </div>
            <div style="position: absolute; right: 20px;">
                <strong>
                    <p id="jam_gadang" class="text-red text-uppercase"></p>
                </strong>
                <div class="text-right" style="margin-top: -10px;">
                    <a href="#" class="btn btn-sm btn-danger">
                        <i class="far fa-hand-pointer"> </i>
                        Klik 3 Kali Dapat Rumah Sakit
                    </a>                    
                    <a href="<?php echo base_url("Dashboard");  ?>" class="btn btn-sm btn-secondary">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Main content -->
<section class="content" style="margin-top: 20px;">
    <div class="container-fluid">

        <div class="row">

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php                                                                
                            $y = $obgynAnak[0]->total_obgyn_anak;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if($y != 0){
                                echo('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>Sp.Obgyn + Sp.An</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : '.$y.' Rs</a></strong>
                                    </div>
                                ');
                            }else{
                                echo('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src='. $imgLonceng .' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Sp.Obgyn + Sp.An</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : '.$y.' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                            }
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y=$obgynAnak[0]->total_obgyn_anak;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if($y != 0){    
                                if($y <= 10){
                                    if(!empty($getRsObgynAnak)){
                                        foreach($getRsObgynAnak as $data){

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                             }else{
                                                $imgRs = base_url(img_default);
                                             }

                                            echo ('                                            
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color: #4FBBF5;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">'.$data->rs_name.'</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox"> 
                                                        </td>                                                    
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type '.$data->rs_type.'</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal : '.$data->level_maternal.'</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div> 
                                            ');
                                        }
                                    }
                                }else{
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getRsObgynAnak as $data) {  
                                        if($counter % 2 == 0){
                                            echo('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #F58A81;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }else{
                                            echo('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #F58A81;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }                                
                            }else{                                                                    
                                echo('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                          
                                    </div>
                                ');
                            }                         
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php                                                                
                            $y = $obgyn[0]->total_obgyn;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if($y != 0){
                                echo('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>Sp.Obgyn</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : '.$y.' Rs</a></strong>
                                    </div>
                                ');
                            }else{
                                echo('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src='. $imgLonceng .' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Sp.Obgyn</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : '.$y.' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                            }   
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y=$obgyn[0]->total_obgyn;                   
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if($y != 0){   
                                if($y <= 10){
                                    if(!empty($getRsObgyn)){
                                        foreach($getRsObgyn as $data){
                                            
                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                             }else{
                                                $imgRs = base_url(img_default);
                                             }

                                            echo ('                                            
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color: #78E56E;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">'.$data->rs_name.'</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox"> 
                                                        </td>                                                    
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type '.$data->rs_type.'</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal : '.$data->level_maternal.'</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div> 
                                            ');
                                        }
                                    }
                                }else{
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getRsObgyn as $data) {  
                                        if($counter % 2 == 0){
                                            echo('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #78E56E;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }else{
                                            echo('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #78E56E;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }                                
                            }else{                                                                    
                                echo('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                          
                                    </div>
                                ');
                            }
                        ?>                            
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php                                                                
                            $y = $anak[0]->total_anak;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if($y != 0){
                                echo('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>Sp.Anak</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : '.$y.' Rs</a></strong>
                                    </div>
                                ');
                            }else{
                                echo('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src='. $imgLonceng .' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Sp.Anak</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : '.$y.' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                            }   
                        ?>

                    </div>
                    <div class="card-body">
                        <?php
                            $y=$anak[0]->total_anak;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if($y != 0){   
                                if($y <= 10){
                                    if(!empty($getRsAnak)){
                                        foreach($getRsAnak as $data){

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                             }else{
                                                $imgRs = base_url(img_default);
                                             }

                                            echo ('                                            
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color: #F7CD61;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">'.$data->rs_name.'</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox"> 
                                                        </td>                                                    
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type '.$data->rs_type.'</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal : '.$data->level_maternal.'</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div> 
                                            ');
                                        }
                                    }
                                }else{
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getRsAnak as $data) {  
                                        if($counter % 2 == 0){
                                            echo('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #F7CD61;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }else{
                                            echo('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #F7CD61;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');                                    
                                }                                    
                            }else{                                                 
                                echo('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                          
                                    </div>
                                ');
                            }
                        ?>                            
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php                                                
                            $y = $jantung[0]->total_jantung;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if($y != 0){
                                echo('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>Sp.Jantung</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : '.$y.' Rs</a></strong>
                                    </div>
                                ');
                            }else{
                                echo('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src='. $imgLonceng .' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Sp.Jantung</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : '.$y.' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                            }   
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y= $jantung[0]->total_jantung;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if($y != 0){
                                if($y <= 10){
                                    if(!empty($getRsJantung)){
                                        foreach($getRsJantung as $data){

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                             }else{
                                                $imgRs = base_url(img_default);
                                             }

                                            echo ('                                            
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color: #F58A81;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">'.$data->rs_name.'</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox"> 
                                                        </td>                                                    
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type '.$data->rs_type.'</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal : '.$data->level_maternal.'</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div> 
                                            ');
                                        }
                                    }
                                }else{                                            
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getRsJantung as $data) {   
                                        if($counter % 2 == 0){
                                            echo('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #F58A81;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }else{
                                            echo('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #F58A81;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>'); 
                                }                                    
                            }else{                                                                    
                                echo('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                          
                                    </div>
                                ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php                                                                
                            $y = $dalam[0]->total_dalam;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if($y != 0){
                                echo('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>Sp.Penyakit Dalam</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : '.$y.' Rs</a></strong>
                                    </div>
                                ');
                            }else{
                                echo('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src='. $imgLonceng .' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Sp.Penyakit Dalam</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : '.$y.' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                            }   
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y=$dalam[0]->total_dalam;                  
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if($y != 0){  
                                if($y <= 10){
                                    if(!empty($getRsDalam)){
                                        foreach($getRsDalam as $data){

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                             }else{
                                                $imgRs = base_url(img_default);
                                             }

                                            echo ('                                            
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color: #B696FC;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">'.$data->rs_name.'</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox"> 
                                                        </td>                                                    
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type '.$data->rs_type.'</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal : '.$data->level_maternal.'</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div> 
                                            ');
                                        }
                                    }
                                }else{                                            
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getRsDalam as $data) {  
                                        if($counter % 2 == 0){
                                            echo('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #B696FC;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }else{
                                            echo('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #B696FC;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>'); 
                                }                                    
                            }else{                                                                    
                                echo('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                          
                                    </div>
                                ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php                                                                
                            $y = $paru[0]->total_paru;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if($y != 0){
                                echo('
                                    <div class="wrapper">                                
                                        <strong><a>Sp.Paru</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : '.$y.' Rs</a></strong>
                                    </div>
                                ');
                            }else{
                                echo('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src='. $imgLonceng .' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Sp.Paru</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : '.$y.' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                            }   
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y= $paru[0]->total_paru;                    
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if($y != 0){
                                if($y <= 10){
                                    if(!empty($getRsParu)){
                                        foreach($getRsParu as $data){

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                             }else{
                                                $imgRs = base_url(img_default);
                                             }

                                            echo ('                                            
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color: #FC73DC;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">'.$data->rs_name.'</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox"> 
                                                        </td>                                                    
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type '.$data->rs_type.'</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal : '.$data->level_maternal.'</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div> 
                                            ');
                                        }
                                    }
                                }else{    
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getRsParu as $data) {    
                                        if($counter % 2 == 0){
                                            echo('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #FC73DC;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }else{
                                            echo('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #FC73DC;">
                                                    <strong><a>'.$data->rs_name.'</a></strong>
                                                </div>
                                            ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }                                    
                            }else{                                                                    
                                echo('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                          
                                    </div>
                                ');
                            }
                        ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

<style type="text/css">
    #boxList {
        font-size: 9px;
        border-radius: 20px;
        height: 30px;
        padding-top: 5px;
    }
    #boxListKiri {
        font-size: 9px;
        border-radius: 20px;
        padding-top: 5px;
        padding-bottom: 5px;
        margin-bottom: 5px;
    }
    #boxListKanan {
        font-size: 9px;
        border-radius: 20px;
        padding-top: 5px;
        padding-bottom: 5px;
        margin-left: -4px;
        margin-right: 4px;
    }
    #infoStyleTitle{
        text-align: center;
        font-size: 10px;
    }    
    #styleCardMinHeight{min-height: 240px;}    
    #styleIconLonceng{
        width: 20px; height: 20px; margin-left: -10px;
    }
    #styleImgCardBox{
        width: 35px; height: 30px; border-radius: 10%;
    }
    #styleIconClose{
        width: 130px; height: 125px; margin-left: -10px;
    }
    #infoFontRs{font-size: 10px;}
    #infoFontCardBox{font-size: 9px;}
    #infoStyleTable{margin-top: -5px;}
    #coverimg{width: 20%;}
</style>

<script>
    $(document).ready(function() {

        document.querySelector('#audioplayer source').setAttribute('src', 'assets/images/mp3/audio.mpeg?rand=' + Math.random());
        
        // REALTIME
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        var socket = io(socketIoAddress);

        socket.on('fromServer', data => {
            // console.log(data);            
            location.reload();
        });
        
    });
</script>