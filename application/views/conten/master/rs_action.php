<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">  
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://unpkg.com/jquery-easy-loading/dist/jquery.loading.min.js"></script> 

<!-- <script>
    $('#loading-body-btn').click(function() {
        $('body').loading({
        stoppable: true
        });
    });
</script> -->

<script>
    $(document).ready(function() {

        // tampil_data_barang();            
        // $('#mydata').dataTable({
        //     "processing": true,   
        //     "responsive": true,
        //     "autoWidth": false,
        // });
        // //fungsi tampil barang
        // function tampil_data_barang() {

        //     // setInterval(function() {
        //     // // $('#mydata').loading('toggle');
        //     // }, 2000);

        //     $.ajax({
        //         type: 'ajax',
        //         url: "<?php echo base_url('master/UpdateRs/getRs') ?>",
        //         async: false,
        //         dataType: 'json',
        //         success: function(data) {
        //             var html = '';
        //             var i;
        //             for (i = 0; i < data.length; i++) {
        //                 var no = i + 1;
        //                 html += '<tr>' +
        //                     '<td>' + no + '</td>' +
        //                     '<td id="num">' + data[i].rs_name + '</td>' +
        //                     '<td>' +
        //                         '<img src="' + data[i].img_rs + ' "style="height:50px;width:50px;" />' +
        //                     '</td>' +
        //                     '<td>' + data[i].rs_type + '</td>' +
        //                     '<td>' + data[i].level_maternal + '</td>' +
        //                     '<td>' + data[i].no_hp_rs + '</td>' +
        //                     '<td>' + data[i].long + '</td>' +
        //                     '<td>' + data[i].lat + '</td>' +
        //                     '<td>' + data[i].address + '</td>' +
        //                     '<td style="text-align:left;">' +
        //                     '<a href="<?php echo base_url('master/UpdateRs/edit/') ?>' + data[i].rs_id + '" class="btn btn-info btn-sm item_edit">Edit</a>' +
        //                     '</td>' +
        //                     '</tr>';
        //             }
        //             $('#show_data').html(html);
        //         }

        //     });
        // }

        //datatables
        $('#mydata').DataTable({
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "order": [],

            "ajax": {
                "url": "<?php echo site_url('master/UpdateRs/viewRs') ?>",
                "type": "POST"
            },


            "columnDefs": [{
                // "targets": [0],
                // "orderable": false,                
                "targets": [0], //last column
                "orderable": [0, 1], //set not orderable
            }, ],
        });

    });
</script>