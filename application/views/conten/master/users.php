<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User Management</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Users</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <button class="btn btn-success" onclick="addEstudiante()"><i class="glyphicon glyphicon-plus"></i>Add Users</button>
                            <button class="btn btn-default" onclick="reloadTable()"><i class="glyphicon glyphicon-refresh"></i>Reload</button>
                            <br />
                            <br />
                            <table id="mytable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>No.Hp</th>
                                        <th>Instantsi</th>
                                        <th>Jenis Pengguna</th>
                                        <th>Profesi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="show_data">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>No.Hp</th>
                                        <th>Instantsi</th>
                                        <th>Jenis Pengguna</th>
                                        <th>Profesi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Bootstrap modal -->
            <div class="modal fade" id="modal_form" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Form Add User</h3>
                            <button type="button" style="background-color: grey;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body form">
                            <form action="#" id="form" class="form-horizontal">
                                <input class="col-md-12" type="hidden" name="usersId" />
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nama</label>
                                        <div class="col-md-12">
                                            <input name="username" placeholder="Username" class="form-control" type="text">
                                            <span class="help-block" style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">No Hp</label>
                                        <div class="col-md-12">
                                            <input name="noHp" placeholder="No Handphone" class="form-control" type="number">
                                            <span class="help-block" style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-10">Instansi</label>
                                        <div class="col-md-12">
                                            <input name="instansi" placeholder="instansi" class="form-control" type="text">
                                            <span class="help-block" style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group" id="mRs">
                                        <label class="control-label col-md-10">Asal Faskes</label>
                                        <div class="col-md-12">
                                            <select name="rs" class="form-control select2bs4" name="rs" style="width: 100%;">
                                                <option selected="selected" value="">---- Pilih ----</option>
                                                <?php                                              
                                                    foreach ($getRs as $rs) {
                                                        echo "<option value='" . $rs->rs_id . "'>" . $rs->rs_name . "</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Profesi</label>
                                        <div class="col-md-12">
                                            <input name="position" placeholder="position" class="form-control" type="text">
                                            <span class="help-block" style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group" id="pass">
                                        <label class="control-label col-md-3">Password</label>
                                        <div class="col-md-12">
                                            <input name="password" placeholder="password" class="form-control" type="password">
                                            <span class="help-block" style="color: red;"></span>
                                        </div>
                                    </div>
                                    <div class="form-group" id="conPass">
                                        <label class="control-label col-md-12">Confirm Password</label>
                                        <div class="col-md-12">
                                            <input name="confirm_password" placeholder=" confirm password" class="form-control" type="password">
                                            <span class="help-block" style="color: red;"></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div>
    </section>
    <!-- /.content -->
</div>

<?php require('users_action.php') ?>