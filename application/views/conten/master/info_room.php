<div class="content-header">
    <div class="container-fluid">

        <div class="row" style="margin: 10px;">
            <div class="row mb-6">
                <div style="margin-left: 10px;">
                    <img src="<?php echo base_url("assets/images/logo/LogoMatneo.png"); ?>" style="height: 40px;">
                </div>
                <div style="margin-left: 10px;">
                    <h4 class="m-0 text-dark"><strong>Info Ketersediaan Ruangan</strong></h4>
                    <strong><a>MatneoSafe</a></strong>
                </div>
            </div>
            <div style="position: absolute; right: 20px;">
                <strong>
                    <p id="jam_gadang" class="text-red text-uppercase"></p>
                </strong>
                <div class="text-right" style="margin-top: -10px;">
                    <a href="#" class="btn btn-sm btn-danger">
                        <i class="far fa-hand-pointer"> </i>
                        Klik 3 Kali Dapat Rumah Sakit
                    </a>
                    <a href="<?php echo base_url("Dashboard");  ?>" class="btn btn-sm btn-secondary">
                        Kembali
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Main content -->
<section class="content" style="margin-top: 20px;">
    <div class="container-fluid">

        <div class="row">            

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php
                        $y = $nicu[0]->stock_nicu;
                        $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                        $audio = base_url('assets/images/mp3/audio.mpeg');
                        if ($y != 0) {
                            echo ('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>NICU</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></strong>
                                    </div>
                                ');
                        } else {
                            echo ('                            
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src=' . $imgLonceng . ' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>NICU</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                        }
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                        $y = $nicu[0]->stock_nicu;
                        $img_data_null = base_url("assets/images/icon/clos_icon.png");
                        if ($y != 0) {
                            if ($y <= 10) {
                                if(!empty($getNicu)){
                                    foreach ($getNicu as $data) {

                                        $imgRs = null;
                                        if(!empty($data->img_rs)){
                                            $imgRs = $data->img_rs;
                                        }else{
                                            $imgRs = base_url(img_default);
                                        }

                                        echo ('
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color:#78E56E;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">' . $data->rs_name . '</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox">  
                                                        </td>                                                                                                
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type ' . $data->rs_type . '</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal ' . $data->level_maternal . '</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div>
                                            ');
                                    }
                                }
                            } else {
                                echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                $counter = 1;
                                foreach ($getNicu as $data) {
                                    if ($counter % 2 == 0) {
                                        echo ('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #78E56E;">
                                                    <strong><a>' . $data->rs_name . '</a></strong>
                                                </div>
                                            ');
                                    } else {
                                        echo ('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #78E56E;">
                                                    <strong><a>' . $data->rs_name . '</a></strong>
                                                </div>
                                            ');
                                    }
                                    $counter++;
                                }
                                echo ('</div>');
                            }
                        } else {
                            echo ('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                    </div>
                                ');
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php
                            $y = $icu[0]->stock_icu;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if ($y != 0) {
                                echo ('
                                        <div class="wrapper" style="text-align: center;">                                
                                            <strong><a>ICU</a></strong>
                                            <br />
                                            <strong><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></strong>
                                        </div>
                                    ');
                            } else {
                                echo ('
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src=' . $imgLonceng . ' id="styleIconLonceng">
                                                <audio id="audioplayer" autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>
                                            </div>
                                            <div class="col-md-10">
                                                <strong><div class="col-sm-12"><a>ICU</a></div></strong>
                                                <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></div></strong>
                                            </div>
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y = $icu[0]->stock_icu;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if ($y != 0) {
                                if ($y <= 10) {
                                    if(!empty($getIcu)){                                
                                        foreach ($getIcu as $data) {

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                            }else{
                                                $imgRs = base_url(img_default);
                                            }

                                            echo ('
                                                    <div class="card" style="padding-left:3px; padding-bottom:3px; background-color:#F7CD61;">
                                                        <table id="infoStyleTable">
                                                            <tr>
                                                                <th colspan="3"><strong><a id="infoFontRs">' . $data->rs_name . '</a></strong> </th>
                                                            </tr>
                                                            <td id="coverimg">
                                                                <img src=' . $imgRs . ' id="styleImgCardBox">  
                                                            </td>                                                                                                
                                                            <td style="font-size:8px;">
                                                                <strong><a id="infoFontCardBox">Type ' . $data->rs_type . '</a></strong>
                                                                <br/>
                                                                <strong><a id="infoFontCardBox">Level Maternal ' . $data->level_maternal . '</a></strong>
                                                            </td>                                                    
                                                        </table>
                                                    </div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                        $counter = 1;
                                        foreach ($getIcu as $data) {
                                            if ($counter % 2 == 0) {
                                                echo ('
                                                        <div class="col-md-6" id="boxListKiri" style="background-color: #F7CD61;">
                                                            <strong><a>' . $data->rs_name . '</a></strong>
                                                        </div>
                                                    ');
                                            } else {
                                                echo ('
                                                        <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #F7CD61;">
                                                            <strong><a>' . $data->rs_name . '</a></strong>
                                                        </div>
                                                    ');
                                            }
                                            $counter++;                                    
                                        }
                                    echo ('</div>');
                                }
                            } else {
                                echo ('
                                            <div style="padding: 5px; text-align: center;">
                                                <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                            </div>
                                        ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">

                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php
                            $y = $ward[0]->stock_ward;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if ($y != 0) {
                                echo ('
                                        <div class="wrapper" style="text-align: center;">                                
                                            <strong><a>Ruangan Perawatan</a></strong>
                                            <br />
                                            <strong><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></strong>
                                        </div>
                                    ');
                            } else {
                                echo ('                            
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src=' . $imgLonceng . ' id="styleIconLonceng">
                                                <audio id="audioplayer" autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>
                                            </div>
                                            <div class="col-md-10">
                                                <strong><div class="col-sm-12"><a>Ruangan Perawatan</a></div></strong>
                                                <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></div></strong>
                                            </div>
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y = $ward[0]->stock_ward;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if ($y != 0) {
                                if ($y <= 10) {
                                    if(!empty($getWard)){  
                                        foreach ($getWard as $data) {

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                            }else{
                                                $imgRs = base_url(img_default);
                                            }

                                            echo ('
                                                    <div class="card" style="padding-left:3px; padding-bottom:3px; background-color:#4FBBF5;">
                                                        <table id="infoStyleTable">
                                                            <tr>
                                                                <th colspan="3"><strong><a id="infoFontRs">' . $data->rs_name . '</a></strong> </th>
                                                            </tr>
                                                            <td id="coverimg">
                                                                <img src=' . $imgRs . ' id="styleImgCardBox">  
                                                            </td>
                                                            <td style="font-size:8px;">
                                                                <strong><a id="infoFontCardBox">Type ' . $data->rs_type . '</a></strong>
                                                                <br/>
                                                                <strong><a id="infoFontCardBox">Level Maternal ' . $data->level_maternal . '</a></strong>
                                                            </td>                                                    
                                                        </table>
                                                    </div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getWard as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #4FBBF5;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }
                            } else {
                                echo ('
                                        <div style="padding: 5px; text-align: center;">
                                            <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                </div>
            </div>                

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">                    
                        <?php
                            $y = $hcu[0]->stock_hcu;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');                            
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if ($y != 0) {
                                echo ('
                                        <div class="wrapper">                                
                                            <strong><a>HCU</a></strong>
                                            <br />
                                            <strong><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></strong>
                                        </div>
                                    ');
                            } else {
                                echo ('                            
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src='. $imgLonceng .' id="styleIconLonceng">
                                                <audio id="audioplayer" autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>                                                
                                            </div>
                                            <div class="col-md-10">
                                                <strong><div class="col-sm-12"><a>HCU</a></div></strong>
                                                <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></div></strong>
                                            </div>
                                        </div>
                                    ');
                            }
                            ?>
                        </div>
                        <div class="card-body">
                            <?php
                            $y = $hcu[0]->stock_hcu;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if ($y != 0) {
                                if ($y <= 10) {
                                    if(!empty($getHcu)){ 
                                        foreach ($getHcu as $data) {

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                            }else{
                                                $imgRs = base_url(img_default);
                                            }

                                            echo ('
                                                    <div class="card" style="padding-left:3px; padding-bottom:3px; background-color:#F58A81;">
                                                        <table id="infoStyleTable">
                                                            <tr>
                                                                <th colspan="3"><strong><a id="infoFontRs">' . $data->rs_name . '</a></strong> </th>
                                                            </tr>
                                                            <td id="coverimg">
                                                                <img src=' . $imgRs . ' id="styleImgCardBox">  
                                                            </td>
                                                            <td style="font-size:8px;">
                                                                <strong><a id="infoFontCardBox">Type ' . $data->rs_type . '</a></strong>
                                                                <br/>
                                                                <strong><a id="infoFontCardBox">Level Maternal ' . $data->level_maternal . '</a></strong>
                                                            </td>                                                    
                                                        </table>
                                                    </div>
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');                                    
                                    $counter = 1;
                                    foreach ($getHcu as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #F58A81;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #F58A81;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }
                            } else {
                                echo ('
                                            <div style="padding: 5px; text-align: center;">
                                                <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                            </div>
                                        ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php
                            $y = $igdPonek[0]->stock_igd_ponek;
                            $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                            $audio = base_url('assets/images/mp3/audio.mpeg');
                            if ($y != 0) {
                                echo ('
                                        <div class="wrapper" style="text-align: center;">                                
                                            <strong><a>Ruang Isolasi IGD Ponek</a></strong>
                                            <br />
                                            <strong><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></strong>
                                        </div>
                                    ');
                            } else {
                                echo ('
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src=' . $imgLonceng . ' id="styleIconLonceng">
                                                <audio id="audioplayer" autoplay loop>
                                                    <source src="'.$audio.'" type="audio/mpeg">
                                                </audio>
                                            </div>
                                            <div class="col-md-10">
                                                <strong><div class="col-sm-12"><a>Ruang Isolasi IGD Ponek</a></div></strong>
                                                <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></div></strong>
                                            </div>
                                        </div>
                                    ');
                            }
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                            $y = $igdPonek[0]->stock_igd_ponek;
                            $img_data_null = base_url("assets/images/icon/clos_icon.png");
                            if ($y != 0) {
                                if ($y <= 10) {
                                    if(!empty($getIgdPonek)){
                                        foreach ($getIgdPonek as $data) {

                                            $imgRs = null;
                                            if(!empty($data->img_rs)){
                                                $imgRs = $data->img_rs;
                                            }else{
                                                $imgRs = base_url(img_default);
                                            }

                                            echo ('
                                                    <div class="card" style="padding-left:3px; padding-bottom:3px; background-color:#B696FC;">
                                                        <table id="infoStyleTable">
                                                            <tr>
                                                                <th colspan="3"><strong><a id="infoFontRs">' . $data->rs_name . '</a></strong> </th>
                                                            </tr>
                                                            <td id="coverimg">
                                                                <img src=' . $imgRs . ' id="styleImgCardBox">
                                                            </td>
                                                            <td style="font-size:8px;">
                                                                <strong><a id="infoFontCardBox">Type ' . $data->rs_type . '</a></strong>
                                                                <br/>
                                                                <strong><a id="infoFontCardBox">Level Maternal : ' . $data->level_maternal . '</a></strong>
                                                            </td>                                                    
                                                        </table>
                                                    </div> 
                                                ');
                                        }
                                    }
                                } else {
                                    echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');
                                    $counter = 1;
                                    foreach ($getIgdPonek as $data) {
                                        if ($counter % 2 == 0) {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKiri" style="background-color: #B696FC;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        } else {
                                            echo ('
                                                    <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #B696FC;">
                                                        <strong><a>' . $data->rs_name . '</a></strong>
                                                    </div>
                                                ');
                                        }
                                        $counter++;
                                    }
                                    echo ('</div>');
                                }
                            } else {
                                echo ('
                                            <div style="padding: 5px; text-align: center;">
                                                <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                            </div>
                                        ');
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card" id="styleCardMinHeight">
                    <div class="card-header" id="infoStyleTitle">
                        <?php
                        $y = $covid[0]->total_covid;
                        $imgLonceng = base_url('assets/images/icon/gold-bell.png');
                        $audio = base_url('assets/images/mp3/audio.mpeg');
                        if ($y != 0) {
                            echo ('
                                    <div class="wrapper" style="text-align: center;">                                
                                        <strong><a>Covid</a></strong>
                                        <br />
                                        <strong><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></strong>
                                    </div>
                                ');
                        } else {
                            echo ('
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src=' . $imgLonceng . ' id="styleIconLonceng">
                                            <audio id="audioplayer" autoplay loop>
                                                <source src="'.$audio.'" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="col-md-10">
                                            <strong><div class="col-sm-12"><a>Covid</a></div></strong>
                                            <strong><div class="col-sm-12"><a>Yang Tersedia Onsite : ' . $y . ' Rs</a></div></strong>
                                        </div>
                                    </div>
                                ');
                        }
                        ?>
                    </div>
                    <div class="card-body">
                        <?php
                        $y = $covid[0]->total_covid;
                        $img_data_null = base_url("assets/images/icon/clos_icon.png");
                        if ($y != 0) {
                            if ($y <= 10) {      
                                if(!empty($getCovid)){                       
                                    foreach ($getCovid as $data) {

                                        $imgRs = null;
                                        if(!empty($data->img_rs)){
                                            $imgRs = $data->img_rs;
                                        }else{
                                            $imgRs = base_url(img_default);
                                        }

                                        echo ('
                                                <div class="card" style="padding-left:3px; padding-bottom:3px; background-color:#FC73DC;">
                                                    <table id="infoStyleTable">
                                                        <tr>
                                                            <th colspan="3"><strong><a id="infoFontRs">' . $data->rs_name . '</a></strong> </th>
                                                        </tr>
                                                        <td id="coverimg">
                                                            <img src=' . $imgRs . ' id="styleImgCardBox">  
                                                        </td>                                                                                                
                                                        <td style="font-size:8px;">
                                                            <strong><a id="infoFontCardBox">Type ' . $data->rs_type . '</a></strong>
                                                            <br/>
                                                            <strong><a id="infoFontCardBox">Level Maternal ' . $data->level_maternal . '</a></strong>
                                                        </td>                                                    
                                                    </table>
                                                </div>
                                            ');
                                    }
                                }
                            } else {
                                echo ('<div class="row" style="padding-bottom: 10px; text-align: center;">');                                
                                $counter = 1;
                                foreach ($getCovid as $data) {
                                    if ($counter % 2 == 0) {
                                        echo ('
                                                <div class="col-md-6" id="boxListKiri" style="background-color: #FC73DC;">
                                                    <strong><a>' . $data->rs_name . '</a></strong>
                                                </div>
                                            ');
                                    } else {
                                        echo ('
                                                <div class="col-md-6" id="boxListKanan" style="margin-bottom: 5px; background-color: #FC73DC;">
                                                    <strong><a>' . $data->rs_name . '</a></strong>
                                                </div>
                                            ');
                                    }
                                    $counter++;
                                }
                                echo ('</div>');
                            }
                        } else {
                            echo ('
                                    <div style="padding: 5px; text-align: center;">
                                        <img src=' . $img_data_null . ' id="styleIconClose">                                                                         
                                    </div>
                                ');
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

<style type="text/css">
    #boxList {
        font-size: 9px;
        border-radius: 20px;
        height: 30px;
        padding-top: 5px;
    }

    #boxListKiri {
        font-size: 9px;
        border-radius: 20px;
        padding-top: 5px;
        padding-bottom: 5px;
        margin-bottom: 5px;
    }

    #boxListKanan {
        font-size: 9px;
        border-radius: 20px;
        padding-top: 5px;
        padding-bottom: 5px;
        margin-left: -4px;
        margin-right: 4px;
    }

    #infoStyleTitle {
        text-align: center;
        font-size: 10px;
    }

    #styleIconLonceng {
        width: 20px;
        height: 20px;
        margin-left: -10px;
    }

    #styleImgCardBox {
        width: 35px;
        height: 30px;
        border-radius: 10%;
    }

    #styleIconClose {
        width: 130px;
        height: 125px;
        margin-left: -10px;
    }

    #infoFontRs {
        font-size: 10px;
    }

    #infoFontCardBox {
        font-size: 9px;
    }

    #infoStyleTable {
        margin-top: -5px;
    }

    #coverimg {
        width: 20%;
    }

    #styleCardMinHeight {
        min-height: 240px;
    }
</style>

<script>
    $(document).ready(function() {

        document.querySelector('#audioplayer source').setAttribute('src', 'assets/images/mp3/audio.mpeg?rand=' + Math.random());
        
        // REALTIME        
        var socketIoAddress = "<?php echo URL_SOCKET; ?>";
        var socket = io(socketIoAddress);
        
        socket.on('fromServer', data => {
            // console.log(data);
            location.reload();
        });

    });
</script>
