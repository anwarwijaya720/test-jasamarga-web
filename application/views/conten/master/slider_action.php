<style>
    #imgView {
        padding: 5px;
    }

    .loadAnimate {
        animation: setAnimate ease 2.5s infinite;
    }

    @keyframes setAnimate {
        0% {
            color: #000;
        }

        50% {
            color: transparent;
        }

        99% {
            color: transparent;
        }

        100% {
            color: #000;
        }
    }

    .custom-file-label {
        cursor: pointer;
    }
</style>

<script>
    var save_method; //for save method string
    var imgRs;

    $(document).ready(function() {

        data_table();
        $('#mydata').dataTable({
            "processing": true, //Feature control the processing indicator. 
            "responsive": true,
            "autoWidth": false,
        });
        //fungsi tampil barang
        function data_table() {
            $.ajax({
                type: 'ajax',
                url: "<?php echo base_url('master/UpdateSlider/slider') ?>",
                async: false,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        var no = i + 1;
                        html += '<tr>' +
                            '<td style="width: 20px;">' + no + '</td>' +
                            '<td>' +
                                '<img src="' + data[i].image + ' "style="height:50px;width:100px;" />' +
                            '</td>' +
                            '<td>' + data[i].deskripsi + '</td>' +
                            '<td>' + data[i].insert_date + '</td>' +
                            '<td>' + data[i].insert_by + '</td>' +
                            '<td style="text-align:left;">' +                          
                                '<a href="<?php echo base_url('master/UpdateSlider/edit/') ?>' + data[i].slider_id + '" class="btn btn-info btn-sm item_edit" style="height:35px;width:80px; margin-right:10px;">Edit</a>' +
                                '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data="' + data[i].slider_id + '" style="height:35px;width:80px;">Delete</a>' +
                            '</td>' +
                            '</tr>';
                    }
                    $('#show_data').html(html);
                }

            });
        }

        //GET UPDATE
        // $('#show_data').on('click','.item_edit',function(){
        //     var slider_id = $(this).attr('data');
        //     // console.log(id);

        //     save_method = 'update';
        //     $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        //     $('.modal-title').text('Edit'); // Set title to Bootstrap modal title    

        //     $.ajax({
        //         url: "UpdateSlider/edit/"+slider_id,
        //         type: "POST",
        //         dataType: "JSON",
        //         success: function(data) {
        //             console.log(data);  
        //             imgRs = data[0].image;
        //             $('[name="id"]').val(data[0].slider_id);
        //             $('[name="des"]').val(data[0].deskripsi);
        //             $('[name="slider"]').val(data[0].image);
        //             $("#imgView").attr("src",data[0].image);

        //             var reader = new FileReader();
        //             var filename = data[0].image;
        //             filename = filename.substring(filename.lastIndexOf('\\') + 1);
        //             reader.onload = function(e) {
        //                 debugger;
        //                 $('#imgView').attr('src', e.target.result);
        //                 $('#imgView').hide();
        //                 $('#imgView').fadeIn(200);
        //                 $('.custom-file-label').text(filename);
        //             }
                    
        //         },
        //         error: function(jqXHR, textStatus, errorThrown) {
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Oops...',
        //                 text: 'Error deleting data',
        //             });
        //         }
        //     });

        // });

        $('#show_data').on('click','.item_delete',function(){
            var slider_id = $(this).attr('data');        
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "UpdateSlider/delete/"+slider_id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data) {
                            console.log(data);
                            if (data['status'] == true) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Delete',
                                    text: 'delete data success!',
                                });
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                });
                            }                       
                            window.location.href = '<?php echo base_url('master/UpdateSlider') ?>';
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Error deleting data',
                            });
                        }
                    });
                }
            });

        });

    });

    function reloadTable() {        
        window.location.href = '<?php echo base_url('master/UpdateSlider') ?>';
    }

    function addEstudiante() {
        $('#formSlider')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
    }

    $("#inputFile").change(function(event) {
        fadeInAdd();
        getURL(this);
    });

    $("#inputFile").on('click', function(event) {
        fadeInAdd();
    });

    function getURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var filename = $("#inputFile").val();
            filename = filename.substring(filename.lastIndexOf('\\') + 1);
            reader.onload = function(e) {
                debugger;
                $('#imgView').attr('src', e.target.result);
                $('#imgView').hide();
                $('#imgView').fadeIn(500);
                $('.custom-file-label').text(filename);
            }
            reader.readAsDataURL(input.files[0]);
        }
        $(".alert").removeClass("loadAnimate").hide();
    }

    function fadeInAdd() {
        fadeInAlert();
    }

    function fadeInAlert(text) {
        $(".alert").text(text).addClass("loadAnimate");
    }

    function save() {
        var url = ""
        if(save_method == "update"){
            url = "UpdateSlider/editAction/" + imgRs
        }else{
            url = "UpdateSlider/save"
        }
        $.ajax({
            url: url,
            type: "POST",
            data: $('#formSlider').serialize(),
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if (data['status'] == true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Upload',
                        text: 'upload data success!',
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    });
                }
                $('#modal_form').modal('hide');
                // window.location.href = '<?php echo base_url('master/UpdateSlider') ?>';
                location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Upload Error',
                });
            }
        });
    }


</script>