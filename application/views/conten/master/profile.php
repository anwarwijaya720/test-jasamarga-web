<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Profile</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">User Profile</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

        <?php
          if(!empty($getRs))
          foreach($getRs as $data){
        ?>

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="<?php echo base_url("assets/images/icon/user.png"); ?>" alt="User profile picture">
              </div>
              <h3 class="profile-username text-center"><?php echo $data->username ?></h3>
              <p class="text-muted text-center"><?php echo $data->role_name ?></p>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">About Me</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-home mr-1"></i>Instansi</strong>
              <p class="text-muted">
              <?php echo $data->rs_name ?>
              </p>
              <hr>
              <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
              <p class="text-muted"><?php echo $data->address ?></p>
              <hr>
              <strong><i class="fas fa-graduation-cap mr-1"></i> Profession</strong>
              <p class="text-muted">
                <span class="tag tag-danger"><?php echo $data->profesi ?></span>
              </p>
              <hr>
              <strong><i class="fas fa-microphone mr-1"></i> Phone Number</strong>
              <p class="text-muted"><?php echo $data->no_hp ?></p>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Settings</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="active tab-pane" id="settings">
                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" id="profile">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="user" value="<?php echo $data->username ?>" id="inputName" placeholder="Name" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputTpl" class="col-sm-2 col-form-label">No Telepon</label>
                        <div class="col-sm-10">
                          <input type="number" class="form-control" value="<?php echo $data->no_hp ?>" name="noTlp" id="inputTpl" placeholder="noTlp" required>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputPass" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" autocomplete="off" name="pass" id="inputPass" placeholder="password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputPass" class="col-sm-2 col-form-label">Confirm Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" name="conPass" id="inputPass" placeholder="password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->

        <?php }?>

      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php require('profile_action.php') ?>