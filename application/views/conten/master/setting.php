<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Setting</h1>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Update User Role</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url('master/MasterRole/UpdateRole'); ?>">
                                <div class="form-group">
                                    <label class="col-form-label">Users Level</label>
                                    <div class="col-form-label">
                                        <select name="role" class="form-control" required>
                                            <option value=""> --- Choose --- </option>
                                            <?php
                                            foreach ($getRole as $dataRole) {
                                                echo "<option value='" . $dataRole->role_id . "'>" . $dataRole->role_name . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">

                                        <div class="col-md-4">
                                            <div class="card card-primary">
                                                <div class="card-header">
                                                    <h3 class="card-title">Master</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="master_product" value="product" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Laporan Rujukan
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="master_customer" value="customer" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Info Ketersediaan Ruangan
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="master_kitchen" value="kitchen" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Info Ketersediaan Darah
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="master_zona" value="zona" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Info Layanan Dr Spesialis
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="master_zona" value="zona" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Setting
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="card card-primary">
                                                <div class="card-header">
                                                    <h3 class="card-title">Transaction</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="product_order" value="product_order" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Ketersediaan Dr Spesialis
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="product_approve" value="product_approve" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Ketersediaan Ruangan
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="product_review" value="product_review" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Ketersediaan Darah
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="Inbox" value="Inbox" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            User Management
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="card card-primary">
                                                <div class="card-header">
                                                    <h3 class="card-title">Report</h3>
                                                </div>
                                                <div class="card-body">

                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="report_product_sale" value="report_product_sale" id="defaultCheck1">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                            Rekapitulasi
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <?php require('setting_form.php')?>
                </div>
            </div>

        </div>
    </section>