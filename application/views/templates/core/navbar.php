  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- <img src="<?php echo base_url("assets/images/logo/LogoMatneo.png"); ?>" style="height: 40px;"> -->
    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    
      <!-- Notifications Dropdown Menu -->
      <!-- <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li> -->

      <li class="dropdown user user-menu" style="margin-top: 5px; margin-left: 20px;">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 14px; color: black;">
          Superadmin
        </a>              
        <img src="<?php echo base_url("assets/dist/img/user2-160x160.jpg"); ?>" class="user-image" alt="User Image">
        <ul class="dropdown">
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a href="<?php echo base_url("master/Profile"); ?>" class="nav-link active">
                <p>
                  Profile
                </p>
              </a>   
              <a href="<?php echo base_url('Dashboard/logout'); ?>" class="nav-link active">              
                <p>
                  Logout
                </p>
              </a>   
          </div>  
        </ul>
      </li>

    </ul>
    
  </nav>
  <!-- /.navbar -->