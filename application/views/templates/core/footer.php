<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>CMS &copy; 2021 <a>Jasamarga</a>.</strong>  
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 0.0.1
    </div>
</footer>

<!-- howler js -->
<script src="<?php echo base_url('assets/plugins/howler/howler.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url("assets/plugins/jquery-ui/jquery-ui.min.js"); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets/plugins/sweetalert2/sweetalert2.all.js"); ?>"></script>
<!-- toastr -->
<script src="<?php echo base_url("assets/plugins/toastr/toastr.min.js"); ?>"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?php echo base_url("assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js"); ?>"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url("assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url("assets/plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url("assets/plugins/sparklines/sparkline.js"); ?>"></script>
<!-- JQVMap -->
<script src="<?php echo base_url("assets/plugins/jqvmap/jquery.vmap.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/jqvmap/maps/jquery.vmap.usa.js"); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url("assets/plugins/jquery-knob/jquery.knob.min.js"); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url("assets/plugins/moment/moment.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/daterangepicker/daterangepicker.js"); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url("assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"); ?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url("assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"); ?>"></script>
<!-- Summernote -->
<script src="<?php echo base_url("assets/plugins/summernote/summernote-bs4.min.js"); ?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url("assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/dist/js/adminlte.js"); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url("assets/dist/js/pages/dashboard.js"); ?>"></script> -->
<!-- ChartJS -->
<script src="<?php echo base_url("assets/plugins/chart.js/Chart.min.js"); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("assets/dist/js/demo.js"); ?>"></script>
<!-- jquery-validation -->
<script src="<?php echo base_url("assets/plugins/jquery-validation/jquery.validate.min.js"); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"); ?>"></script>
<!-- Socket Io-->
<script src="<?php echo base_url("assets/plugins/socket/socket.io.js");?>"></script>

<script>

$(document).ready(function() {
    moment.locale('id');
    timedUpdate();

    function updateClock() {
      $('#jam_gadang').text(moment().format('DD MMM YYYY, H:mm:ss a'));
    }

    function timedUpdate() {
      updateClock();
      setTimeout(timedUpdate, 1000);
    }
});

  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });    
    //Date range picker
    $('#date1').datetimepicker({
        format: 'L',
    });
    //Date range picker
    $("#dateTime").datepicker("option", "dateFormat", "yy-mm-dd hh:mm:ss");   
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        // format: 'MM/DD/YYYY hh:mm A'
        format: 'dd/mm/yyy'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  });
</script>
