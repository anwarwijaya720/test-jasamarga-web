
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url("assets/plugins/jquery-ui/jquery-ui.min.js"); ?>"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url("assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url("assets/plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url("assets/plugins/jquery-knob/jquery.knob.min.js"); ?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url("assets/plugins/chart.js/Chart.min.js"); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url("assets/plugins/moment/moment.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/daterangepicker/daterangepicker.js"); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url("assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"); ?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url("assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"); ?>"></script>
<!-- Socket Io-->
<script src="<?php echo base_url("assets/plugins/socket/socket.io.js");?>"></script>

<script>

$(document).ready(function() {
    moment.locale('id');
    timedUpdate();

    function updateClock() {
      $('#jam_gadang').text(moment().format('DD MMM YYYY, H:mm:ss a'));
    }

    function timedUpdate() {
      updateClock();
      setTimeout(timedUpdate, 1000);
    }
});

</script>
