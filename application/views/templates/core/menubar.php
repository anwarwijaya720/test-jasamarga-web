  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">  
    <a href="<?php echo base_url("Dashboard"); ?>" class="brand-link" style="text-align: center;">
      <!-- <span class="brand-text font-weight-light"><strong>MatneoSafe</strong></span> -->
      <img src="<?php echo base_url("assets/images/logo/jasamarga-icon.png"); ?>" style="height: 40px;">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
        <li class="nav-item">
            <a href="<?php echo base_url("jasamarga/Dashboard_admin"); ?>" class="nav-link">
              <i class="nav-icon far fa-chart-bar"></i>
                <p style="font-size: 14px;">
                  Dashboard Admin
                </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url("jasamarga/Dashboard_pemagang"); ?>" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
                <p style="font-size: 14px;">
                  Dashboard Pemagang
                </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url("jasamarga/Magang"); ?>" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
                <p style="font-size: 14px;">
                  Lowongan Mangang
                </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url("jasamarga/Kandidat"); ?>" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
                <p style="font-size: 14px;">
                  Daftar Kandidat
                </p>
            </a>
          </li>

          <!-- <li class="nav-item">
            <a href="<?php echo base_url("master/LaporanRujukan"); ?>" class="nav-link">
              <i class="nav-icon far fa-chart-bar"></i>
              <p style="font-size: 14px;">
                Laporan Rujukan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("master/InfoRoom"); ?>" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
                <p style="font-size: 14px;">
                  Info Ketersediaan Ruangan
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("master/InfoBlood"); ?>" class="nav-link">
              <i class="nav-icon far fa fa-tint"></i>
                <p style="font-size: 14px;">
                  Info Ketersediaan Darah
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("master/InfoDoctor"); ?>" class="nav-link">
              <i class="nav-icon far fa fa-calendar-alt"></i>
                <p style="font-size: 14px;">
                  Info Layanan Dr Spesialis
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("transaction/DokterSp"); ?>" class="nav-link">
              <i class="nav-icon far fa-user"></i>
                <p style="font-size: 14px;">
                  Update Lay Dr Spesialis
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("transaction/StockRoom"); ?>" class="nav-link">
              <i class="nav-icon far fa fa-inbox"></i>
                <p style="font-size: 14px;">
                  Update Lay Ruangan
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("transaction/StockBlood"); ?>" class="nav-link">
              <i class="nav-icon far fa fa-suitcase"></i>
                <p style="font-size: 14px;">
                  Update Lay Darah
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("master/Users"); ?>" class="nav-link">
              <i class="nav-icon far fa fa-address-card"></i>
                <p style="font-size: 14px;">
                  User Management
                </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url("report/Rekapitulasi"); ?>" class="nav-link">
              <i class="nav-icon fa fa-folder-open"></i>
                <p style="font-size: 14px;">
                  Rekapitulasi
                </p>
            </a>
          </li> -->
          <!-- Report -->
          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa fa-cog"></i>
              <p style="font-size: 14px;">
                Setting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url("master/UpdateRs"); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master RS</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("master/UpdateSlider"); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Slider</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("report/ExamplePagination"); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Example Pagination</p>
                </a>
              </li>
            </ul>
          </li> -->

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>