<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('templates/core/header'); ?>
<body style="background-color: #F2F2F2;">
    <div class="wrapper">
        <?php 
            $this->load->view('templates/core/navbar');
            $this->load->view('templates/core/menubar');
        ?>
        <div class="main-content">
            <?php $this->load->view($view) ?>
        </div>
        <?php 
            $this->load->view('templates/core/footer'); 
        ?>
    </div>
</body>
</html>