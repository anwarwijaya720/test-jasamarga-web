<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('templates/core/header_public'); ?>
<body style="background-color: #F2F2F2;">
    <div class="wrapper">        
        <div class="main-content">
            <?php $this->load->view($view) ?>
        </div>
        <?php
            $this->load->view('templates/core/footer_public'); 
        ?>
    </div>
</body>
</html>