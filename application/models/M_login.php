<?php

class M_login extends CI_Model{

	function cek_login($where)
	{
		$this->db->select('
							mu.user_id, mu.username, mu.no_hp,							
							mr.role_id, mr.role_name,
							ms.rs_name, ms.rs_type, ms.level_maternal, ms.img_rs,							
						');
		$this->db->from('m_users mu');
		$this->db->join('m_role mr', 'mu.role_id = mr.role_id', 'left');
		$this->db->join('m_rs ms', 'mu.rs_id =  ms.rs_id', 'left');
		$this->db->where($where);
		$query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        }else {
			$this->updatedStatusLogin($where);
            return $query->result();        
		}
	}

	function updatedStatusLogin($where)
	{		
		$data = array(
			'status' => '1',
		);		
		$this->db->where($where);
		$this->db->update('m_users', $data);
	}

	// function cek_login($table,$where){	
	// 	$username = $where["username"];	
	// 	$password = $where["password"];
		
	// 	return $this->db->query("SELECT * FROM $table mu
	// 							 left join m_role mr on mu.role_id = mr.role_id
	// 							 WHERE username LIKE '$username' AND password = '$password'
	// 						");
	// }
    
}

?>