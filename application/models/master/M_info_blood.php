<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_info_blood extends CI_Model{

	function getUsers()
	{
        $this->db->select('*');
        $this->db->from('m_users');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
    function info_blood($data, $as){
        $this->db->select('count('.$data.') as '.$as.' ');        
        $this->db->from('t_stock_blood ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where(''.$data.' = "tersedia"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function info_rs($data){
        $this->db->select('*');
        $this->db->from('t_stock_blood ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where(''.$data.' = "tersedia"');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
        
}