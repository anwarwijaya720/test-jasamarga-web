<?php

class M_register extends CI_Model{

    function getUsers()
	{
        $this->db->select('*');
        $this->db->from('m_users');
        $this->db->where('status != "2"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function delete($data, $where)
	{
		$this->db->where($where);
		$this->db->delete('m_users', $data);
	}

}

?>