<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_users extends CI_Model{

    var $column = array('username','no_hp','profesi');
    var $order = array('username' => 'desc');

	function getUsers()
	{
        $this->db->select('*');
        $this->db->from('m_users');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function example()
	{
        $this->db->select('*');
        $this->db->from('m_users');
        $this->db->where('status != "2"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getUsersWhere($where)
	{
        $this->db->select('*');
        $this->db->from('m_users');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getRs()
	{
        $this->db->select('rs_id, rs_name');
        $this->db->from('m_rs');
        // $this->db->where('institute_type = "RS"');
        $this->db->group_by('rs_name');
        $this->db->order_by('rs_name', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getRole()
	{
        $this->db->select('*');
        $this->db->from('m_role');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getWhere($where)
	{
        $this->db->select('*');
        $this->db->from('m_users mu');
        $this->db->join('m_role mr', 'mu.role_id = mr.role_id', 'left');
        $this->db->join('m_rs ms', 'mu.rs_id = ms.rs_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRsWhere($where)
	{
        $this->db->select('*');
        $this->db->from('m_rs');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function statusLogin($data, $where)
	{
		$this->db->where($where);
		$this->db->update('m_users', $data);
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from('m_users');
        return $this->db->count_all_results();
    }

    private function _get_datatables_query()
    {
        $this->db->select('*');
        $this->db->from('m_users mu');
        $this->db->join('m_role mr', 'mu.role_id = mr.role_id','left');
        $this->db->join('m_rs ms', 'mu.rs_id = ms.rs_id', 'left');
        $this->db->order_by('mu.created_date', 'desc');        
        $this->db->where('status != "2"');
        $i = 0;
        foreach ($this->column as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function delete($where, $data)
	{
        $this->db->where($where);
        $this->db->update('m_users', $data);
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
	}
    
    function save($data){
        $this->db->insert('m_users', $data);
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function addRs($data){        
        $this->db->insert('m_rs', $data);
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function edit($data, $where){
        $this->db->where($where);
        $this->db->update('m_users', $data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function approve($data, $where){
        $this->db->where($where);
        $this->db->update('m_users', $data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}

?>