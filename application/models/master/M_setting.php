<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends CI_Model
{
    function getRole(){
        $this->db->select('*');
        $this->db->from('m_role');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getSelectRole(){
        $this->db->select('*');
        $this->db->from('m_menu mm');
        $this->db->join('m_role mr', 'mm.role_id = mr.role_id', 'left');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
    function getUserRole(){
        $this->db->select('*');
        $this->db->from('m_role');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getMenuMaster($whereRoleId){      
        $this->db->select('role_id');
        $this->db->from('menu_master');
        $this->db->where($whereRoleId);
        $resposne = $this->db->get();
        if($resposne->num_rows() < 1){
            echo"Data Is Empty";
        }else{
            return $resposne->result();
        }
    }

    function getMaster($where, $menuRole){
        $this->db->select('mm.menu');
        $this->db->from('m_role mr');        
        $this->db->join('m_menu_master mm', 'mr.role_id = mm.'.$menuRole.'_id');
        $this->db->where_in($where);
        $resposne = $this->db->get();
        if($resposne->num_rows() < 1){
            // echo"Data Is Empty";
        }else{
            return $resposne->result();
        }
    }

    function insert_menu_role($data){        
		$this->db->insert('menu_master',$data);
    }

    function update_menu_role($data, $where){
        $this->db->where($where);
		$this->db->update('menu_master',$data);
    }
    
}