<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_slider extends CI_Model{
    
    function getSlider()
	{
        $this->db->select('*');
        $this->db->from('m_slider');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getSliderId($where){
        $this->db->select('*');
        $this->db->from('m_slider');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
        } else {
            return $query->result();
        }
    }

    function save($data){
        $this->db->insert('m_slider', $data);
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function update($data, $where){
        $this->db->where($where);
        $this->db->update('m_slider', $data);
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function getImgSlider($where){
        $this->db->select('*');
        $this->db->from('m_slider');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function delete($where){
        $this->db->where($where);
        $this->db->delete('m_slider');
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}