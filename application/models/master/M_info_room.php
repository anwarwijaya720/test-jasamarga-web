<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_info_room extends CI_Model{

	function stock_ward()
	{
        $this->db->select('count(ts.rs_id) as stock_ward');        
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('ruang_perawatan = "Tersedia"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function get_ward(){        
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('ts.ruang_perawatan = "Tersedia"');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

	function stock_nicu()
	{
        $this->db->select('count(ts.rs_id) as stock_nicu');        
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('nicu = "Tersedia"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function get_nicu(){
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('nicu = "Tersedia"');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function get_icu(){
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('icu = "Tersedia"');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

	function stock_icu()
	{
        $this->db->select('count(ts.rs_id) as stock_icu');        
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('icu = "Tersedia"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

	function stock_hcu()
	{
        $this->db->select('count(ts.rs_id) as stock_hcu');        
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('hcu = "Tersedia"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function get_hcu(){
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('hcu = "Tersedia"');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

	function stock_igd_ponek()
	{
        $this->db->select('count(ts.rs_id) as stock_igd_ponek');        
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('isolasi_igd_ponek = "Tersedia"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function get_igd_ponek(){
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('isolasi_igd_ponek = "Tersedia"');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

	function covid()
	{
        $this->db->select('COUNT(ts.rs_id) as total_covid');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('
            icu_covid = "Tersedia" OR 
            perina_covid = "Tersedia" OR 
            picu_covid  = "Tersedia" OR 
            nicu_covid = "Tersedia"
        ');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

	function getCovid()
	{
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id');
        $this->db->where('        
            icu_covid = "Tersedia" OR 
            perina_covid = "Tersedia" OR 
            picu_covid  = "Tersedia" OR 
            nicu_covid = "Tersedia"
        ');
        $this->db->order_by('ts.updated_date', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }    

	// function stock_icu_covid()
	// {
    //     $this->db->select('count(icu_covid) as stock_icu_covid');
    //     $this->db->from('t_stock_room');
    //     $this->db->where('icu_covid = "Tersedia"');
    //     $query = $this->db->get();
    //     if ($query->num_rows() < 1) {
    //         // echo"Database Is Empty";
    //     } else {
    //         return $query->result();        
    //     }
    // }    

	// function stock_perina_covid()
	// {
    //     $this->db->select('count(perina_covid) as stock_perina_covid');
    //     $this->db->from('t_stock_room');
    //     $this->db->where('perina_covid = "Tersedia"');
    //     $query = $this->db->get();
    //     if ($query->num_rows() < 1) {
    //         // echo"Database Is Empty";
    //     } else {
    //         return $query->result();        
    //     }
    // }

	// function stock_picu_covid()
	// {
    //     $this->db->select('count(picu_covid) as stock_picu_covid');
    //     $this->db->from('t_stock_room');
    //     $this->db->where('picu_covid = "Tersedia"');
    //     $query = $this->db->get();
    //     if ($query->num_rows() < 1) {
    //         // echo"Database Is Empty";
    //     } else {
    //         return $query->result();        
    //     }
    // }

	// function stock_nicu_covid()
	// {
    //     $this->db->select('count(nicu_covid) as stock_nicu_covid');
    //     $this->db->from('t_stock_room');
    //     $this->db->where('nicu_covid = "Tersedia"');
    //     $query = $this->db->get();
    //     if ($query->num_rows() < 1) {
    //         // echo"Database Is Empty";
    //     } else {
    //         return $query->result();        
    //     }
    // }

}