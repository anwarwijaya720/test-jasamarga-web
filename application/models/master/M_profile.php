<?php

class M_profile extends CI_Model{

    function getUser($where)
	{
		$this->db->select('
							mu.user_id, mu.username, mu.no_hp,							
							mr.role_id, mr.role_name,
							ms.rs_name, ms.rs_type, ms.level_maternal, ms.img_rs, address,
							mu.profesi							
						');
		$this->db->from('m_users mu');
		$this->db->join('m_role mr', 'mu.role_id = mr.role_id', 'left');
		$this->db->join('m_rs ms', ' mu.rs_id =  ms.rs_id', 'left');	
		$this->db->where($where);
		$query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        }else {
            return $query->result();        
		}	
	}

	function update($data, $where){
        $this->db->where($where);
        $this->db->update('m_users', $data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}

?>