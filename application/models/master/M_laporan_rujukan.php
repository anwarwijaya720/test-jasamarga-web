<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_laporan_rujukan extends CI_Model{

    function getTransaction()
	{
        $this->db->select('*');
        $this->db->from('t_rujuk');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

	function laporan_penerima_rujukan()
	{
        $result = $this->db->query('
            select 
                tr.rs_id_penerima,
                ms.rs_name,
                tr.kebutuhan,
                count(tr.rs_id_penerima) as total,
                ms.img_rs,
                CAST(tr.insert_date AS DATE) as tanggal
            from t_rujuk tr
            inner join m_rs ms on tr.rs_id_penerima = ms.rs_id 
            inner join (
                    select
                    rs_id_penerima, MAX(CAST(insert_date AS DATE)) AS max_date
                    FROM t_rujuk
                    where 
                    status = 0
                    GROUP BY rs_id_penerima
                    order by max_date desc 
                ) groupedtt
            ON tr.rs_id_penerima = groupedtt.rs_id_penerima 
            AND CAST(tr.insert_date AS DATE)  = groupedtt.max_date
            where 
            tr.kebutuhan LIKE "%SP. Obgyn + Sp. An%" AND status = 0 OR
            tr.kebutuhan LIKE "%Sp. Obgyn%" AND status = 0 OR
            tr.kebutuhan LIKE "%SP. PD%" AND status = 0 OR
            tr.kebutuhan LIKE "%Preklamsi/E%" AND status = 0 OR 
            tr.kebutuhan LIKE "%HPP%" AND status = 0 OR
            tr.kebutuhan LIKE "%Lainnya%" AND status = 0 OR 
            tr.kebutuhan LIKE "%SP. Obgyn + Isolasi IGD Ponek%" AND status = 0 OR
            tr.kebutuhan LIKE "%SP. P%" AND status = 0 OR
            tr.kebutuhan LIKE "%OK Tekanan Negatif%" AND status = 0 OR            
            tr.kebutuhan LIKE "%ICU Covid%" AND status = 0 OR
            tr.kebutuhan LIKE "%Perina Covid%" AND status = 0 OR
            tr.kebutuhan LIKE "%PICU Copid%" AND status = 0 OR
            tr.kebutuhan LIKE "%NICU Covid%" AND status = 0
            group by tr.rs_id_penerima, ms.rs_name 
            order by tanggal desc
        ');        
        return $result->result();
    }

    function laporan_rujukan()  
	{
        $result = $this->db->query('
            select 
                tr.rs_id_penerima,
                ms.rs_name,
                tr.kebutuhan,
                count(tr.rs_id_penerima) as total,
                ms.img_rs,
                CAST(tr.insert_date AS DATE) as tanggal
            from t_rujuk tr
            inner join m_rs ms on tr.rs_id_penerima = ms.rs_id 
            inner join (
                    select
                    rs_id_penerima, MAX(CAST(insert_date AS DATE)) AS max_date
                    FROM t_rujuk
                    where 
                    status = 0
                    GROUP BY rs_id_penerima
                    order by max_date desc 
                ) groupedtt
            ON tr.rs_id_penerima = groupedtt.rs_id_penerima 
            AND CAST(tr.insert_date AS DATE)  = groupedtt.max_date
            where 
            tr.kebutuhan LIKE "%Asfiksia%" AND status = 0 OR
            tr.kebutuhan LIKE "%BBLSR%" AND status = 0 OR
            tr.kebutuhan LIKE "%SP.Anak%" AND status = 0
            group by tr.rs_id_penerima, ms.rs_name 
            order by tanggal desc
        ');        
        return $result->result();
    }

    function laporan_nakses_perujuk_ibu()  
	{
        $result = $this->db->query('
            select 
                tr.rs_id_perujuk,
                ms.rs_name,
                tr.kebutuhan,
                count(tr.rs_id_perujuk) as total,
                ms.img_rs,
                CAST(tr.insert_date AS DATE) as tanggal
            from t_rujuk tr
            inner join m_rs ms on tr.rs_id_perujuk = ms.rs_id 
            inner join (
                    select
                    rs_id_perujuk, MAX(CAST(insert_date AS DATE)) AS max_date
                    FROM t_rujuk
                    where 
                    status = 0
                    GROUP BY rs_id_perujuk
                    order by max_date desc 
                ) groupedtt
            ON tr.rs_id_perujuk = groupedtt.rs_id_perujuk 
            AND CAST(tr.insert_date AS DATE)  = groupedtt.max_date
            where 
            tr.kebutuhan LIKE "%SP. Obgyn + Sp. An%" AND status = 0 OR
            tr.kebutuhan LIKE "%Sp. Obgyn%" AND status = 0 OR
            tr.kebutuhan LIKE "%SP. PD%" AND status = 0 OR
            tr.kebutuhan LIKE "%Preklamsi/E%" AND status = 0 OR 
            tr.kebutuhan LIKE "%HPP%" AND status = 0 OR
            tr.kebutuhan LIKE "%Lainnya%" AND status = 0 OR 
            tr.kebutuhan LIKE "%SP. Obgyn + Isolasi IGD Ponek%" AND status = 0 OR
            tr.kebutuhan LIKE "%SP. P%" AND status = 0 OR
            tr.kebutuhan LIKE "%OK Tekanan Negatif%" AND status = 0 OR            
            tr.kebutuhan LIKE "%ICU Covid%" AND status = 0 OR
            tr.kebutuhan LIKE "%Perina Covid%" AND status = 0 OR
            tr.kebutuhan LIKE "%PICU Copid%" AND status = 0 OR
            tr.kebutuhan LIKE "%NICU Covid%" AND status = 0
            group by tr.rs_id_perujuk, ms.rs_name 
            order by tanggal desc
        ');        
        return $result->result();
    }

    function laporan_nakses_perujuk_total()  
	{
        $result = $this->db->query('
            select 
                tr.rs_id_perujuk,
                ms.rs_name,
                tr.kebutuhan,
                count(tr.rs_id_perujuk) as total,
                ms.img_rs,
                CAST(tr.insert_date AS DATE) as tanggal
            from t_rujuk tr
            inner join m_rs ms on tr.rs_id_perujuk = ms.rs_id 
            inner join (
                    select
                    rs_id_perujuk, MAX(CAST(insert_date AS DATE)) AS max_date
                    FROM t_rujuk
                    where 
                    status = 0
                    GROUP BY rs_id_perujuk
                    order by max_date desc 
                ) groupedtt
            ON tr.rs_id_perujuk = groupedtt.rs_id_perujuk 
            AND CAST(tr.insert_date AS DATE)  = groupedtt.max_date
            where 
            tr.kebutuhan LIKE "%Asfiksia%" AND status = 0 OR
            tr.kebutuhan LIKE "%BBLSR%" AND status = 0 OR
            tr.kebutuhan LIKE "%SP.Anak%" AND status = 0
            group by tr.rs_id_perujuk, ms.rs_name 
            order by tanggal desc
        ');        
        return $result->result();        
    }

    // Grafik Data 
    function stock_obgyn_anak()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%SP. Obgyn + Sp.An%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_obgyn()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%SP. Obgyn%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_sp_anak()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%SP. Anak%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_sp_jantung()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%SP. JP%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_sp_pd()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%SP. PD%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }    

    function stock_sp_paru()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%SP. P%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    //RUANGAN
    function stock_r_perawatan()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%R. Perawatan%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
    function stock_r_nicu()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan = "NICU" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
    function stock_r_instensif()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan = "ICU" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_r_hcu()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan = "HCU" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_r_ponek()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('kebutuhan LIKE "%Isolasi IGD Ponek%" AND tr.status = 0');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function stock_r_covid(){
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('
            tr.kebutuhan LIKE "%Perina Covid%" AND tr.status = 0 OR
            tr.kebutuhan = "ICU Covid" AND tr.status = 0 OR
            tr.kebutuhan = "NICU Covid" AND tr.status = 0 OR
            tr.kebutuhan = "PICU Covid" AND tr.status = 0
        ');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    //DATA GRAFIK COFID DI JADIKAN SATU 
    function covid()
	{
        $this->db->select('COUNT(kebutuhan) as total');
        $this->db->from('t_rujuk tr');        
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id');
        $this->db->where('tr.kebutuhan LIKE "%ICU%"');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }


    // Report
    function export($where){
        
        $time="";   

        switch ($where) {
            case "hari":
                $time = 'CAST(insert_date AS DATE) = CURDATE()';
                break;
            case "minggu":
                $time = 'CURDATE() BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()';
                break;
            case "bulan":
                $time = 'insert_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()';
                break;
            case "tahun":
                $time = 'insert_date >= DATE_SUB(NOW(),INTERVAL 1 YEAR)';
                break;
            }

        $this->db->select('rs_id_penerima, ms.rs_name, kebutuhan, COUNT(kebutuhan) as total, CAST(insert_date AS DATE) as tanggal');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_penerima = ms.rs_id');
        $this->db->where('status = 0 AND '.$time.' ');
        $this->db->group_by('rs_id_penerima, ms.rs_name, kebutuhan, tanggal');
        $this->db->order_by('tanggal', 'desc');
        $query = $this->db->get();
        // print_r($this->db->last_query());
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }


}