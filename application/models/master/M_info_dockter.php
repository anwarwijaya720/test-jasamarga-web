<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_info_dockter extends CI_Model{

	function obgyn_anak($status)
	{
        $this->db->select('count(DISTINCT ms.rs_id) as total_obgyn_anak');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where('ms.obgyn = '.$status.' or ms.anak = '.$status.'');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result(); 
        }
    }
    
	function obgyn($status)
	{
        $this->db->select('count(DISTINCT ms.rs_id) as total_obgyn');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where('ms.obgyn = '.$status.'');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
	function anak($status)
	{
        $this->db->select('count(DISTINCT ms.rs_id) as total_anak');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where('ms.anak = '.$status.'');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
	function jantung($status)
	{
        $this->db->select('count(DISTINCT ms.rs_id) as total_jantung');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where('ms.jp = '.$status.'');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
	function dalam($status)
	{
        $this->db->select('count(DISTINCT ms.rs_id) as total_dalam');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where('ms.pd = '.$status.'');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }
    
	function paru($status)
	{
        $this->db->select('count(DISTINCT ms.rs_id) as total_paru');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where('ms.spp = '.$status.'');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function get_data_rs($where)
	{
        $this->db->select('distinct (ms.rs_id), mr.rs_name, mr.rs_type, mr.img_rs, mr.level_maternal, mr.address, ms.date');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where($where);
        $this->db->group_by('mr.rs_name');
        $this->db->order_by('ms.date', 'desc');      
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }


    function status_off($where)
	{
        $this->db->select('mr.rs_name, ms.*');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where(''.$where.' = 1 and ms.status = "off" ');
        $this->db->group_by('mr.rs_name');
        $this->db->order_by('ms.date', 'desc');      
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function status_off_ObgynAnak()
	{
        $this->db->select('mr.rs_name, ms.*');
        $this->db->from('m_schedule ms');
        $this->db->join('m_rs mr', 'ms.rs_id = mr.rs_id');
        $this->db->where(' ms.obgyn = 1 and ms.status = "off" or ms.anak = 1 and ms.status = "off"');
        $this->db->group_by('mr.rs_name');
        $this->db->order_by('ms.date', 'desc');      
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

}