<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_pagination extends CI_Model{

    function getRs()
	{
        $this->db->select('rs_id, rs_name');
        $this->db->from('m_rs');
        // $this->db->where('institute_type = "RS"');
        $this->db->group_by('rs_name');
        $this->db->order_by('rs_name', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getData($searchTerm=""){
        // Fetch users
     $this->db->select('*');
     $this->db->from('m_rs');
     $this->db->where("rs_name like '%".$searchTerm."%' ");
     $fetched_records = $this->db->get();
     $users = $fetched_records->result_array();

     // Initialize Array with fetched data
     $data = array();
     foreach($users as $user){
        $data[] = array("id"=>$user['rs_id'], "text"=>$user['rs_name']);
     }
     return $data;
    }

}