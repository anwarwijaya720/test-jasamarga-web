<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_rekapitulasi extends CI_Model{

    function cekTransaction()
	{
        $this->db->select('*');
        $this->db->from('t_rujuk');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getUsers()
	{
        $this->db->select('*');
        $this->db->from('m_users');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasi()
	{
        $this->db->select('
            ms.rs_id,
            ms.rs_name,
            count(CAST(insert_date AS DATE)) as total,
            CAST(insert_date AS DATE) as tanggal,
            tr.status
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = ms.rs_id', 'left');
        $this->db->where('tr.status = 0');
        $this->db->group_by('tanggal, ms.rs_name');
        $this->db->order_by('tanggal', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiMom($date1, $date2, $rs)
	{
        $this->db->select('            
            tr.rs_id_perujuk,
            ms.rs_name,
            COUNT(kebutuhan) as total,
            kebutuhan,
            CAST(tr.insert_date AS DATE) as tanggal 
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = ms.rs_id', 'left');
        $this->db->where('          
            CAST(insert_date AS DATE) between "'.$date1.'" and "'.$date2.'" AND       
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%SP. Obgyn + Sp. An%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%Sp. Obgyn%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%SP. PD%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%Preklamsi/E%" AND tr.status = 0 OR 
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%HPP%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%Lainnya%" AND tr.status = 0 OR 
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%SP. Obgyn + Isolasi IGD Ponek%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%SP. P%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%OK Tekanan Negatif%" AND tr.status = 0 OR            
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%ICU Covid%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%Perina Covid%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%PICU Copid%" AND tr.status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%NICU Covid%" AND tr.status = 0
        ');
        $this->db->group_by('tanggal, ms.rs_name, tr.kebutuhan');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        // print_r($this->db->last_query());
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiMomOnsite($date1, $date2, $rs)
	{
        $this->db->select('            
            tr.rs_id_perujuk,
            mr.rs_name,
            COUNT(tr.kebutuhan) as total,
            tr.kebutuhan,
            CAST(tr.insert_date AS DATE) as tanggal 
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_schedule ms', 'tr.rs_id_perujuk = ms.rs_id AND CAST(tr.insert_date AS DATE) = CAST(ms.update_date AS DATE)', 'left');
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id', 'left');
        $this->db->where('
            tr.status = 0 and
            CAST(tr.insert_date AS DATE) between "'.$date1.'" and "'.$date2.'" AND
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%SP. Anak%" AND tr.status = 0 AND ms.status = "onsite" AND anak="1" 
        ');
        
        //belum ada di table
        // OR
        // tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%BBLR%" AND tr.status = 0 AND ms.status = "onsite" AND BBLR="1" OR
        // tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%Asfiksia/E%" AND tr.status = 0 AND ms.status = "onsite" AND Asfiksia="1"

        $this->db->group_by('mr.rs_name, tr.kebutuhan, tanggal');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiMomRoom($date1, $date2, $rs)
	{
        $this->db->select('
            tr.rs_id_perujuk,
            ms.rs_name,
            count(CAST(insert_date AS DATE)) as total,
            CAST(insert_date AS DATE) as tanggal,
            status
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = rs_id', 'left');
        $this->db->join('t_stock_room ts', 'tr.rs_id_perujuk = ts.rs_id AND CAST(tr.insert_date AS DATE) = CAST(ts.updated_date AS DATE)', 'left');
        $this->db->where('
            tr.status = 0 AND
            rs_id_perujuk = "'.$rs.'" AND
            CAST(insert_date AS DATE) between "'.$date1.'" and "'.$date2.'" AND            
            ts.rs_id = "'.$rs.'" AND ts.hcu = "Tersedia" OR
            ts.rs_id = "'.$rs.'" AND ts.icu = "Tersedia" OR
            ts.rs_id = "'.$rs.'" AND ts.ruang_perawatan = "Tersedia" OR
            ts.rs_id = "'.$rs.'" AND ts.ruang_isolasi_ibu = "Tersedia" OR 
            ts.rs_id = "'.$rs.'" AND ts.isolasi_igd_ponek = "Tersedia" OR
            ts.rs_id = "'.$rs.'" AND ts.icu_covid = "Tersedia" OR 
            ts.rs_id = "'.$rs.'" AND ts.picu_covid = "Tersedia" OR 
            ts.rs_id = "'.$rs.'" AND ts.nicu_covid = "Tersedia" OR
            ts.rs_id = "'.$rs.'" AND tekanan_negatif = "Tersedia"
        ');
        $this->db->group_by('tr.rs_id_perujuk, ms.rs_name');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiNeonatal($date1, $date2, $rs)
	{
        $this->db->select('           
            tr.rs_id_perujuk,
            ms.rs_name,
            COUNT(kebutuhan) as total,
            kebutuhan,
            CAST(tr.insert_date AS DATE) as tanggal 
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = rs_id', 'left');
        $this->db->where('            
            CAST(tr.insert_date AS DATE) BETWEEN "'.$date1.'" AND "'.$date2.'" AND
            tr.rs_id_perujuk = "'.$rs.'" AND kebutuhan = "Asfiksia" AND status = 0 OR
            tr.rs_id_perujuk = "'.$rs.'" AND kebutuhan = "BBLR" AND status = 0 OR 
            tr.rs_id_perujuk = "'.$rs.'" AND kebutuhan = "SP. Anak" AND status = 0            
        ');
        $this->db->group_by('tanggal, ms.rs_name, kebutuhan');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiNeonatalOnsite($date1, $date2, $rs)
	{
        $this->db->select('
            tr.rs_id_perujuk,
            mr.rs_name,
            COUNT(kebutuhan) as total,
            kebutuhan,
            CAST(tr.insert_date AS DATE) as tanggal 
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs mr', 'tr.rs_id_perujuk = mr.rs_id', 'left');
        $this->db->join('m_schedule ms', 'tr.rs_id_perujuk = ms.rs_id AND CAST(tr.insert_date AS DATE) = ms.date', 'left');
        $this->db->where('
            tr.status = 0 and
            CAST(tr.insert_date AS DATE) between "'.$date1.'" and "'.$date2.'" AND
            tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%SP. Anak%" AND tr.status = 0 AND ms.status = "onsite" AND anak="1" 
        ');

        //belum ada di table
        // OR
        // tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%BBLR%" AND tr.status = 0 AND ms.status = "onsite" AND BBLR="1" OR
        // tr.rs_id_perujuk = "'.$rs.'" AND tr.kebutuhan LIKE "%Asfiksia/E%" AND tr.status = 0 AND ms.status = "onsite" AND Asfiksia="1"

        $this->db->group_by('CAST(tr.insert_date AS DATE), tr.rs_id_perujuk, kebutuhan ');
        $this->db->order_by("CAST(tr.insert_date AS DATE)", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiNeonatalRoom($date1, $date2, $rs)
	{
        $this->db->select('
            tr.rs_id_perujuk,
            ms.rs_name,
            count(CAST(insert_date AS DATE)) as total,
            CAST(insert_date AS DATE) as tanggal,
            status
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = rs_id', 'left');
        $this->db->join('t_stock_room ts', 'tr.rs_id_perujuk = ts.rs_id AND CAST(tr.insert_date AS DATE) = CAST(ts.updated_date AS DATE)', 'left');
        $this->db->where('
            tr.status = 0 AND
            rs_id_perujuk = "'.$rs.'" AND
            CAST(insert_date AS DATE) between "'.$date1.'" and "'.$date2.'"            
        ');
        $this->db->group_by('tr.rs_id_perujuk, ms.rs_name');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiUpdateLayanan($date1, $date2, $rs)
	{   
        $this->db->select('
            tr.rs_id_perujuk,
            ms.rs_name,
            count(CAST(insert_date AS DATE)) as total,
            CAST(insert_date AS DATE) as tanggal
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = rs_id', 'left');
        $this->db->where('
            tr.status = 1 AND
            rs_id_perujuk = "'.$rs.'" AND
            CAST(insert_date AS DATE) between "'.$date1.'" and "'.$date2.'"
        ');
        $this->db->group_by('tr.rs_id_perujuk, ms.rs_name');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRekapitulasiBatal($date1, $date2, $rs)
	{
        $this->db->select('
            tr.rs_id_perujuk,
            ms.rs_name,
            count(CAST(insert_date AS DATE)) as total,
            CAST(insert_date AS DATE) as tanggal
        ');
        $this->db->from('t_rujuk tr');
        $this->db->join('m_rs ms', 'tr.rs_id_perujuk = rs_id', 'left');
        $this->db->where('
            tr.status = 1 AND
            rs_id_perujuk = "'.$rs.'" AND
            CAST(insert_date AS DATE) between "'.$date1.'" and "'.$date2.'"     
        ');
        $this->db->group_by('tr.rs_id_perujuk, tanggal');
        $this->db->order_by("tanggal", "desc");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }
    

    function getRs()
	{
        $this->db->select('rs_id, rs_name');
        $this->db->from('m_rs');
        $this->db->where('institute_type = "RS"');
        $this->db->group_by('rs_name');
        $this->db->order_by('rs_name', 'asc');        
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function update($data, $where){
        $this->db->where($where);
        $this->db->update('m_rs', $data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function getRsWhere($where){       
        $this->db->select('*');
        $this->db->from('m_rs');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function rekapRoom($rsId, $date1, $date2){
        $result = $this->db->query('
            select 
            ts.stock_room_id, ts.rs_id, 
            ms.rs_name,
            ts.updated_date
            from t_stock_room ts
            inner join m_rs ms on ts.rs_id = ms.rs_id 
            WHERE 
            CAST(ts.updated_date AS DATE) between "'.$date1.'" and "'.$date2.'"  
            and
            substring(ts.updated_date , 12,2) between substring( DATE_ADD(now(), interval -9 hour ),12, 2 ) and  substring(now(),12, 2 )
            and 
            ts.rs_id = "'.$rsId.'"
            ORDER BY ts.updated_date DESC                    
        ');        
        return $result->result();
    }

    function rekapBlood($rsId, $date1, $date2){
        $result = $this->db->query('
            select
            ts.stock_blood_id, ts.rs_id, 
            ms.rs_name,
            ts.updated_date 
            from t_stock_blood ts
            inner join m_rs ms on ts.rs_id = ms.rs_id 
            WHERE 
            CAST(ts.updated_date AS DATE) between "'.$date1.'" and "'.$date2.'"  
            and
            substring(ts.updated_date , 12,2) between substring( DATE_ADD(now(), interval -9 hour ),12, 2 ) and  substring(now(),12, 2 )
            and 
            ts.rs_id = "'.$rsId.'"
            ORDER BY ts.updated_date DESC                    
        ');        
        return $result->result();
    }

    function rekapDoc($rsId, $date1, $date2){
        $result = $this->db->query('
            select 
            ts.rs_id, 
            ms.rs_name,
            ts.update_date 
            from m_schedule ts
            inner join m_rs ms on ts.rs_id = ms.rs_id 
            WHERE 
            CAST(ts.update_date AS DATE) between "'.$date1.'" and "'.$date2.'"  
            and
            substring(ts.update_date , 12,2) between substring( DATE_ADD(now(), interval -9 hour ),12, 2 ) and  substring(now(),12, 2 )
            and 
            ts.rs_id = "'.$rsId.'"
            GROUP BY ts.update_date 
            ORDER BY ts.update_date DESC                   
        ');        
        return $result->result();
    }

}