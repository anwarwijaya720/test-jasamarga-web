<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_uploadrs extends CI_Model
{

    function getrs()
    {
        $this->db->select('*');
        $this->db->from('m_users');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function inFile()
    {
        $path = "C:\\xampp\\htdocs\\cms-wisesa\\doc\\m_rs_upload.csv";
        $qq = `LOAD DATA INFILE {str_replace("\\",$path)}
        INTO TABLE m_rs (kode_rs, rs_name, long, lat) FIELDS TERMINATED BY ','
        ENCLOSED BY '"' LINES TERMINATED BY '\r\n'`;       
        $result = $this->db->query($qq);
        return $result->result();

        $file_path = FCPATH."doc\upload.txt"; //It should always be set to FCPATH to make sure that the path is absolute
        $table_name = "m_rs";
        $query_name = "LOAD DATA LOCAL INFILE '"
            . $file_path .
            "' INTO TABLE "
            . $table_name .
            "` FIELDS TERMINATED BY ',' 
             LINES TERMINATED BY '\\n' 
             (kode_rs, rs_name, lng, lat)
              ";

        // $query_name = "
        // LOAD DATA 
        // LOCAL INFILE 'C:\\xampp\\htdocs\\cms-wisesa\\doc\\m_rs_upload.csv'
        //   INTO TABLE m_rs
        //              (kode_rs, rs_name, long, @lat)
        //          SET lat = CAST(@lat AS DECIMAL(33,30));
        // ";

        $result = $this->db->query($query_name);
        return $result->result();
    }

    function save($data)
    {
        $this->db->insert('m_rs', $data);
        $cek = $this->db->affected_rows();
        if ($cek == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update($data, $where)
    {
        $this->db->where($where);
        $this->db->update('m_rs', $data);
        $cek = $this->db->affected_rows();
        if ($cek == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
