<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_room extends CI_Model{

    function getRs()
	{
        $this->db->select('rs_id, rs_name');
        $this->db->from('m_rs');
        $this->db->where('institute_type = "RS"');
        $this->db->group_by('rs_name');
        $this->db->order_by('rs_name', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRoom($where){
        $this->db->select('*');
        $this->db->from('m_rs mr');
        $this->db->join('t_stock_room ts', 'mr.rs_id = ts.rs_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRoomById($rsId){ 
        $this->db->select('*');
        $this->db->from('t_stock_room ts');
        $this->db->join('m_rs mr', 'ts.rs_id = mr.rs_id', 'left');
        $this->db->where('
            ts.rs_id = "'.$rsId.'"
            and
            CAST(ts.insert_date AS DATE)
            in (
                select MAX(CAST(insert_date AS DATE)) from t_stock_room
                where 
                rs_id = "'.$rsId.'"
            )        
        ');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getStockRoom($where){        
        $this->db->select('*');
        $this->db->from('t_stock_room');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function save_stock_room($data){        
        $this->db->insert('t_stock_room',$data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function update_stock_room($data, $where){
        $this->db->where($where);
        $this->db->update('t_stock_room',$data);                
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

?>
