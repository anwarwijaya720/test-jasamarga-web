<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_dokter extends CI_Model{

	function getRs()
	{
        $this->db->select('rs_id, rs_name');
        $this->db->from('m_rs');
        $this->db->where('institute_type = "RS"');
        $this->db->group_by('rs_name');
        $this->db->order_by('rs_name', 'asc');   
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getScheduleByStatus($where)
	{
        $this->db->select('*');
        $this->db->from('m_schedule');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getSp()
	{
        $this->db->select('*');
        $this->db->from('m_doctor');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getSs()
	{
        $this->db->select('*');
        $this->db->from('m_spesialis');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getDoctor($spId){
        $where = array(
            'doctor_id' => $spId
        );
        $this->db->select('*');
        $this->db->from('m_doctor');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getWhereSpesialis($where){
        $this->db->select('*');
        $this->db->from('m_spesialis');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getWhereSchedule($where){
        $this->db->select('*');
        $this->db->from('m_schedule');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }
    

    function getSchedule($where)
	{
        $this->db->select('*');
        $this->db->from('m_doctor');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function save($data)
	{
        $this->db->insert('m_schedule',$data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    
    function update_schedule($data, $where){
        $this->db->where($where);
        $this->db->update('m_schedule',$data);            
        $cek = $this->db->affected_rows();
        // print_r($this->db->last_query());        
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function save_sp($dataSp){
        $this->db->insert('m_spesialis',$dataSp);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function update_sp($dataSp, $where, $like){
        $this->db->where($where);
        $this->db->like($like);
        $this->db->update('m_spesialis', $dataSp);
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}

?>
