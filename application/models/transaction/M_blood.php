<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_blood extends CI_Model{

	function getStockBlood()
	{
        $this->db->select('*');
        $this->db->from('t_stock_blood');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();        
        }
    }

    function getRs()
	{
        $this->db->select('rs_id, rs_name');
        $this->db->from('m_rs');
        $this->db->where('institute_type = "RS"');
        $this->db->group_by('rs_name');
        $this->db->order_by('rs_name', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getRsWhere($where){
        $this->db->select('*');
        $this->db->from('m_rs mr');
        $this->db->join('t_stock_blood ts', 'mr.rs_id = ts.rs_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();        
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function getWhere($where){
        $this->db->select('*');
        $this->db->from('t_stock_blood');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            // echo"Database Is Empty";
        } else {
            return $query->result();
        }
    }

    function save_stock_blood($data){ 
        $this->db->insert('t_stock_blood',$data);        
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }    

    function update_stock_blood($data, $where){
        $this->db->where($where);
        $this->db->update('t_stock_blood',$data);                
        $cek = $this->db->affected_rows();
        if($cek == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }




}

?>
